<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Class ContactForm
 * @package frontend\models
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $message;

    public function rules(): array
    {
        return [
            [['name', 'email', 'message'], 'safe'],
            [['name', 'email', 'message'], 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name'    => 'Ваше имя',
            'email'   => 'E-mail',
            'message' => 'Сообщение',
        ];
    }

    public function sendEmail(): bool
    {
        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom([$this->email => $this->name])
            ->setSubject('Сообщение из формы обратной связи')
            ->setTextBody($this->message)
            ->send();
    }
}
