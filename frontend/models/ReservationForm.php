<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Class ReservationForm
 * @package frontend\models
 */
class ReservationForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $date;

    public function attributeLabels(): array
    {
        return [
            'name'  => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'date'  => 'Дата',
        ];
    }

    public function rules(): array
    {
        return [
            [['name', 'phone', 'date'], 'required'],
            [['name', 'phone'], 'string'],
            ['email', 'email'],
            ['date', 'date'],
        ];
    }

    public function reserve()
    {
        return Yii::$app->mailer
            ->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom(Yii::$app->params['robotEmail'])
            ->setSubject('Заполнена форма заявки')
            ->setTextBody('qef')
            ->send();
    }
}