<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\JqueryAsset',
        'backend\assets\BootstrapAsset',
        'common\assets\fonts\MaterialDesignAsset',
    ];

    public function init()
    {
        $this->css = [
            'css/app.css?' . time(),
        ];

        $this->js = [
            'js/core.js?' . time(),
            'js/app.js?' . time(),
        ];

        $this->publishOptions = [
            'forceCopy' => YII_ENV_DEV ? true : false,
        ];
    }
}
