/**
 * Device.js
 * @see          https://github.com/matthewhudson/device.js
 * @license      MIT License
 */
(function () {
    var e, i, n, o, d, c, t, r, a, v;
    e = window.device, window.device = {}, n = window.document.documentElement, v = window.navigator.userAgent.toLowerCase(), device.ios = function () {
        return device.iphone() || device.ipod() || device.ipad()
    }, device.iphone = function () {
        return o("iphone")
    }, device.ipod = function () {
        return o("ipod")
    }, device.ipad = function () {
        return o("ipad")
    }, device.android = function () {
        return o("android")
    }, device.androidPhone = function () {
        return device.android() && o("mobile")
    }, device.androidTablet = function () {
        return device.android() && !o("mobile")
    }, device.blackberry = function () {
        return o("blackberry") || o("bb10") || o("rim")
    }, device.blackberryPhone = function () {
        return device.blackberry() && !o("tablet")
    }, device.blackberryTablet = function () {
        return device.blackberry() && o("tablet")
    }, device.windows = function () {
        return o("windows")
    }, device.windowsPhone = function () {
        return device.windows() && o("phone")
    }, device.windowsTablet = function () {
        return device.windows() && o("touch") && !device.windowsPhone()
    }, device.fxos = function () {
        return (o("(mobile;") || o("(tablet;")) && o("; rv:")
    }, device.fxosPhone = function () {
        return device.fxos() && o("mobile")
    }, device.fxosTablet = function () {
        return device.fxos() && o("tablet")
    }, device.meego = function () {
        return o("meego")
    }, device.cordova = function () {
        return window.cordova && "file:" === location.protocol
    }, device.nodeWebkit = function () {
        return "object" == typeof window.process
    }, device.mobile = function () {
        return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego()
    }, device.tablet = function () {
        return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet()
    }, device.desktop = function () {
        return !device.tablet() && !device.mobile()
    }, device.portrait = function () {
        return window.innerHeight / window.innerWidth > 1
    }, device.landscape = function () {
        return window.innerHeight / window.innerWidth < 1
    }, device.noConflict = function () {
        return window.device = e, this
    }, o = function (e) {
        return -1 !== v.indexOf(e)
    }, c = function (e) {
        var i;
        return i = new RegExp(e, "i"), n.className.match(i)
    }, i = function (e) {
        return c(e) ? void 0 : n.className += " " + e
    }, r = function (e) {
        return c(e) ? n.className = n.className.replace(e, "") : void 0
    }, device.ios() ? device.ipad() ? i("ios ipad tablet") : device.iphone() ? i("ios iphone mobile") : device.ipod() && i("ios ipod mobile") : i(device.android() ? device.androidTablet() ? "android tablet" : "android mobile" : device.blackberry() ? device.blackberryTablet() ? "blackberry tablet" : "blackberry mobile" : device.windows() ? device.windowsTablet() ? "windows tablet" : device.windowsPhone() ? "windows mobile" : "desktop" : device.fxos() ? device.fxosTablet() ? "fxos tablet" : "fxos mobile" : device.meego() ? "meego mobile" : device.nodeWebkit() ? "node-webkit" : "desktop"), device.cordova() && i("cordova"), d = function () {
        return device.landscape() ? (r("portrait"), i("landscape")) : (r("landscape"), i("portrait"))
    }, a = "onorientationchange" in window, t = a ? "orientationchange" : "resize", window.addEventListener ? window.addEventListener(t, d, !1) : window.attachEvent ? window.attachEvent(t, d) : window[t] = d, d()
}).call(this);

/**
 * @module       SmoothScroll
 * @author       Balazs Galambosi, Patrick Brunner, Michael Herf
 * @license      MIT license
 * @version      v0.9.9
 */
if ($("html").hasClass("smoothscroll")) {
    function ssc_init() {
        if (document.body) {
            var a = document.body, b = document.documentElement, c = window.innerHeight, d = a.scrollHeight;
            (ssc_root = 0 <= document.compatMode.indexOf("CSS") ? b : a, ssc_activeElement = a, ssc_initdone = !0, top != self) ? ssc_frame = !0 : d > c && (a.offsetHeight <= c || b.offsetHeight <= c) && (ssc_root.style.height = "auto", ssc_root.offsetHeight <= c) && (c = document.createElement("div"), c.style.clear = "both", a.appendChild(c));
            ssc_fixedback || (a.style.backgroundAttachment = "scroll", b.style.backgroundAttachment = "scroll");
            ssc_keyboardsupport && ssc_addEvent("keydown", ssc_keydown)
        }
    }

    function ssc_scrollArray(a, b, c, d) {
        if (d || (d = 1E3), ssc_directionCheck(b, c), ssc_que.push({
                x: b,
                y: c,
                lastX: 0 > b ? .99 : -.99,
                lastY: 0 > c ? .99 : -.99,
                start: +new Date
            }), !ssc_pending) {
            var e = function () {
                for (var n = +new Date, h = 0, m = 0, k = 0; k < ssc_que.length; k++) {
                    var f = ssc_que[k], l = n - f.start, p = l >= ssc_animtime, g = p ? 1 : l / ssc_animtime;
                    ssc_pulseAlgorithm && (g = ssc_pulse(g));
                    l = f.x * g - f.lastX >> 0;
                    g = f.y * g - f.lastY >> 0;
                    h += l;
                    m += g;
                    f.lastX += l;
                    f.lastY += g;
                    p && (ssc_que.splice(k, 1), k--)
                }
                b && (n = a.scrollLeft, a.scrollLeft += h, h && a.scrollLeft === n && (b = 0));
                c && (h = a.scrollTop, a.scrollTop += m, m && a.scrollTop === h && (c = 0));
                b || c || (ssc_que = []);
                ssc_que.length ? setTimeout(e, d / ssc_framerate + 1) : ssc_pending = !1
            };
            setTimeout(e, 0);
            ssc_pending = !0
        }
    }

    function ssc_wheel(a) {
        ssc_initdone || ssc_init();
        var b = a.target, c = ssc_overflowingAncestor(b);
        if (!c || a.defaultPrevented || ssc_isNodeName(ssc_activeElement, "embed") || ssc_isNodeName(b, "embed") && /\.pdf/i.test(b.src))return !0;
        var b = a.wheelDeltaX || 0, d = a.wheelDeltaY || 0;
        b || d || (d = a.wheelDelta || 0);
        1.2 < Math.abs(b) && (b *= ssc_stepsize / 120);
        1.2 < Math.abs(d) && (d *= ssc_stepsize / 120);
        ssc_scrollArray(c, -b, -d);
        a.preventDefault()
    }

    function ssc_keydown(a) {
        var b = a.target, c = a.ctrlKey || a.altKey || a.metaKey;
        if (/input|textarea|embed/i.test(b.nodeName) || b.isContentEditable || a.defaultPrevented || c || ssc_isNodeName(b, "button") && a.keyCode === ssc_key.spacebar)return !0;
        var d;
        d = b = 0;
        var c = ssc_overflowingAncestor(ssc_activeElement), e = c.clientHeight;
        switch (c == document.body && (e = window.innerHeight), a.keyCode) {
            case ssc_key.up:
                d = -ssc_arrowscroll;
                break;
            case ssc_key.down:
                d = ssc_arrowscroll;
                break;
            case ssc_key.spacebar:
                d = a.shiftKey ? 1 : -1;
                d = -d * e * .9;
                break;
            case ssc_key.pageup:
                d = .9 * -e;
                break;
            case ssc_key.pagedown:
                d = .9 * e;
                break;
            case ssc_key.home:
                d = -c.scrollTop;
                break;
            case ssc_key.end:
                e = c.scrollHeight - c.scrollTop - e;
                d = 0 < e ? e + 10 : 0;
                break;
            case ssc_key.left:
                b = -ssc_arrowscroll;
                break;
            case ssc_key.right:
                b = ssc_arrowscroll;
                break;
            default:
                return !0
        }
        ssc_scrollArray(c, b, d);
        a.preventDefault()
    }

    function ssc_mousedown(a) {
        ssc_activeElement = a.target
    }

    function ssc_setCache(a, b) {
        for (var c = a.length; c--;)ssc_cache[ssc_uniqueID(a[c])] = b;
        return b
    }

    function ssc_overflowingAncestor(a) {
        var b = [], c = ssc_root.scrollHeight;
        do {
            var d = ssc_cache[ssc_uniqueID(a)];
            if (d)return ssc_setCache(b, d);
            if (b.push(a), c === a.scrollHeight) {
                if (!ssc_frame || ssc_root.clientHeight + 10 < c)return ssc_setCache(b, document.body)
            } else if (a.clientHeight + 10 < a.scrollHeight && (overflow = getComputedStyle(a, "").getPropertyValue("overflow"), "scroll" === overflow || "auto" === overflow))return ssc_setCache(b, a)
        } while (a = a.parentNode)
    }

    function ssc_addEvent(a, b, c) {
        window.addEventListener(a, b, c || !1)
    }

    function ssc_removeEvent(a, b, c) {
        window.removeEventListener(a, b, c || !1)
    }

    function ssc_isNodeName(a, b) {
        return a.nodeName.toLowerCase() === b.toLowerCase()
    }

    function ssc_directionCheck(a, b) {
        a = 0 < a ? 1 : -1;
        b = 0 < b ? 1 : -1;
        (ssc_direction.x !== a || ssc_direction.y !== b) && (ssc_direction.x = a, ssc_direction.y = b, ssc_que = [])
    }

    function ssc_pulse_(a) {
        var b, c, d;
        return a *= ssc_pulseScale, 1 > a ? b = a - (1 - Math.exp(-a)) : (c = Math.exp(-1), --a, d = 1 - Math.exp(-a), b = c + d * (1 - c)), b * ssc_pulseNormalize
    }

    function ssc_pulse(a) {
        return 1 <= a ? 1 : 0 >= a ? 0 : (1 == ssc_pulseNormalize && (ssc_pulseNormalize /= ssc_pulse_(1)), ssc_pulse_(a))
    }

    if (-1 === navigator.platform.toUpperCase().indexOf("MAC") && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini|BlackBerry)/)) {
        var ssc_framerate = 150, ssc_animtime = 700, ssc_stepsize = 100, ssc_pulseAlgorithm = !0, ssc_pulseScale = 8,
            ssc_pulseNormalize = 1, ssc_keyboardsupport = !0, ssc_arrowscroll = 50, ssc_frame = !1,
            ssc_direction = {x: 0, y: 0}, ssc_initdone = !1, ssc_fixedback = !0, ssc_root = document.documentElement,
            ssc_activeElement, ssc_key = {
                left: 37,
                up: 38,
                right: 39,
                down: 40,
                spacebar: 32,
                pageup: 33,
                pagedown: 34,
                end: 35,
                home: 36
            }, ssc_que = [], ssc_pending = !1, ssc_cache = {};
        setInterval(function () {
            ssc_cache = {}
        }, 1E4);
        var ssc_uniqueID = function () {
            var a = 0;
            return function (b) {
                return b.ssc_uniqueID || (b.ssc_uniqueID = a++)
            }
        }(), ischrome = /chrome/.test(navigator.userAgent.toLowerCase());
        ischrome && (ssc_addEvent("mousedown", ssc_mousedown), ssc_addEvent("mousewheel", ssc_wheel), ssc_addEvent("load", ssc_init))
    }
    ;
}

/**
 * @module       RD Parallax
 * @author       Evgeniy Gusarov
 * @see          https://ua.linkedin.com/pub/evgeniy-gusarov/8a/a40/54a
 * @version      3.6.4
 */
(function () {
    (function (p, n, d) {
        var t, x, y, v, k, m, w, r, u;
        m = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        y = /Chrome/.test(navigator.userAgent);
        r = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) || /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
        v = m && /crios/i.test(navigator.userAgent);
        w = /iPhone|iPad|iPod/i.test(navigator.userAgent) && !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
        k = -1 !== navigator.appVersion.indexOf("MSIE") ||
            -1 < navigator.appVersion.indexOf("Trident/");
        u = /windows nt 6.2/.test(navigator.userAgent.toLowerCase()) || /windows nt 6.3/.test(navigator.userAgent.toLowerCase());
        x = null != n.body.classList;
        (function () {
            var k, m, n, b, a;
            m = 0;
            a = ["ms", "moz", "webkit", "o"];
            k = 0;
            for (n = a.length; k < n; k++)b = a[k], d.requestAnimationFrame = d[b + "RequestAnimationFrame"], d.cancelAnimationFrame = d[b + "CancelAnimationFrame"] || d[b + "CancelRequestAnimationFrame"];
            d.requestAnimationFrame || (d.requestAnimationFrame = function (a, c) {
                var e, b, f;
                e = (new Date).getTime();
                f = Math.max(0, 16 - (e - m));
                b = d.setTimeout(function () {
                    a(e + f)
                }, f);
                m = e + f;
                return b
            });
            if (!d.cancelAnimationFrame)return d.cancelAnimationFrame = function (a) {
                return clearTimeout(a)
            }
        });
        t = function () {
            function q(b) {
                this.options = p.extend(!0, {}, this.Defaults, b);
                this.scenes = [];
                this.initialize();
                this.scrollY = d.scrollY || d.pageYOffset;
                this.lastScrollY = -1;
                this.lastDocHeight = 0;
                this.checkLayerHeight = this.inputFocus = !1
            }

            var z, t;
            z = function () {
                function b(a, g, c, e, b, f, h) {
                    this.amend = r || k || m ? 60 : 0;
                    this.element = a;
                    this.aliases = g;
                    this.type =
                        a.getAttribute("data-type") || "html";
                    "html" === this.type && (this.holder = this.createHolder());
                    this.direction = "normal" === a.getAttribute("data-direction") || null == a.getAttribute("data-direction") ? 1 : -1;
                    this.fade = "true" === a.getAttribute("data-fade");
                    this.blur = "true" === a.getAttribute("data-blur");
                    this.boundTo = n.querySelector(a.getAttribute("data-bound-to"));
                    "media" === this.type && (this.url = a.getAttribute("data-url"));
                    this.responsive = this.getResponsiveOptions();
                    this.element.style.position = !k && !m || m || u && k ? "absolute" :
                        "fixed";
                    switch (this.type) {
                        case "media":
                            null != this.url && (this.element.style["background-image"] = "url(" + this.url + ")");
                            break;
                        case "html":
                            k && m && (this.element.style["z-index"] = 1)
                    }
                    this.refresh(c, e, b, f, h)
                }

                b.prototype.refresh = function (a, g, c, e, b) {
                    this.speed = this.getOption("speed", a) || 0;
                    this.offset = this.getOption("offset", a) || 0;
                    m || u && k || (this.element.style.position = b ? "fixed" : "absolute");
                    k && "html" === this.type && (this.element.style.position = "absolute");
                    switch (this.type) {
                        case "media":
                            if (!k)return this.offsetHeight =
                                this.getMediaHeight(g, e, this.speed, this.direction), this.element.style.height = this.offsetHeight + "px";
                            break;
                        case "html":
                            this.element.style.width = this.holder.offsetWidth + "px";
                            this.offsetHeight = this.element.offsetHeight;
                            this.holder.style.height = this.offsetHeight + "px";
                            if (!(!k && !m || m || u && k))return k ? this.element.style.position = "static" : b && (this.element.style.left = this.getOffset(this.holder).left + "px", this.element.style.top = this.getOffset(this.holder).top - c + "px"), this.holder.style.position = "static";
                            break;
                        case "custom":
                            return this.offsetHeight = this.element.offsetHeight
                    }
                };
                b.prototype.createHolder = function () {
                    var a;
                    a = n.createElement("div");
                    x ? a.classList.add("rd-parallax-layer-holder") : a.className = "rd-parallax-layer-holder";
                    this.element.parentNode.insertBefore(a, this.element);
                    a.appendChild(this.element);
                    if (!k && !m || v) a.style.position = "relative";
                    return a
                };
                b.prototype.isHolderWrong = function () {
                    return "html" === this.type && this.holder.offsetHeight !== this.element.offsetHeight ? !0 : !1
                };
                b.prototype.getOption = function (a,
                                                  g) {
                    var c, e;
                    for (c in this.responsive)c <= g && (e = c);
                    return this.responsive[e][a]
                };
                b.prototype.getResponsiveOptions = function () {
                    var a, g, c, e, b, f, h, l, d;
                    l = {};
                    h = [];
                    g = [];
                    e = this.aliases;
                    for (c in e)a = e[c], h.push(c), g.push(a);
                    c = e = 0;
                    for (b = h.length; e < b; c = ++e)for (f = h[c], l[f] = {}; -1 <= (a = c);)!l[f].speed && (d = this.element.getAttribute("data" + g[a] + "speed")) && (l[f].speed = this.getSpeed(d)), !l[f].offset && (d = this.element.getAttribute("data" + g[a] + "offset")) && (l[f].offset = parseInt(d)), !l[f].fade && (d = this.element.getAttribute("data" +
                        g[a] + "fade")) && (l[f].fade = "true" === d), c--;
                    return l
                };
                b.prototype.fuse = function (a, g) {
                    var c, e, b;
                    c = this.getOffset(this.element).top + this.element.getBoundingClientRect().top;
                    e = a + g / 2;
                    c += this.offsetHeight / 2;
                    b = g / 6;
                    e + b > c && e - b < c ? this.element.style.opacity = 1 : (e = e - b < c ? 1 + (e + b - c) / g / 3 * 10 : 1 - (e - b - c) / g / 3 * 10, this.element.style.opacity = 0 > e ? 0 : 1 < e ? 1 : e.toFixed(2))
                };
                b.prototype.move = function (a, g, c, b, d, f, h, l, n) {
                    k && "media" === this.type || m || u && k || (h ? (h = !m || "html" === this.type && n || v ? this.speed * this.direction : this.speed * this.direction -
                        1, g = this.offsetHeight, null != l ? f = (b + c - (l + c)) / (c - d) : "media" !== this.type ? b < c || b > f - c ? (f = b < c ? b / (c - d) : (b + c - f) / (c - d), isFinite(f) || (f = 0)) : f = .5 : f = .5, a = v || k ? (d - g) / 2 + (c - d) * f * h + this.offset : m ? -(b - a) * h + (d - g) / 2 + (c - d) * f * (h + 1) + this.offset : -(b - a) * h + (d - g) / 2 + (c - d) * f * h + this.offset, m && null != l && (this.element.style.top = b - l + "px"), r && (this.element.style["-webkit-transform"] = "translate3d(0," + a + "px,0)"), this.element.style.transform = "translate3d(0," + a + "px,0)") : (r && (this.element.style["-webkit-transform"] = "translate3d(0,0,0)"),
                        this.element.style.transform = "translate3d(0,0,0)"))
                };
                b.prototype.getSpeed = function (a) {
                    return Math.min(Math.max(parseFloat(a), 0), 2)
                };
                b.prototype.getMediaHeight = function (a, b, c, e) {
                    return b + (-1 === e ? (b + a) * c : 0) + (1 >= c ? Math.abs(a - b) * c : a * c) + 2 * this.amend
                };
                b.prototype.getOffset = function (a) {
                    a = a.getBoundingClientRect();
                    return {top: a.top + (d.scrollY || d.pageYOffset), left: a.left + (d.scrollX || d.pageXOffset)}
                };
                return b
            }();
            t = function () {
                function b(a, b, c, e) {
                    this.amend = r ? 60 : 0;
                    this.element = a;
                    this.aliases = b;
                    this.on = !0;
                    this.agent =
                        n.querySelector(a.getAttribute("data-agent"));
                    this.anchor = this.findAnchor();
                    this.canvas = this.createCanvas();
                    this.layers = this.createLayers(c);
                    this.fitTo = this.getFitElement();
                    this.responsive = this.getResponsiveOptions();
                    this.refresh(c, e)
                }

                b.prototype.getFitElement = function () {
                    var a;
                    return null != (a = this.element.getAttribute("data-fit-to")) ? "parent" === a ? this.element.parentNode : n.querySelector(a) : null
                };
                b.prototype.findAnchor = function () {
                    var a;
                    for (a = this.element.parentNode; null != a && a !== n;) {
                        if (this.isTransformed.call(a))return a;
                        a = a.parentNode
                    }
                    return null
                };
                b.prototype.createCanvas = function () {
                    var a;
                    a = n.createElement("div");
                    x ? a.classList.add("rd-parallax-inner") : a.className = "rd-parallax-inner";
                    for (this.element.appendChild(a); this.element.firstChild !== a;)a.appendChild(this.element.firstChild);
                    this.element.style.position = "relative";
                    this.element.style.overflow = "hidden";
                    k || m ? (a.style.position = "absolute", u && k || (a.style.clip = "rect(0, auto, auto, 0)"), a.style.transform = k ? "translate3d(0,0,0)" : "none") : a.style.position = "fixed";
                    a.style.left =
                        this.offsetLeft + "px";
                    a.style.top = 0;
                    r && (a.style["margin-top"] = "-" + this.amend + "px", a.style.padding = this.amend + "px 0", this.element.style["z-index"] = 0);
                    return a
                };
                b.prototype.getOption = function (a, b) {
                    var c, e;
                    for (c in this.responsive)c <= b && (e = c);
                    return this.responsive[e][a]
                };
                b.prototype.getResponsiveOptions = function () {
                    var a, b, c, e, d, f, h, l, k;
                    l = {};
                    h = [];
                    b = [];
                    e = this.aliases;
                    for (c in e)a = e[c], h.push(c), b.push(a);
                    c = e = 0;
                    for (d = h.length; e < d; c = ++e)for (f = h[c], l[f] = {}; -1 <= (a = c);)l[f].on || null == (k = this.element.getAttribute("data" +
                        b[a] + "on")) || (l[f].on = "false" !== k), null == l[f].on && 0 === a && (l[f].on = !0), c--;
                    return l
                };
                b.prototype.createLayers = function (a, b) {
                    var c, e, d, f, h;
                    e = p(this.element).find(".rd-parallax-layer").get();
                    f = [];
                    c = d = 0;
                    for (h = e.length; d < h; c = ++d)c = e[c], f.push(new z(c, this.aliases, a, b, this.offsetTop, this.offsetHeight, this.on));
                    return f
                };
                b.prototype.move = function (a) {
                    a = null != this.anchor ? this.positionTop : this.offsetTop - a;
                    r && (this.canvas.style["-webkit-transform"] = "translate3d(0," + a + "px,0)");
                    return this.canvas.style.transform =
                        "translate3d(0," + a + "px,0)"
                };
                b.prototype.refresh = function (a, b) {
                    var c, e, d, f, h;
                    f = [];
                    this.on = this.getOption("on", a);
                    this.offsetTop = this.getOffset(this.element).top;
                    this.offsetLeft = this.getOffset(this.element).left;
                    this.width = this.element.offsetWidth;
                    this.canvas.style.width = this.width + "px";
                    null != this.anchor && (this.positionTop = this.element.offsetTop);
                    null != this.agent ? (this.agentOffset = this.getOffset(this.agent).top, this.agentHeight = this.agent.offsetHeight) : this.agentOffset = this.agentHeight = null;
                    h = this.layers;
                    c = 0;
                    for (d = h.length; c < d; c++)e = h[c], "media" === e.type ? f.push(e) : e.refresh(a, b, this.offsetTop, this.offsetHeight, this.on);
                    this.offsetHeight = this.canvas.offsetHeight - 2 * this.amend;
                    this.element.style.height = this.offsetHeight + "px";
                    c = 0;
                    for (d = f.length; c < d; c++)e = f[c], e.refresh(a, b, this.offsetTop, this.offsetHeight, this.on)
                };
                b.prototype.update = function (a, b, c, e, d) {
                    var f, h, l, n, p, q, r;
                    r = this.offsetTop;
                    q = this.offsetHeight;
                    k || m || this.move(a);
                    n = this.layers;
                    p = [];
                    f = 0;
                    for (l = n.length; f < l; f++)h = n[f], h.move(a, b, c, r, q, e, this.on,
                        this.agentOffset, d), h.fade = h.getOption("fade", b) || !1, !h.fade || m || k ? p.push(void 0) : p.push(h.fuse(r, q));
                    return p
                };
                b.prototype.isTransformed = function () {
                    var a, b, c;
                    c = {
                        webkitTransform: "-webkit-transform",
                        OTransform: "-o-transform",
                        msTransform: "-ms-transform",
                        MozTransform: "-moz-transform",
                        transform: "transform"
                    };
                    for (a in c)c.hasOwnProperty(a) && null != this.style[a] && (b = d.getComputedStyle(this).getPropertyValue(c[a]));
                    return null != b && 0 < b.length && "none" !== b ? !0 : !1
                };
                b.prototype.getOffset = function (a) {
                    a = a.getBoundingClientRect();
                    return {top: a.top + (d.scrollY || d.pageYOffset), left: a.left + (d.scrollX || d.pageYOffset)}
                };
                return b
            }();
            q.prototype.Defaults = {
                selector: ".rd-parallax",
                screenAliases: {
                    0: "-",
                    480: "-xs-",
                    768: "-sm-",
                    992: "-md-",
                    1200: "-lg-",
                    1920: "-xl-",
                    2560: "-xxl-"
                }
            };
            q.prototype.initialize = function () {
                var b, a, g, c, e, k, f;
                b = this;
                g = n.querySelectorAll(b.options.selector);
                f = d.innerWidth;
                k = d.innerHeight;
                a = c = 0;
                for (e = g.length; c < e; a = ++c)a = g[a], b.scenes.push(new t(a, b.options.screenAliases, f, k));
                p(d).on("resize", p.proxy(b.resize, b));
                if (w) p("input").on("focusin focus",
                    function (a) {
                        a.preventDefault();
                        b.activeOffset = p(this).offset().top;
                        return d.scrollTo(d.scrollX || d.pageXOffset, b.activeOffset - this.offsetHeight - 100)
                    });
                p(d).trigger("resize");
                b.update();
                b.checkResize()
            };
            q.prototype.resize = function (b) {
                var a, g, c;
                if ((a = d.innerWidth) !== this.windowWidth || !m || b) {
                    this.windowWidth = a;
                    this.windowHeight = d.innerHeight;
                    this.documentHeight = n.body.offsetHeight;
                    g = this.scenes;
                    b = 0;
                    for (a = g.length; b < a; b++)c = g[b], c.refresh(this.windowWidth, this.windowHeight);
                    return this.update(!0)
                }
            };
            q.prototype.update =
                function (b) {
                    var a, g, c, e, k, f, h, l, p, q;
                    g = this;
                    b || requestAnimationFrame(function () {
                        g.update()
                    });
                    l = d.scrollY || d.pageYOffset;
                    w && null != (a = n.activeElement) && (a.tagName.match(/(input)|(select)|(textarea)/i) ? (g.activeElement = a, g.inputFocus = !0) : (g.activeElement = null, g.inputFocus = !1, b = !0));
                    m && y && (a = d.innerHeight - g.windowHeight, g.deltaHeight = a, l -= g.deltaHeight);
                    if ((l !== g.lastScrollY || b) && !g.isActing) {
                        g.isActing = !0;
                        q = g.windowWidth;
                        p = g.windowHeight;
                        c = g.documentHeight;
                        a = l - g.lastScrollY;
                        w && null != g.activeElement &&
                        (g.activeElement.value += " ", g.activeElement.value = g.activeElement.value.trim());
                        f = g.scenes;
                        e = 0;
                        for (k = f.length; e < k; e++)h = f[e], (g.inputFocus || b || l + p >= (h.agentOffset || h.offsetTop) + a && l <= (h.agentOffset || h.offsetTop) + (h.agentHeight || h.offsetHeight) + a) && h.update(l, q, p, c, g.inputFocus);
                        g.lastScrollY = l;
                        return g.isActing = !1
                    }
                };
            q.prototype.checkResize = function () {
                var b;
                b = this;
                setInterval(function () {
                    var a, d, c, e, k, f, h, l;
                    a = n.body.offsetHeight;
                    h = b.scenes;
                    d = 0;
                    for (k = h.length; d < k; d++) {
                        c = h[d];
                        l = c.layers;
                        c = 0;
                        for (f = l.length; c <
                        f; c++)if (e = l[c], e.isHolderWrong()) {
                            b.checkLayerHeight = !0;
                            break
                        }
                        if (b.checkLayerHeight)break
                    }
                    if (b.checkLayerHeight || a !== b.lastDocHeight)return b.resize(!0), b.lastDocHeight = a, b.checkLayerHeight = !1
                }, 500)
            };
            return q
        }();
        p.RDParallax = function (d) {
            var k;
            k = p(n);
            if (!k.data("RDParallax"))return k.data("RDParallax", new t(d))
        };
        return d.RDParallax = t
    })(window.jQuery, document, window);
    "undefined" !== typeof module && null !== module ? module.exports = window.RDParallax : "function" === typeof define && define.amd && define(["jquery"],
            function () {
                return window.RDParallax
            })
}).call(this);

/**
 * @module       RD Navbar
 * @author       Evgeniy Gusarov
 * @see          https://ua.linkedin.com/pub/evgeniy-gusarov/8a/a40/54a
 * @version      2.1.3
 */
(function () {
    var t;
    t = "ontouchstart" in window, function (o, e, n) {
        var s;
        return s = function () {
            function s(t, s) {
                this.options = o.extend(!1, {}, this.Defaults, s), this.$element = o(t), this.$clone = null, this.$win = o(n), this.$doc = o(e), this.currentLayout = this.options.layout, this.loaded = !1, this.focusOnHover = this.options.focusOnHover, this.focusTimer = !1, this.cloneTimer = !1, this.isStuck = !1, this.initialize()
            }

            return s.prototype.Defaults = {
                layout: "rd-navbar-static",
                deviceLayout: "rd-navbar-fixed",
                focusOnHover: !0,
                focusOnHoverTimeout: 800,
                linkedElements: ["html"],
                domAppend: !0,
                stickUp: !0,
                stickUpClone: !0,
                stickUpOffset: "100%",
                anchorNavSpeed: 400,
                anchorNavOffset: 0,
                anchorNavEasing: "swing",
                autoHeight: !0,
                responsive: {
                    0: {layout: "rd-navbar-fixed", focusOnHover: !1, stickUp: !1},
                    992: {layout: "rd-navbar-static", focusOnHover: !0, stickUp: !0}
                },
                callbacks: {
                    onToggleSwitch: !1,
                    onToggleClose: !1,
                    onDomAppend: !1,
                    onDropdownOver: !1,
                    onDropdownOut: !1,
                    onDropdownToggle: !1,
                    onDropdownClose: !1,
                    onStuck: !1,
                    onUnstuck: !1,
                    onAnchorChange: !1
                }
            }, s.prototype.initialize = function () {
                var o;
                return o = this, o.$element.addClass("rd-navbar").addClass(o.options.layout), t && o.$element.addClass("rd-navbar--is-touch"), this.setDataAPI(o), o.options.domAppend && this.createNav(o), o.options.stickUpClone && this.createClone(o), this.applyHandlers(o), this.offset = o.$element.offset().top, this.height = o.$element.outerHeight(), this.loaded = !0, o
            }, s.prototype.resize = function (e, n) {
                var s, a;
                return a = t ? e.getOption("deviceLayout") : e.getOption("layout"), s = e.$element.add(e.$clone), a === e.currentLayout && e.loaded || (e.switchClass(s, e.currentLayout, a), null != e.options.linkedElements && o.grep(e.options.linkedElements, function (t, o) {
                    return e.switchClass(t, e.currentLayout + "-linked", a + "-linked")
                }), e.currentLayout = a), e.focusOnHover = e.getOption("focusOnHover"), e
            }, s.prototype.stickUp = function (t, e) {
                var n, s, a, r, i;
                return s = t.getOption("stickUp"), n = t.$doc.scrollTop(), r = null != t.$clone ? t.$clone : t.$element, a = t.getOption("stickUpOffset"), i = "string" == typeof a ? a.indexOf("%") > 0 ? parseFloat(a) * t.height / 100 : parseFloat(a) : a, s ? (n >= i && !t.isStuck || i > n && t.isStuck) && (t.$element.add(t.$clone).find("[data-rd-navbar-toggle]").each(function () {
                        o.proxy(t.closeToggle, this)(t, !1)
                    }).end().find(".rd-navbar-submenu").removeClass("opened").removeClass("focus"), n >= i && !t.isStuck ? ("resize" === e.type ? t.switchClass(r, "", "rd-navbar--is-stuck") : r.addClass("rd-navbar--is-stuck"), t.isStuck = !0, t.options.callbacks.onStuck && t.options.callbacks.onStuck.call(t)) : ("resize" === e.type ? t.switchClass(r, "rd-navbar--is-stuck", "") : r.removeClass("rd-navbar--is-stuck").one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", o.proxy(t.resizeWrap, t, e)), t.isStuck = !1, t.options.callbacks.onUnstuck && t.options.callbacks.onUnstuck.call(t))) : t.isStuck && (t.switchClass(r, "rd-navbar--is-stuck", ""), t.isStuck = !1, t.resizeWrap(e)), t
            }, s.prototype.resizeWrap = function (t) {
                var o, e;
                return e = this, null != e.$clone || e.isStuck ? void 0 : (o = e.$element.parent(), e.getOption("autoHeight") ? (e.height = e.$element.outerHeight(), "resize" === t.type ? (o.addClass("rd-navbar--no-transition").css("height", e.height), o[0].offsetHeight, o.removeClass("rd-navbar--no-transition")) : o.css("height", e.height)) : void o.css("height", "auto"))
            }, s.prototype.createNav = function (t) {
                return t.$element.find(".rd-navbar-dropdown, .rd-navbar-megamenu").each(function () {
                    var t;
                    return t = o(this), t.offset().left + t.outerWidth() > n.innerWidth && t.addClass("rd-navbar-open-left"), t.hasClass("rd-navbar-megamenu") ? t.parent().addClass("rd-navbar--has-megamenu") : t.parent().addClass("rd-navbar--has-dropdown")
                }).parents("li").addClass("rd-navbar-submenu").append(o("<span/>", {"class": "rd-navbar-submenu-toggle"})), t.options.callbacks.onDomAppend && t.options.callbacks.onDomAppend.call(this), t
            }, s.prototype.createClone = function (t) {
                return t.$clone = t.$element.clone().insertAfter(t.$element).addClass("rd-navbar--is-clone"), t
            }, s.prototype.closeToggle = function (t, e) {
                var n, s, a, r;
                return s = o(e.target), a = !1, e.target !== this && !s.parents("[data-rd-navbar-toggle]").length && (r = this.getAttribute("data-rd-navbar-toggle")) && (n = o(this).parents(".rd-navbar").find(r).add(o(this).parents(".rd-navbar")[0]), n.each(function () {
                    return a ? void 0 : a = (e.target === this || o.contains(this, e.target)) === !0
                }), a || (n.add(this).removeClass("active"), t.options.callbacks.onToggleClose && t.options.callbacks.onToggleClose.call(this, t))), this
            }, s.prototype.switchToggle = function (t, e) {
                var n;
                return e.preventDefault(), (n = this.getAttribute("data-rd-navbar-toggle")) && (o("[data-rd-navbar-toggle]").not(this).each(function () {
                    var t;
                    return (t = this.getAttribute("data-rd-navbar-toggle")) ? o(this).parents(".rd-navbar").find(t).add(this).add(o.inArray(".rd-navbar", t.split(/\s*,\s*/i)) > -1 ? o(this).parents(".rd-navbar")[0] : !1).removeClass("active") : void 0
                }), o(this).parents(".rd-navbar").find(n).add(this).add(o.inArray(".rd-navbar", n.split(/\s*,\s*/i)) > -1 ? o(this).parents(".rd-navbar")[0] : !1).toggleClass("active"), t.options.callbacks.onToggleSwitch && t.options.callbacks.onToggleSwitch.call(this, t)), this
            }, s.prototype.dropdownOver = function (e, n) {
                var s;
                return e.focusOnHover && !t && (s = o(this), clearTimeout(n), s.addClass("focus").siblings().removeClass("opened").each(e.dropdownUnfocus), e.options.callbacks.onDropdownOver && e.options.callbacks.onDropdownOver.call(this, e)), this
            }, s.prototype.dropdownOut = function (e, n) {
                var s;
                return e.focusOnHover && !t && (s = o(this), s.one("mouseenter.navbar", function () {
                    return clearTimeout(n)
                }), clearTimeout(n), n = setTimeout(o.proxy(e.dropdownUnfocus, this, e), e.options.focusOnHoverTimeout), e.options.callbacks.onDropdownOut && e.options.callbacks.onDropdownOut.call(this, e)), this
            }, s.prototype.dropdownUnfocus = function (t) {
                var e;
                return e = o(this), e.find("li.focus").add(this).removeClass("focus"), this
            }, s.prototype.dropdownClose = function (t, e) {
                var n;
                return e.target === this || o(e.target).parents(".rd-navbar-submenu").length || (n = o(this), n.find("li.focus").add(this).removeClass("focus").removeClass("opened"), t.options.callbacks.onDropdownClose && t.options.callbacks.onDropdownClose.call(this, t)), this
            }, s.prototype.dropdownToggle = function (t) {
                return o(this).toggleClass("opened").siblings().removeClass("opened"), t.options.callbacks.onDropdownToggle && t.options.callbacks.onDropdownToggle.call(this, t), this
            }, s.prototype.goToAnchor = function (t, e) {
                var n, s;
                return s = this.hash, n = o(s), n.length && (e.preventDefault(), o("html, body").stop().animate({scrollTop: n.offset().top + t.getOption("anchorNavOffset") + 1}, t.getOption("anchorNavSpeed"), t.getOption("anchorNavEasing"), function () {
                    return t.changeAnchor(s)
                })), this
            }, s.prototype.activateAnchor = function (t) {
                var e, n, s, a, r, i, l, c, p, d, h, u;
                if (a = this, h = a.$doc.scrollTop(), u = a.$win.height(), r = a.$doc.height(), d = a.getOption("anchorNavOffset"), h + u > r - 50)return e = o('[data-type="anchor"]').last(), e.length && e.offset().top >= h && (i = "#" + e.attr("id"), n = o('.rd-navbar-nav a[href^="' + i + '"]').parent(), n.hasClass("active") || (n.addClass("active").siblings().removeClass("active"), a.options.callbacks.onAnchorChange && a.options.callbacks.onAnchorChange.call(e[0], a))), e;
                p = o('.rd-navbar-nav a[href^="#"]').get();
                for (l in p)c = p[l], s = o(c), i = s.attr("href"), e = o(i), e.length && e.offset().top + d <= h && e.offset().top + e.outerHeight() > h && (s.parent().addClass("active").siblings().removeClass("active"), a.options.callbacks.onAnchorChange && a.options.callbacks.onAnchorChange.call(e[0], a));
                return null
            }, s.prototype.getAnchor = function () {
                return history && history.state ? history.state.id : null
            }, s.prototype.changeAnchor = function (t) {
                return history && (history.state && history.state.id !== t ? history.replaceState({anchorId: t}, null, t) : history.pushState({anchorId: t}, null, t)), this
            }, s.prototype.applyHandlers = function (e) {
                return null != e.options.responsive && e.$win.on("resize.navbar", o.proxy(e.resize, e.$win[0], e)).on("resize.navbar", o.proxy(e.resizeWrap, e)).on("resize.navbar", o.proxy(e.stickUp, null != e.$clone ? e.$clone : e.$element, e)).on("orientationchange.navbar", o.proxy(e.resize, e.$win[0], e)).trigger("resize.navbar"), e.$doc.on("scroll.navbar", o.proxy(e.stickUp, null != e.$clone ? e.$clone : e.$element, e)).on("scroll.navbar", o.proxy(e.activateAnchor, e)), e.$element.add(e.$clone).find("[data-rd-navbar-toggle]").each(function () {
                    var n;
                    return n = o(this), n.on(t ? "touchstart" : "click", o.proxy(e.switchToggle, this, e)), n.parents("body").on(t ? "touchstart" : "click", o.proxy(e.closeToggle, this, e))
                }), e.$element.add(e.$clone).find(".rd-navbar-submenu").each(function () {
                    var n, s;
                    return n = o(this), s = n.parents(".rd-navbar--is-clone").length ? e.cloneTimer : e.focusTimer, n.on("mouseleave.navbar", o.proxy(e.dropdownOut, this, e, s)), n.find("> a").on("mouseenter.navbar", o.proxy(e.dropdownOver, this, e, s)), n.find("> .rd-navbar-submenu-toggle").on("click", o.proxy(e.dropdownToggle, this, e)), n.parents("body").on(t ? "touchstart" : "click", o.proxy(e.dropdownClose, this, e))
                }), e.$element.add(e.$clone).find('.rd-navbar-nav a[href^="#"]').each(function () {
                    return o(this).on(t ? "touchstart" : "click", o.proxy(e.goToAnchor, this, e))
                }), e
            }, s.prototype.switchClass = function (t, e, n) {
                var s;
                return s = t instanceof jQuery ? t : o(t), s.addClass("rd-navbar--no-transition").removeClass(e).addClass(n), s[0].offsetHeight, s.removeClass("rd-navbar--no-transition")
            }, s.prototype.setDataAPI = function (t) {
                var o, e, n, s, a, r;
                for (o = ["-", "-xs-", "-sm-", "-md-", "-lg-"], r = [0, 480, 768, 992, 1200], e = n = 0, s = r.length; s > n; e = ++n)a = r[e], this.$element.attr("data" + o[e] + "layout") && (this.options.responsive[r[e]] || (this.options.responsive[r[e]] = {}), this.options.responsive[r[e]].layout = this.$element.attr("data" + o[e] + "layout")), this.$element.attr("data" + o[e] + "device-layout") && (this.options.responsive[r[e]] || (this.options.responsive[r[e]] = {}), this.options.responsive[r[e]].deviceLayout = this.$element.attr("data" + o[e] + "device-layout")), this.$element.attr("data" + o[e] + "hover-on") && (this.options.responsive[r[e]] || (this.options.responsive[r[e]] = {}), this.options.responsive[r[e]].focusOnHover = "true" === this.$element.attr("data" + o[e] + "hover-on")), this.$element.attr("data" + o[e] + "stick-up") && (this.options.responsive[r[e]] || (this.options.responsive[r[e]] = {}), this.options.responsive[r[e]].stickUp = "true" === this.$element.attr("data" + o[e] + "stick-up")), this.$element.attr("data" + o[e] + "auto-height") && (this.options.responsive[r[e]] || (this.options.responsive[r[e]] = {}), this.options.responsive[r[e]].autoHeight = "true" === this.$element.attr("data" + o[e] + "auto-height"))
            }, s.prototype.getOption = function (t) {
                var o, e;
                for (o in this.options.responsive)o <= n.innerWidth && (e = o);
                return null != this.options.responsive && null != this.options.responsive[e][t] ? this.options.responsive[e][t] : this.options[t]
            }, s
        }(), o.fn.extend({
            RDNavbar: function (t) {
                var e;
                return e = o(this), e.data("RDNavbar") ? void 0 : e.data("RDNavbar", new s(this, t))
            }
        }), n.RDNavbar = s
    }(window.jQuery, document, window), "undefined" != typeof module && null !== module ? module.exports = window.RDNavbar : "function" == typeof define && define.amd && define(["jquery"], function () {
            "use strict";
            return window.RDNavbar
        })
}).call(this);

/**
 * @module       WOW
 * @author       Matthieu Aussaguel
 * @version      v1.1.2
 * @license      MIT License
 */
(function () {
    var t, e, n, i, o, r = function (t, e) {
        return function () {
            return t.apply(e, arguments)
        }
    }, s = [].indexOf || function (t) {
            for (var e = 0, n = this.length; n > e; e++)if (e in this && this[e] === t)return e;
            return -1
        };
    e = function () {
        function t() {
        }

        return t.prototype.extend = function (t, e) {
            var n, i;
            for (n in e)i = e[n], null == t[n] && (t[n] = i);
            return t
        }, t.prototype.isMobile = function (t) {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(t)
        }, t.prototype.createEvent = function (t, e, n, i) {
            var o;
            return null == e && (e = !1), null == n && (n = !1), null == i && (i = null), null != document.createEvent ? (o = document.createEvent("CustomEvent"), o.initCustomEvent(t, e, n, i)) : null != document.createEventObject ? (o = document.createEventObject(), o.eventType = t) : o.eventName = t, o
        }, t.prototype.emitEvent = function (t, e) {
            return null != t.dispatchEvent ? t.dispatchEvent(e) : e in (null != t) ? t[e]() : "on" + e in (null != t) ? t["on" + e]() : void 0
        }, t.prototype.addEvent = function (t, e, n) {
            return null != t.addEventListener ? t.addEventListener(e, n, !1) : null != t.attachEvent ? t.attachEvent("on" + e, n) : t[e] = n
        }, t.prototype.removeEvent = function (t, e, n) {
            return null != t.removeEventListener ? t.removeEventListener(e, n, !1) : null != t.detachEvent ? t.detachEvent("on" + e, n) : delete t[e]
        }, t.prototype.innerHeight = function () {
            return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight
        }, t
    }(), n = this.WeakMap || this.MozWeakMap || (n = function () {
            function t() {
                this.keys = [], this.values = []
            }

            return t.prototype.get = function (t) {
                var e, n, i, o, r;
                for (r = this.keys, e = i = 0, o = r.length; o > i; e = ++i)if (n = r[e], n === t)return this.values[e]
            }, t.prototype.set = function (t, e) {
                var n, i, o, r, s;
                for (s = this.keys, n = o = 0, r = s.length; r > o; n = ++o)if (i = s[n], i === t)return void(this.values[n] = e);
                return this.keys.push(t), this.values.push(e)
            }, t
        }()), t = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (t = function () {
            function t() {
                "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")
            }

            return t.notSupported = !0, t.prototype.observe = function () {
            }, t
        }()), i = this.getComputedStyle || function (t) {
            return this.getPropertyValue = function (e) {
                var n;
                return "float" === e && (e = "styleFloat"), o.test(e) && e.replace(o, function (t, e) {
                    return e.toUpperCase()
                }), (null != (n = t.currentStyle) ? n[e] : void 0) || null
            }, this
        }, o = /(\-([a-z]){1})/g, this.WOW = function () {
        function o(t) {
            null == t && (t = {}), this.scrollCallback = r(this.scrollCallback, this), this.scrollHandler = r(this.scrollHandler, this), this.resetAnimation = r(this.resetAnimation, this), this.start = r(this.start, this), this.scrolled = !0, this.config = this.util().extend(t, this.defaults), null != t.scrollContainer && (this.config.scrollContainer = document.querySelector(t.scrollContainer)), this.animationNameCache = new n, this.wowEvent = this.util().createEvent(this.config.boxClass)
        }

        return o.prototype.defaults = {
            boxClass: "wow",
            animateClass: "animated",
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null,
            scrollContainer: null
        }, o.prototype.init = function () {
            var t;
            return this.element = window.document.documentElement, "interactive" === (t = document.readyState) || "complete" === t ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = []
        }, o.prototype.start = function () {
            var e, n, i, o;
            if (this.stopped = !1, this.boxes = function () {
                    var t, n, i, o;
                    for (i = this.element.querySelectorAll("." + this.config.boxClass), o = [], t = 0, n = i.length; n > t; t++)e = i[t], o.push(e);
                    return o
                }.call(this), this.all = function () {
                    var t, n, i, o;
                    for (i = this.boxes, o = [], t = 0, n = i.length; n > t; t++)e = i[t], o.push(e);
                    return o
                }.call(this), this.boxes.length)if (this.disabled()) this.resetStyle(); else for (o = this.boxes, n = 0, i = o.length; i > n; n++)e = o[n], this.applyStyle(e, !0);
            return this.disabled() || (this.util().addEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live ? new t(function (t) {
                return function (e) {
                    var n, i, o, r, s;
                    for (s = [], n = 0, i = e.length; i > n; n++)r = e[n], s.push(function () {
                        var t, e, n, i;
                        for (n = r.addedNodes || [], i = [], t = 0, e = n.length; e > t; t++)o = n[t], i.push(this.doSync(o));
                        return i
                    }.call(t));
                    return s
                }
            }(this)).observe(document.body, {childList: !0, subtree: !0}) : void 0
        }, o.prototype.stop = function () {
            return this.stopped = !0, this.util().removeEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval ? clearInterval(this.interval) : void 0
        }, o.prototype.sync = function () {
            return t.notSupported ? this.doSync(this.element) : void 0
        }, o.prototype.doSync = function (t) {
            var e, n, i, o, r;
            if (null == t && (t = this.element), 1 === t.nodeType) {
                for (t = t.parentNode || t, o = t.querySelectorAll("." + this.config.boxClass), r = [], n = 0, i = o.length; i > n; n++)e = o[n], s.call(this.all, e) < 0 ? (this.boxes.push(e), this.all.push(e), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(e, !0), r.push(this.scrolled = !0)) : r.push(void 0);
                return r
            }
        }, o.prototype.show = function (t) {
            return this.applyStyle(t), t.className = t.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(t), this.util().emitEvent(t, this.wowEvent), this.util().addEvent(t, "animationend", this.resetAnimation), this.util().addEvent(t, "oanimationend", this.resetAnimation), this.util().addEvent(t, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(t, "MSAnimationEnd", this.resetAnimation), t
        }, o.prototype.applyStyle = function (t, e) {
            var n, i, o;
            return i = t.getAttribute("data-wow-duration"), n = t.getAttribute("data-wow-delay"), o = t.getAttribute("data-wow-iteration"), this.animate(function (r) {
                return function () {
                    return r.customStyle(t, e, i, n, o)
                }
            }(this))
        }, o.prototype.animate = function () {
            return "requestAnimationFrame" in window ? function (t) {
                return window.requestAnimationFrame(t)
            } : function (t) {
                return t()
            }
        }(), o.prototype.resetStyle = function () {
            var t, e, n, i, o;
            for (i = this.boxes, o = [], e = 0, n = i.length; n > e; e++)t = i[e], o.push(t.style.visibility = "visible");
            return o
        }, o.prototype.resetAnimation = function (t) {
            var e;
            return t.type.toLowerCase().indexOf("animationend") >= 0 ? (e = t.target || t.srcElement, e.className = e.className.replace(this.config.animateClass, "").trim()) : void 0
        }, o.prototype.customStyle = function (t, e, n, i, o) {
            return e && this.cacheAnimationName(t), t.style.visibility = e ? "hidden" : "visible", n && this.vendorSet(t.style, {animationDuration: n}), i && this.vendorSet(t.style, {animationDelay: i}), o && this.vendorSet(t.style, {animationIterationCount: o}), this.vendorSet(t.style, {animationName: e ? "none" : this.cachedAnimationName(t)}), t
        }, o.prototype.vendors = ["moz", "webkit"], o.prototype.vendorSet = function (t, e) {
            var n, i, o, r;
            i = [];
            for (n in e)o = e[n], t["" + n] = o, i.push(function () {
                var e, i, s, l;
                for (s = this.vendors, l = [], e = 0, i = s.length; i > e; e++)r = s[e], l.push(t["" + r + n.charAt(0).toUpperCase() + n.substr(1)] = o);
                return l
            }.call(this));
            return i
        }, o.prototype.vendorCSS = function (t, e) {
            var n, o, r, s, l, a;
            for (l = i(t), s = l.getPropertyCSSValue(e), r = this.vendors, n = 0, o = r.length; o > n; n++)a = r[n], s = s || l.getPropertyCSSValue("-" + a + "-" + e);
            return s
        }, o.prototype.animationName = function (t) {
            var e;
            try {
                e = this.vendorCSS(t, "animation-name").cssText
            } catch (n) {
                e = i(t).getPropertyValue("animation-name")
            }
            return "none" === e ? "" : e
        }, o.prototype.cacheAnimationName = function (t) {
            return this.animationNameCache.set(t, this.animationName(t))
        }, o.prototype.cachedAnimationName = function (t) {
            return this.animationNameCache.get(t)
        }, o.prototype.scrollHandler = function () {
            return this.scrolled = !0
        }, o.prototype.scrollCallback = function () {
            var t;
            return !this.scrolled || (this.scrolled = !1, this.boxes = function () {
                var e, n, i, o;
                for (i = this.boxes, o = [], e = 0, n = i.length; n > e; e++)t = i[e], t && (this.isVisible(t) ? this.show(t) : o.push(t));
                return o
            }.call(this), this.boxes.length || this.config.live) ? void 0 : this.stop()
        }, o.prototype.offsetTop = function (t) {
            for (var e; void 0 === t.offsetTop;)t = t.parentNode;
            for (e = t.offsetTop; t = t.offsetParent;)e += t.offsetTop;
            return e
        }, o.prototype.isVisible = function (t) {
            var e, n, i, o, r;
            return n = t.getAttribute("data-wow-offset") || this.config.offset, r = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset, o = r + Math.min(this.element.clientHeight, this.util().innerHeight()) - n, i = this.offsetTop(t), e = i + t.clientHeight, o >= i && e >= r
        }, o.prototype.util = function () {
            return null != this._util ? this._util : this._util = new e
        }, o.prototype.disabled = function () {
            return !this.config.mobile && this.util().isMobile(navigator.userAgent)
        }, o
    }()
}).call(this);

/**
 * @module       Isotope PACKAGED
 * @version      v2.2.2
 * @license      GPLv3
 * @see          http://isotope.metafizzy.co
 */
!function (a) {
    function b() {
    }

    function c(a) {
        function c(b) {
            b.prototype.option || (b.prototype.option = function (b) {
                a.isPlainObject(b) && (this.options = a.extend(!0, this.options, b))
            })
        }

        function e(b, c) {
            a.fn[b] = function (e) {
                if ("string" == typeof e) {
                    for (var g = d.call(arguments, 1), h = 0, i = this.length; i > h; h++) {
                        var j = this[h], k = a.data(j, b);
                        if (k)if (a.isFunction(k[e]) && "_" !== e.charAt(0)) {
                            var l = k[e].apply(k, g);
                            if (void 0 !== l)return l
                        } else f("no such method '" + e + "' for " + b + " instance"); else f("cannot call methods on " + b + " prior to initialization; attempted to call '" + e + "'")
                    }
                    return this
                }
                return this.each(function () {
                    var d = a.data(this, b);
                    d ? (d.option(e), d._init()) : (d = new c(this, e), a.data(this, b, d))
                })
            }
        }

        if (a) {
            var f = "undefined" == typeof console ? b : function (a) {
                console.error(a)
            };
            return a.bridget = function (a, b) {
                c(b), e(a, b)
            }, a.bridget
        }
    }

    var d = Array.prototype.slice;
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], c) : c("object" == typeof exports ? require("jquery") : a.jQuery)
}(window), function (a) {
    function b(b) {
        var c = a.event;
        return c.target = c.target || c.srcElement || b, c
    }

    var c = document.documentElement, d = function () {
    };
    c.addEventListener ? d = function (a, b, c) {
        a.addEventListener(b, c, !1)
    } : c.attachEvent && (d = function (a, c, d) {
            a[c + d] = d.handleEvent ? function () {
                var c = b(a);
                d.handleEvent.call(d, c)
            } : function () {
                var c = b(a);
                d.call(a, c)
            }, a.attachEvent("on" + c, a[c + d])
        });
    var e = function () {
    };
    c.removeEventListener ? e = function (a, b, c) {
        a.removeEventListener(b, c, !1)
    } : c.detachEvent && (e = function (a, b, c) {
            a.detachEvent("on" + b, a[b + c]);
            try {
                delete a[b + c]
            } catch (d) {
                a[b + c] = void 0
            }
        });
    var f = {bind: d, unbind: e};
    "function" == typeof define && define.amd ? define("eventie/eventie", f) : "object" == typeof exports ? module.exports = f : a.eventie = f
}(window), function () {
    "use strict";
    function a() {
    }

    function b(a, b) {
        for (var c = a.length; c--;)if (a[c].listener === b)return c;
        return -1
    }

    function c(a) {
        return function () {
            return this[a].apply(this, arguments)
        }
    }

    var d = a.prototype, e = this, f = e.EventEmitter;
    d.getListeners = function (a) {
        var b, c, d = this._getEvents();
        if (a instanceof RegExp) {
            b = {};
            for (c in d)d.hasOwnProperty(c) && a.test(c) && (b[c] = d[c])
        } else b = d[a] || (d[a] = []);
        return b
    }, d.flattenListeners = function (a) {
        var b, c = [];
        for (b = 0; b < a.length; b += 1)c.push(a[b].listener);
        return c
    }, d.getListenersAsObject = function (a) {
        var b, c = this.getListeners(a);
        return c instanceof Array && (b = {}, b[a] = c), b || c
    }, d.addListener = function (a, c) {
        var d, e = this.getListenersAsObject(a), f = "object" == typeof c;
        for (d in e)e.hasOwnProperty(d) && -1 === b(e[d], c) && e[d].push(f ? c : {listener: c, once: !1});
        return this
    }, d.on = c("addListener"), d.addOnceListener = function (a, b) {
        return this.addListener(a, {listener: b, once: !0})
    }, d.once = c("addOnceListener"), d.defineEvent = function (a) {
        return this.getListeners(a), this
    }, d.defineEvents = function (a) {
        for (var b = 0; b < a.length; b += 1)this.defineEvent(a[b]);
        return this
    }, d.removeListener = function (a, c) {
        var d, e, f = this.getListenersAsObject(a);
        for (e in f)f.hasOwnProperty(e) && (d = b(f[e], c), -1 !== d && f[e].splice(d, 1));
        return this
    }, d.off = c("removeListener"), d.addListeners = function (a, b) {
        return this.manipulateListeners(!1, a, b)
    }, d.removeListeners = function (a, b) {
        return this.manipulateListeners(!0, a, b)
    }, d.manipulateListeners = function (a, b, c) {
        var d, e, f = a ? this.removeListener : this.addListener, g = a ? this.removeListeners : this.addListeners;
        if ("object" != typeof b || b instanceof RegExp)for (d = c.length; d--;)f.call(this, b, c[d]); else for (d in b)b.hasOwnProperty(d) && (e = b[d]) && ("function" == typeof e ? f.call(this, d, e) : g.call(this, d, e));
        return this
    }, d.removeEvent = function (a) {
        var b, c = typeof a, d = this._getEvents();
        if ("string" === c) delete d[a]; else if (a instanceof RegExp)for (b in d)d.hasOwnProperty(b) && a.test(b) && delete d[b]; else delete this._events;
        return this
    }, d.removeAllListeners = c("removeEvent"), d.emitEvent = function (a, b) {
        var c, d, e, f, g = this.getListenersAsObject(a);
        for (e in g)if (g.hasOwnProperty(e))for (d = g[e].length; d--;)c = g[e][d], c.once === !0 && this.removeListener(a, c.listener), f = c.listener.apply(this, b || []), f === this._getOnceReturnValue() && this.removeListener(a, c.listener);
        return this
    }, d.trigger = c("emitEvent"), d.emit = function (a) {
        var b = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(a, b)
    }, d.setOnceReturnValue = function (a) {
        return this._onceReturnValue = a, this
    }, d._getOnceReturnValue = function () {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }, d._getEvents = function () {
        return this._events || (this._events = {})
    }, a.noConflict = function () {
        return e.EventEmitter = f, a
    }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function () {
        return a
    }) : "object" == typeof module && module.exports ? module.exports = a : e.EventEmitter = a
}.call(this), function (a) {
    function b(a) {
        if (a) {
            if ("string" == typeof d[a])return a;
            a = a.charAt(0).toUpperCase() + a.slice(1);
            for (var b, e = 0, f = c.length; f > e; e++)if (b = c[e] + a, "string" == typeof d[b])return b
        }
    }

    var c = "Webkit Moz ms Ms O".split(" "), d = document.documentElement.style;
    "function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function () {
        return b
    }) : "object" == typeof exports ? module.exports = b : a.getStyleProperty = b
}(window), function (a, b) {
    function c(a) {
        var b = parseFloat(a), c = -1 === a.indexOf("%") && !isNaN(b);
        return c && b
    }

    function d() {
    }

    function e() {
        for (var a = {width: 0, height: 0, innerWidth: 0, innerHeight: 0, outerWidth: 0, outerHeight: 0}, b = 0,
                 c = h.length; c > b; b++) {
            var d = h[b];
            a[d] = 0
        }
        return a
    }

    function f(b) {
        function d() {
            if (!m) {
                m = !0;
                var d = a.getComputedStyle;
                if (j = function () {
                        var a = d ? function (a) {
                            return d(a, null)
                        } : function (a) {
                            return a.currentStyle
                        };
                        return function (b) {
                            var c = a(b);
                            return c || g("Style returned " + c + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), c
                        }
                    }(), k = b("boxSizing")) {
                    var e = document.createElement("div");
                    e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style[k] = "border-box";
                    var f = document.body || document.documentElement;
                    f.appendChild(e);
                    var h = j(e);
                    l = 200 === c(h.width), f.removeChild(e)
                }
            }
        }

        function f(a) {
            if (d(), "string" == typeof a && (a = document.querySelector(a)), a && "object" == typeof a && a.nodeType) {
                var b = j(a);
                if ("none" === b.display)return e();
                var f = {};
                f.width = a.offsetWidth, f.height = a.offsetHeight;
                for (var g = f.isBorderBox = !(!k || !b[k] || "border-box" !== b[k]), m = 0, n = h.length; n > m; m++) {
                    var o = h[m], p = b[o];
                    p = i(a, p);
                    var q = parseFloat(p);
                    f[o] = isNaN(q) ? 0 : q
                }
                var r = f.paddingLeft + f.paddingRight, s = f.paddingTop + f.paddingBottom,
                    t = f.marginLeft + f.marginRight, u = f.marginTop + f.marginBottom,
                    v = f.borderLeftWidth + f.borderRightWidth, w = f.borderTopWidth + f.borderBottomWidth, x = g && l,
                    y = c(b.width);
                y !== !1 && (f.width = y + (x ? 0 : r + v));
                var z = c(b.height);
                return z !== !1 && (f.height = z + (x ? 0 : s + w)), f.innerWidth = f.width - (r + v), f.innerHeight = f.height - (s + w), f.outerWidth = f.width + t, f.outerHeight = f.height + u, f
            }
        }

        function i(b, c) {
            if (a.getComputedStyle || -1 === c.indexOf("%"))return c;
            var d = b.style, e = d.left, f = b.runtimeStyle, g = f && f.left;
            return g && (f.left = b.currentStyle.left), d.left = c, c = d.pixelLeft, d.left = e, g && (f.left = g), c
        }

        var j, k, l, m = !1;
        return f
    }

    var g = "undefined" == typeof console ? d : function (a) {
            console.error(a)
        },
        h = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
    "function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], f) : "object" == typeof exports ? module.exports = f(require("desandro-get-style-property")) : a.getSize = f(a.getStyleProperty)
}(window), function (a) {
    function b(a) {
        "function" == typeof a && (b.isReady ? a() : g.push(a))
    }

    function c(a) {
        var c = "readystatechange" === a.type && "complete" !== f.readyState;
        b.isReady || c || d()
    }

    function d() {
        b.isReady = !0;
        for (var a = 0, c = g.length; c > a; a++) {
            var d = g[a];
            d()
        }
    }

    function e(e) {
        return "complete" === f.readyState ? d() : (e.bind(f, "DOMContentLoaded", c), e.bind(f, "readystatechange", c), e.bind(a, "load", c)), b
    }

    var f = a.document, g = [];
    b.isReady = !1, "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], e) : "object" == typeof exports ? module.exports = e(require("eventie")) : a.docReady = e(a.eventie)
}(window), function (a) {
    "use strict";
    function b(a, b) {
        return a[g](b)
    }

    function c(a) {
        if (!a.parentNode) {
            var b = document.createDocumentFragment();
            b.appendChild(a)
        }
    }

    function d(a, b) {
        c(a);
        for (var d = a.parentNode.querySelectorAll(b), e = 0, f = d.length; f > e; e++)if (d[e] === a)return !0;
        return !1
    }

    function e(a, d) {
        return c(a), b(a, d)
    }

    var f, g = function () {
        if (a.matches)return "matches";
        if (a.matchesSelector)return "matchesSelector";
        for (var b = ["webkit", "moz", "ms", "o"], c = 0, d = b.length; d > c; c++) {
            var e = b[c], f = e + "MatchesSelector";
            if (a[f])return f
        }
    }();
    if (g) {
        var h = document.createElement("div"), i = b(h, "div");
        f = i ? b : e
    } else f = d;
    "function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function () {
        return f
    }) : "object" == typeof exports ? module.exports = f : window.matchesSelector = f
}(Element.prototype), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["doc-ready/doc-ready", "matches-selector/matches-selector"], function (c, d) {
        return b(a, c, d)
    }) : "object" == typeof exports ? module.exports = b(a, require("doc-ready"), require("desandro-matches-selector")) : a.fizzyUIUtils = b(a, a.docReady, a.matchesSelector)
}(window, function (a, b, c) {
    var d = {};
    d.extend = function (a, b) {
        for (var c in b)a[c] = b[c];
        return a
    }, d.modulo = function (a, b) {
        return (a % b + b) % b
    };
    var e = Object.prototype.toString;
    d.isArray = function (a) {
        return "[object Array]" == e.call(a)
    }, d.makeArray = function (a) {
        var b = [];
        if (d.isArray(a)) b = a; else if (a && "number" == typeof a.length)for (var c = 0,
                                                                                    e = a.length; e > c; c++)b.push(a[c]); else b.push(a);
        return b
    }, d.indexOf = Array.prototype.indexOf ? function (a, b) {
        return a.indexOf(b)
    } : function (a, b) {
        for (var c = 0, d = a.length; d > c; c++)if (a[c] === b)return c;
        return -1
    }, d.removeFrom = function (a, b) {
        var c = d.indexOf(a, b);
        -1 != c && a.splice(c, 1)
    }, d.isElement = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function (a) {
        return a instanceof HTMLElement
    } : function (a) {
        return a && "object" == typeof a && 1 == a.nodeType && "string" == typeof a.nodeName
    }, d.setText = function () {
        function a(a, c) {
            b = b || (void 0 !== document.documentElement.textContent ? "textContent" : "innerText"), a[b] = c
        }

        var b;
        return a
    }(), d.getParent = function (a, b) {
        for (; a != document.body;)if (a = a.parentNode, c(a, b))return a
    }, d.getQueryElement = function (a) {
        return "string" == typeof a ? document.querySelector(a) : a
    }, d.handleEvent = function (a) {
        var b = "on" + a.type;
        this[b] && this[b](a)
    }, d.filterFindElements = function (a, b) {
        a = d.makeArray(a);
        for (var e = [], f = 0, g = a.length; g > f; f++) {
            var h = a[f];
            if (d.isElement(h))if (b) {
                c(h, b) && e.push(h);
                for (var i = h.querySelectorAll(b), j = 0, k = i.length; k > j; j++)e.push(i[j])
            } else e.push(h)
        }
        return e
    }, d.debounceMethod = function (a, b, c) {
        var d = a.prototype[b], e = b + "Timeout";
        a.prototype[b] = function () {
            var a = this[e];
            a && clearTimeout(a);
            var b = arguments, f = this;
            this[e] = setTimeout(function () {
                d.apply(f, b), delete f[e]
            }, c || 100)
        }
    }, d.toDashed = function (a) {
        return a.replace(/(.)([A-Z])/g, function (a, b, c) {
            return b + "-" + c
        }).toLowerCase()
    };
    var f = a.console;
    return d.htmlInit = function (c, e) {
        b(function () {
            for (var b = d.toDashed(e), g = document.querySelectorAll(".js-" + b), h = "data-" + b + "-options", i = 0,
                     j = g.length; j > i; i++) {
                var k, l = g[i], m = l.getAttribute(h);
                try {
                    k = m && JSON.parse(m)
                } catch (n) {
                    f && f.error("Error parsing " + h + " on " + l.nodeName.toLowerCase() + (l.id ? "#" + l.id : "") + ": " + n);
                    continue
                }
                var o = new c(l, k), p = a.jQuery;
                p && p.data(l, e, o)
            }
        })
    }, d
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property", "fizzy-ui-utils/utils"], function (c, d, e, f) {
        return b(a, c, d, e, f)
    }) : "object" == typeof exports ? module.exports = b(a, require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property"), require("fizzy-ui-utils")) : (a.Outlayer = {}, a.Outlayer.Item = b(a, a.EventEmitter, a.getSize, a.getStyleProperty, a.fizzyUIUtils))
}(window, function (a, b, c, d, e) {
    "use strict";
    function f(a) {
        for (var b in a)return !1;
        return b = null, !0
    }

    function g(a, b) {
        a && (this.element = a, this.layout = b, this.position = {x: 0, y: 0}, this._create())
    }

    function h(a) {
        return a.replace(/([A-Z])/g, function (a) {
            return "-" + a.toLowerCase()
        })
    }

    var i = a.getComputedStyle, j = i ? function (a) {
        return i(a, null)
    } : function (a) {
        return a.currentStyle
    }, k = d("transition"), l = d("transform"), m = k && l, n = !!d("perspective"), o = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "otransitionend",
        transition: "transitionend"
    }[k], p = ["transform", "transition", "transitionDuration", "transitionProperty"], q = function () {
        for (var a = {}, b = 0, c = p.length; c > b; b++) {
            var e = p[b], f = d(e);
            f && f !== e && (a[e] = f)
        }
        return a
    }();
    e.extend(g.prototype, b.prototype), g.prototype._create = function () {
        this._transn = {ingProperties: {}, clean: {}, onEnd: {}}, this.css({position: "absolute"})
    }, g.prototype.handleEvent = function (a) {
        var b = "on" + a.type;
        this[b] && this[b](a)
    }, g.prototype.getSize = function () {
        this.size = c(this.element)
    }, g.prototype.css = function (a) {
        var b = this.element.style;
        for (var c in a) {
            var d = q[c] || c;
            b[d] = a[c]
        }
    }, g.prototype.getPosition = function () {
        var a = j(this.element), b = this.layout.options, c = b.isOriginLeft, d = b.isOriginTop,
            e = a[c ? "left" : "right"], f = a[d ? "top" : "bottom"], g = this.layout.size,
            h = -1 != e.indexOf("%") ? parseFloat(e) / 100 * g.width : parseInt(e, 10),
            i = -1 != f.indexOf("%") ? parseFloat(f) / 100 * g.height : parseInt(f, 10);
        h = isNaN(h) ? 0 : h, i = isNaN(i) ? 0 : i, h -= c ? g.paddingLeft : g.paddingRight, i -= d ? g.paddingTop : g.paddingBottom, this.position.x = h, this.position.y = i
    }, g.prototype.layoutPosition = function () {
        var a = this.layout.size, b = this.layout.options, c = {}, d = b.isOriginLeft ? "paddingLeft" : "paddingRight",
            e = b.isOriginLeft ? "left" : "right", f = b.isOriginLeft ? "right" : "left", g = this.position.x + a[d];
        c[e] = this.getXValue(g), c[f] = "";
        var h = b.isOriginTop ? "paddingTop" : "paddingBottom", i = b.isOriginTop ? "top" : "bottom",
            j = b.isOriginTop ? "bottom" : "top", k = this.position.y + a[h];
        c[i] = this.getYValue(k), c[j] = "", this.css(c), this.emitEvent("layout", [this])
    }, g.prototype.getXValue = function (a) {
        var b = this.layout.options;
        return b.percentPosition && !b.isHorizontal ? a / this.layout.size.width * 100 + "%" : a + "px"
    }, g.prototype.getYValue = function (a) {
        var b = this.layout.options;
        return b.percentPosition && b.isHorizontal ? a / this.layout.size.height * 100 + "%" : a + "px"
    }, g.prototype._transitionTo = function (a, b) {
        this.getPosition();
        var c = this.position.x, d = this.position.y, e = parseInt(a, 10), f = parseInt(b, 10),
            g = e === this.position.x && f === this.position.y;
        if (this.setPosition(a, b), g && !this.isTransitioning)return void this.layoutPosition();
        var h = a - c, i = b - d, j = {};
        j.transform = this.getTranslate(h, i), this.transition({
            to: j,
            onTransitionEnd: {transform: this.layoutPosition},
            isCleaning: !0
        })
    }, g.prototype.getTranslate = function (a, b) {
        var c = this.layout.options;
        return a = c.isOriginLeft ? a : -a, b = c.isOriginTop ? b : -b, n ? "translate3d(" + a + "px, " + b + "px, 0)" : "translate(" + a + "px, " + b + "px)"
    }, g.prototype.goTo = function (a, b) {
        this.setPosition(a, b), this.layoutPosition()
    }, g.prototype.moveTo = m ? g.prototype._transitionTo : g.prototype.goTo, g.prototype.setPosition = function (a, b) {
        this.position.x = parseInt(a, 10), this.position.y = parseInt(b, 10)
    }, g.prototype._nonTransition = function (a) {
        this.css(a.to), a.isCleaning && this._removeStyles(a.to);
        for (var b in a.onTransitionEnd)a.onTransitionEnd[b].call(this)
    }, g.prototype._transition = function (a) {
        if (!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(a);
        var b = this._transn;
        for (var c in a.onTransitionEnd)b.onEnd[c] = a.onTransitionEnd[c];
        for (c in a.to)b.ingProperties[c] = !0, a.isCleaning && (b.clean[c] = !0);
        if (a.from) {
            this.css(a.from);
            var d = this.element.offsetHeight;
            d = null
        }
        this.enableTransition(a.to), this.css(a.to), this.isTransitioning = !0
    };
    var r = "opacity," + h(q.transform || "transform");
    g.prototype.enableTransition = function () {
        this.isTransitioning || (this.css({
            transitionProperty: r,
            transitionDuration: this.layout.options.transitionDuration
        }), this.element.addEventListener(o, this, !1))
    }, g.prototype.transition = g.prototype[k ? "_transition" : "_nonTransition"], g.prototype.onwebkitTransitionEnd = function (a) {
        this.ontransitionend(a)
    }, g.prototype.onotransitionend = function (a) {
        this.ontransitionend(a)
    };
    var s = {"-webkit-transform": "transform", "-moz-transform": "transform", "-o-transform": "transform"};
    g.prototype.ontransitionend = function (a) {
        if (a.target === this.element) {
            var b = this._transn, c = s[a.propertyName] || a.propertyName;
            if (delete b.ingProperties[c], f(b.ingProperties) && this.disableTransition(), c in b.clean && (this.element.style[a.propertyName] = "", delete b.clean[c]), c in b.onEnd) {
                var d = b.onEnd[c];
                d.call(this), delete b.onEnd[c]
            }
            this.emitEvent("transitionEnd", [this])
        }
    }, g.prototype.disableTransition = function () {
        this.removeTransitionStyles(), this.element.removeEventListener(o, this, !1), this.isTransitioning = !1
    }, g.prototype._removeStyles = function (a) {
        var b = {};
        for (var c in a)b[c] = "";
        this.css(b)
    };
    var t = {transitionProperty: "", transitionDuration: ""};
    return g.prototype.removeTransitionStyles = function () {
        this.css(t)
    }, g.prototype.removeElem = function () {
        this.element.parentNode.removeChild(this.element), this.css({display: ""}), this.emitEvent("remove", [this])
    }, g.prototype.remove = function () {
        if (!k || !parseFloat(this.layout.options.transitionDuration))return void this.removeElem();
        var a = this;
        this.once("transitionEnd", function () {
            a.removeElem()
        }), this.hide()
    }, g.prototype.reveal = function () {
        delete this.isHidden, this.css({display: ""});
        var a = this.layout.options, b = {}, c = this.getHideRevealTransitionEndProperty("visibleStyle");
        b[c] = this.onRevealTransitionEnd, this.transition({
            from: a.hiddenStyle,
            to: a.visibleStyle,
            isCleaning: !0,
            onTransitionEnd: b
        })
    }, g.prototype.onRevealTransitionEnd = function () {
        this.isHidden || this.emitEvent("reveal")
    }, g.prototype.getHideRevealTransitionEndProperty = function (a) {
        var b = this.layout.options[a];
        if (b.opacity)return "opacity";
        for (var c in b)return c
    }, g.prototype.hide = function () {
        this.isHidden = !0, this.css({display: ""});
        var a = this.layout.options, b = {}, c = this.getHideRevealTransitionEndProperty("hiddenStyle");
        b[c] = this.onHideTransitionEnd, this.transition({
            from: a.visibleStyle,
            to: a.hiddenStyle,
            isCleaning: !0,
            onTransitionEnd: b
        })
    }, g.prototype.onHideTransitionEnd = function () {
        this.isHidden && (this.css({display: "none"}), this.emitEvent("hide"))
    }, g.prototype.destroy = function () {
        this.css({position: "", left: "", right: "", top: "", bottom: "", transition: "", transform: ""})
    }, g
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("outlayer/outlayer", ["eventie/eventie", "eventEmitter/EventEmitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (c, d, e, f, g) {
        return b(a, c, d, e, f, g)
    }) : "object" == typeof exports ? module.exports = b(a, require("eventie"), require("wolfy87-eventemitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : a.Outlayer = b(a, a.eventie, a.EventEmitter, a.getSize, a.fizzyUIUtils, a.Outlayer.Item)
}(window, function (a, b, c, d, e, f) {
    "use strict";
    function g(a, b) {
        var c = e.getQueryElement(a);
        if (!c)return void(h && h.error("Bad element for " + this.constructor.namespace + ": " + (c || a)));
        this.element = c, i && (this.$element = i(this.element)), this.options = e.extend({}, this.constructor.defaults), this.option(b);
        var d = ++k;
        this.element.outlayerGUID = d, l[d] = this, this._create(), this.options.isInitLayout && this.layout()
    }

    var h = a.console, i = a.jQuery, j = function () {
    }, k = 0, l = {};
    return g.namespace = "outlayer", g.Item = f, g.defaults = {
        containerStyle: {position: "relative"},
        isInitLayout: !0,
        isOriginLeft: !0,
        isOriginTop: !0,
        isResizeBound: !0,
        isResizingContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {opacity: 0, transform: "scale(0.001)"},
        visibleStyle: {opacity: 1, transform: "scale(1)"}
    }, e.extend(g.prototype, c.prototype), g.prototype.option = function (a) {
        e.extend(this.options, a)
    }, g.prototype._create = function () {
        this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), e.extend(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize()
    }, g.prototype.reloadItems = function () {
        this.items = this._itemize(this.element.children)
    }, g.prototype._itemize = function (a) {
        for (var b = this._filterFindItemElements(a), c = this.constructor.Item, d = [], e = 0,
                 f = b.length; f > e; e++) {
            var g = b[e], h = new c(g, this);
            d.push(h)
        }
        return d
    }, g.prototype._filterFindItemElements = function (a) {
        return e.filterFindElements(a, this.options.itemSelector)
    }, g.prototype.getItemElements = function () {
        for (var a = [], b = 0, c = this.items.length; c > b; b++)a.push(this.items[b].element);
        return a
    }, g.prototype.layout = function () {
        this._resetLayout(), this._manageStamps();
        var a = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
        this.layoutItems(this.items, a), this._isLayoutInited = !0
    }, g.prototype._init = g.prototype.layout, g.prototype._resetLayout = function () {
        this.getSize()
    }, g.prototype.getSize = function () {
        this.size = d(this.element)
    }, g.prototype._getMeasurement = function (a, b) {
        var c, f = this.options[a];
        f ? ("string" == typeof f ? c = this.element.querySelector(f) : e.isElement(f) && (c = f), this[a] = c ? d(c)[b] : f) : this[a] = 0
    }, g.prototype.layoutItems = function (a, b) {
        a = this._getItemsForLayout(a), this._layoutItems(a, b), this._postLayout()
    }, g.prototype._getItemsForLayout = function (a) {
        for (var b = [], c = 0, d = a.length; d > c; c++) {
            var e = a[c];
            e.isIgnored || b.push(e)
        }
        return b
    }, g.prototype._layoutItems = function (a, b) {
        if (this._emitCompleteOnItems("layout", a), a && a.length) {
            for (var c = [], d = 0, e = a.length; e > d; d++) {
                var f = a[d], g = this._getItemLayoutPosition(f);
                g.item = f, g.isInstant = b || f.isLayoutInstant, c.push(g)
            }
            this._processLayoutQueue(c)
        }
    }, g.prototype._getItemLayoutPosition = function () {
        return {x: 0, y: 0}
    }, g.prototype._processLayoutQueue = function (a) {
        for (var b = 0, c = a.length; c > b; b++) {
            var d = a[b];
            this._positionItem(d.item, d.x, d.y, d.isInstant)
        }
    }, g.prototype._positionItem = function (a, b, c, d) {
        d ? a.goTo(b, c) : a.moveTo(b, c)
    }, g.prototype._postLayout = function () {
        this.resizeContainer()
    }, g.prototype.resizeContainer = function () {
        if (this.options.isResizingContainer) {
            var a = this._getContainerSize();
            a && (this._setContainerMeasure(a.width, !0), this._setContainerMeasure(a.height, !1))
        }
    }, g.prototype._getContainerSize = j, g.prototype._setContainerMeasure = function (a, b) {
        if (void 0 !== a) {
            var c = this.size;
            c.isBorderBox && (a += b ? c.paddingLeft + c.paddingRight + c.borderLeftWidth + c.borderRightWidth : c.paddingBottom + c.paddingTop + c.borderTopWidth + c.borderBottomWidth), a = Math.max(a, 0), this.element.style[b ? "width" : "height"] = a + "px"
        }
    }, g.prototype._emitCompleteOnItems = function (a, b) {
        function c() {
            e.dispatchEvent(a + "Complete", null, [b])
        }

        function d() {
            g++, g === f && c()
        }

        var e = this, f = b.length;
        if (!b || !f)return void c();
        for (var g = 0, h = 0, i = b.length; i > h; h++) {
            var j = b[h];
            j.once(a, d)
        }
    }, g.prototype.dispatchEvent = function (a, b, c) {
        var d = b ? [b].concat(c) : c;
        if (this.emitEvent(a, d), i)if (this.$element = this.$element || i(this.element), b) {
            var e = i.Event(b);
            e.type = a, this.$element.trigger(e, c)
        } else this.$element.trigger(a, c)
    }, g.prototype.ignore = function (a) {
        var b = this.getItem(a);
        b && (b.isIgnored = !0)
    }, g.prototype.unignore = function (a) {
        var b = this.getItem(a);
        b && delete b.isIgnored
    }, g.prototype.stamp = function (a) {
        if (a = this._find(a)) {
            this.stamps = this.stamps.concat(a);
            for (var b = 0, c = a.length; c > b; b++) {
                var d = a[b];
                this.ignore(d)
            }
        }
    }, g.prototype.unstamp = function (a) {
        if (a = this._find(a))for (var b = 0, c = a.length; c > b; b++) {
            var d = a[b];
            e.removeFrom(this.stamps, d), this.unignore(d)
        }
    }, g.prototype._find = function (a) {
        return a ? ("string" == typeof a && (a = this.element.querySelectorAll(a)), a = e.makeArray(a)) : void 0
    }, g.prototype._manageStamps = function () {
        if (this.stamps && this.stamps.length) {
            this._getBoundingRect();
            for (var a = 0, b = this.stamps.length; b > a; a++) {
                var c = this.stamps[a];
                this._manageStamp(c)
            }
        }
    }, g.prototype._getBoundingRect = function () {
        var a = this.element.getBoundingClientRect(), b = this.size;
        this._boundingRect = {
            left: a.left + b.paddingLeft + b.borderLeftWidth,
            top: a.top + b.paddingTop + b.borderTopWidth,
            right: a.right - (b.paddingRight + b.borderRightWidth),
            bottom: a.bottom - (b.paddingBottom + b.borderBottomWidth)
        }
    }, g.prototype._manageStamp = j, g.prototype._getElementOffset = function (a) {
        var b = a.getBoundingClientRect(), c = this._boundingRect, e = d(a), f = {
            left: b.left - c.left - e.marginLeft,
            top: b.top - c.top - e.marginTop,
            right: c.right - b.right - e.marginRight,
            bottom: c.bottom - b.bottom - e.marginBottom
        };
        return f
    }, g.prototype.handleEvent = function (a) {
        var b = "on" + a.type;
        this[b] && this[b](a)
    }, g.prototype.bindResize = function () {
        this.isResizeBound || (b.bind(a, "resize", this), this.isResizeBound = !0)
    }, g.prototype.unbindResize = function () {
        this.isResizeBound && b.unbind(a, "resize", this), this.isResizeBound = !1
    }, g.prototype.onresize = function () {
        function a() {
            b.resize(), delete b.resizeTimeout
        }

        this.resizeTimeout && clearTimeout(this.resizeTimeout);
        var b = this;
        this.resizeTimeout = setTimeout(a, 100)
    }, g.prototype.resize = function () {
        this.isResizeBound && this.needsResizeLayout() && this.layout()
    }, g.prototype.needsResizeLayout = function () {
        var a = d(this.element), b = this.size && a;
        return b && a.innerWidth !== this.size.innerWidth
    }, g.prototype.addItems = function (a) {
        var b = this._itemize(a);
        return b.length && (this.items = this.items.concat(b)), b
    }, g.prototype.appended = function (a) {
        var b = this.addItems(a);
        b.length && (this.layoutItems(b, !0), this.reveal(b))
    }, g.prototype.prepended = function (a) {
        var b = this._itemize(a);
        if (b.length) {
            var c = this.items.slice(0);
            this.items = b.concat(c), this._resetLayout(), this._manageStamps(), this.layoutItems(b, !0), this.reveal(b), this.layoutItems(c)
        }
    }, g.prototype.reveal = function (a) {
        this._emitCompleteOnItems("reveal", a);
        for (var b = a && a.length, c = 0; b && b > c; c++) {
            var d = a[c];
            d.reveal()
        }
    }, g.prototype.hide = function (a) {
        this._emitCompleteOnItems("hide", a);
        for (var b = a && a.length, c = 0; b && b > c; c++) {
            var d = a[c];
            d.hide()
        }
    }, g.prototype.revealItemElements = function (a) {
        var b = this.getItems(a);
        this.reveal(b)
    }, g.prototype.hideItemElements = function (a) {
        var b = this.getItems(a);
        this.hide(b)
    }, g.prototype.getItem = function (a) {
        for (var b = 0, c = this.items.length; c > b; b++) {
            var d = this.items[b];
            if (d.element === a)return d
        }
    }, g.prototype.getItems = function (a) {
        a = e.makeArray(a);
        for (var b = [], c = 0, d = a.length; d > c; c++) {
            var f = a[c], g = this.getItem(f);
            g && b.push(g)
        }
        return b
    }, g.prototype.remove = function (a) {
        var b = this.getItems(a);
        if (this._emitCompleteOnItems("remove", b), b && b.length)for (var c = 0, d = b.length; d > c; c++) {
            var f = b[c];
            f.remove(), e.removeFrom(this.items, f)
        }
    }, g.prototype.destroy = function () {
        var a = this.element.style;
        a.height = "", a.position = "", a.width = "";
        for (var b = 0, c = this.items.length; c > b; b++) {
            var d = this.items[b];
            d.destroy()
        }
        this.unbindResize();
        var e = this.element.outlayerGUID;
        delete l[e], delete this.element.outlayerGUID, i && i.removeData(this.element, this.constructor.namespace)
    }, g.data = function (a) {
        a = e.getQueryElement(a);
        var b = a && a.outlayerGUID;
        return b && l[b]
    }, g.create = function (a, b) {
        function c() {
            g.apply(this, arguments)
        }

        return Object.create ? c.prototype = Object.create(g.prototype) : e.extend(c.prototype, g.prototype), c.prototype.constructor = c, c.defaults = e.extend({}, g.defaults), e.extend(c.defaults, b), c.prototype.settings = {}, c.namespace = a, c.data = g.data, c.Item = function () {
            f.apply(this, arguments)
        }, c.Item.prototype = new f, e.htmlInit(c, a), i && i.bridget && i.bridget(a, c), c
    }, g.Item = f, g
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/item", ["outlayer/outlayer"], b) : "object" == typeof exports ? module.exports = b(require("outlayer")) : (a.Isotope = a.Isotope || {}, a.Isotope.Item = b(a.Outlayer))
}(window, function (a) {
    "use strict";
    function b() {
        a.Item.apply(this, arguments)
    }

    b.prototype = new a.Item, b.prototype._create = function () {
        this.id = this.layout.itemGUID++, a.Item.prototype._create.call(this), this.sortData = {}
    }, b.prototype.updateSortData = function () {
        if (!this.isIgnored) {
            this.sortData.id = this.id, this.sortData["original-order"] = this.id, this.sortData.random = Math.random();
            var a = this.layout.options.getSortData, b = this.layout._sorters;
            for (var c in a) {
                var d = b[c];
                this.sortData[c] = d(this.element, this)
            }
        }
    };
    var c = b.prototype.destroy;
    return b.prototype.destroy = function () {
        c.apply(this, arguments), this.css({display: ""})
    }, b
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/layout-mode", ["get-size/get-size", "outlayer/outlayer"], b) : "object" == typeof exports ? module.exports = b(require("get-size"), require("outlayer")) : (a.Isotope = a.Isotope || {}, a.Isotope.LayoutMode = b(a.getSize, a.Outlayer))
}(window, function (a, b) {
    "use strict";
    function c(a) {
        this.isotope = a, a && (this.options = a.options[this.namespace], this.element = a.element, this.items = a.filteredItems, this.size = a.size)
    }

    return function () {
        function a(a) {
            return function () {
                return b.prototype[a].apply(this.isotope, arguments)
            }
        }

        for (var d = ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout"],
                 e = 0, f = d.length; f > e; e++) {
            var g = d[e];
            c.prototype[g] = a(g)
        }
    }(), c.prototype.needsVerticalResizeLayout = function () {
        var b = a(this.isotope.element), c = this.isotope.size && b;
        return c && b.innerHeight != this.isotope.size.innerHeight
    }, c.prototype._getMeasurement = function () {
        this.isotope._getMeasurement.apply(this, arguments)
    }, c.prototype.getColumnWidth = function () {
        this.getSegmentSize("column", "Width")
    }, c.prototype.getRowHeight = function () {
        this.getSegmentSize("row", "Height")
    }, c.prototype.getSegmentSize = function (a, b) {
        var c = a + b, d = "outer" + b;
        if (this._getMeasurement(c, d), !this[c]) {
            var e = this.getFirstItemSize();
            this[c] = e && e[d] || this.isotope.size["inner" + b]
        }
    }, c.prototype.getFirstItemSize = function () {
        var b = this.isotope.filteredItems[0];
        return b && b.element && a(b.element)
    }, c.prototype.layout = function () {
        this.isotope.layout.apply(this.isotope, arguments)
    }, c.prototype.getSize = function () {
        this.isotope.getSize(), this.size = this.isotope.size
    }, c.modes = {}, c.create = function (a, b) {
        function d() {
            c.apply(this, arguments)
        }

        return d.prototype = new c, b && (d.options = b), d.prototype.namespace = a, c.modes[a] = d, d
    }, c
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("masonry/masonry", ["outlayer/outlayer", "get-size/get-size", "fizzy-ui-utils/utils"], b) : "object" == typeof exports ? module.exports = b(require("outlayer"), require("get-size"), require("fizzy-ui-utils")) : a.Masonry = b(a.Outlayer, a.getSize, a.fizzyUIUtils)
}(window, function (a, b, c) {
    var d = a.create("masonry");
    return d.prototype._resetLayout = function () {
        this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns();
        var a = this.cols;
        for (this.colYs = []; a--;)this.colYs.push(0);
        this.maxY = 0
    }, d.prototype.measureColumns = function () {
        if (this.getContainerWidth(), !this.columnWidth) {
            var a = this.items[0], c = a && a.element;
            this.columnWidth = c && b(c).outerWidth || this.containerWidth
        }
        var d = this.columnWidth += this.gutter, e = this.containerWidth + this.gutter, f = e / d, g = d - e % d,
            h = g && 1 > g ? "round" : "floor";
        f = Math[h](f), this.cols = Math.max(f, 1)
    }, d.prototype.getContainerWidth = function () {
        var a = this.options.isFitWidth ? this.element.parentNode : this.element, c = b(a);
        this.containerWidth = c && c.innerWidth
    }, d.prototype._getItemLayoutPosition = function (a) {
        a.getSize();
        var b = a.size.outerWidth % this.columnWidth, d = b && 1 > b ? "round" : "ceil",
            e = Math[d](a.size.outerWidth / this.columnWidth);
        e = Math.min(e, this.cols);
        for (var f = this._getColGroup(e), g = Math.min.apply(Math, f), h = c.indexOf(f, g),
                 i = {x: this.columnWidth * h, y: g}, j = g + a.size.outerHeight, k = this.cols + 1 - f.length,
                 l = 0; k > l; l++)this.colYs[h + l] = j;
        return i
    }, d.prototype._getColGroup = function (a) {
        if (2 > a)return this.colYs;
        for (var b = [], c = this.cols + 1 - a, d = 0; c > d; d++) {
            var e = this.colYs.slice(d, d + a);
            b[d] = Math.max.apply(Math, e)
        }
        return b
    }, d.prototype._manageStamp = function (a) {
        var c = b(a), d = this._getElementOffset(a), e = this.options.isOriginLeft ? d.left : d.right,
            f = e + c.outerWidth, g = Math.floor(e / this.columnWidth);
        g = Math.max(0, g);
        var h = Math.floor(f / this.columnWidth);
        h -= f % this.columnWidth ? 0 : 1, h = Math.min(this.cols - 1, h);
        for (var i = (this.options.isOriginTop ? d.top : d.bottom) + c.outerHeight,
                 j = g; h >= j; j++)this.colYs[j] = Math.max(i, this.colYs[j])
    }, d.prototype._getContainerSize = function () {
        this.maxY = Math.max.apply(Math, this.colYs);
        var a = {height: this.maxY};
        return this.options.isFitWidth && (a.width = this._getContainerFitWidth()), a
    }, d.prototype._getContainerFitWidth = function () {
        for (var a = 0, b = this.cols; --b && 0 === this.colYs[b];)a++;
        return (this.cols - a) * this.columnWidth - this.gutter
    }, d.prototype.needsResizeLayout = function () {
        var a = this.containerWidth;
        return this.getContainerWidth(), a !== this.containerWidth
    }, d
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/layout-modes/masonry", ["../layout-mode", "masonry/masonry"], b) : "object" == typeof exports ? module.exports = b(require("../layout-mode"), require("masonry-layout")) : b(a.Isotope.LayoutMode, a.Masonry)
}(window, function (a, b) {
    "use strict";
    function c(a, b) {
        for (var c in b)a[c] = b[c];
        return a
    }

    var d = a.create("masonry"), e = d.prototype._getElementOffset, f = d.prototype.layout,
        g = d.prototype._getMeasurement;
    c(d.prototype, b.prototype), d.prototype._getElementOffset = e, d.prototype.layout = f, d.prototype._getMeasurement = g;
    var h = d.prototype.measureColumns;
    d.prototype.measureColumns = function () {
        this.items = this.isotope.filteredItems, h.call(this)
    };
    var i = d.prototype._manageStamp;
    return d.prototype._manageStamp = function () {
        this.options.isOriginLeft = this.isotope.options.isOriginLeft, this.options.isOriginTop = this.isotope.options.isOriginTop, i.apply(this, arguments)
    }, d
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/layout-modes/fit-rows", ["../layout-mode"], b) : "object" == typeof exports ? module.exports = b(require("../layout-mode")) : b(a.Isotope.LayoutMode)
}(window, function (a) {
    "use strict";
    var b = a.create("fitRows");
    return b.prototype._resetLayout = function () {
        this.x = 0, this.y = 0, this.maxY = 0, this._getMeasurement("gutter", "outerWidth")
    }, b.prototype._getItemLayoutPosition = function (a) {
        a.getSize();
        var b = a.size.outerWidth + this.gutter, c = this.isotope.size.innerWidth + this.gutter;
        0 !== this.x && b + this.x > c && (this.x = 0, this.y = this.maxY);
        var d = {x: this.x, y: this.y};
        return this.maxY = Math.max(this.maxY, this.y + a.size.outerHeight), this.x += b, d
    }, b.prototype._getContainerSize = function () {
        return {height: this.maxY}
    }, b
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/layout-modes/vertical", ["../layout-mode"], b) : "object" == typeof exports ? module.exports = b(require("../layout-mode")) : b(a.Isotope.LayoutMode)
}(window, function (a) {
    "use strict";
    var b = a.create("vertical", {horizontalAlignment: 0});
    return b.prototype._resetLayout = function () {
        this.y = 0
    }, b.prototype._getItemLayoutPosition = function (a) {
        a.getSize();
        var b = (this.isotope.size.innerWidth - a.size.outerWidth) * this.options.horizontalAlignment, c = this.y;
        return this.y += a.size.outerHeight, {x: b, y: c}
    }, b.prototype._getContainerSize = function () {
        return {height: this.y}
    }, b
}), function (a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "matches-selector/matches-selector", "fizzy-ui-utils/utils", "isotope/js/item", "isotope/js/layout-mode", "isotope/js/layout-modes/masonry", "isotope/js/layout-modes/fit-rows", "isotope/js/layout-modes/vertical"], function (c, d, e, f, g, h) {
        return b(a, c, d, e, f, g, h)
    }) : "object" == typeof exports ? module.exports = b(a, require("outlayer"), require("get-size"), require("desandro-matches-selector"), require("fizzy-ui-utils"), require("./item"), require("./layout-mode"), require("./layout-modes/masonry"), require("./layout-modes/fit-rows"), require("./layout-modes/vertical")) : a.Isotope = b(a, a.Outlayer, a.getSize, a.matchesSelector, a.fizzyUIUtils, a.Isotope.Item, a.Isotope.LayoutMode)
}(window, function (a, b, c, d, e, f, g) {
    function h(a, b) {
        return function (c, d) {
            for (var e = 0, f = a.length; f > e; e++) {
                var g = a[e], h = c.sortData[g], i = d.sortData[g];
                if (h > i || i > h) {
                    var j = void 0 !== b[g] ? b[g] : b, k = j ? 1 : -1;
                    return (h > i ? 1 : -1) * k
                }
            }
            return 0
        }
    }

    var i = a.jQuery, j = String.prototype.trim ? function (a) {
        return a.trim()
    } : function (a) {
        return a.replace(/^\s+|\s+$/g, "")
    }, k = document.documentElement, l = k.textContent ? function (a) {
        return a.textContent
    } : function (a) {
        return a.innerText
    }, m = b.create("isotope", {layoutMode: "masonry", isJQueryFiltering: !0, sortAscending: !0});
    m.Item = f, m.LayoutMode = g, m.prototype._create = function () {
        this.itemGUID = 0, this._sorters = {}, this._getSorters(), b.prototype._create.call(this), this.modes = {}, this.filteredItems = this.items, this.sortHistory = ["original-order"];
        for (var a in g.modes)this._initLayoutMode(a)
    }, m.prototype.reloadItems = function () {
        this.itemGUID = 0, b.prototype.reloadItems.call(this)
    }, m.prototype._itemize = function () {
        for (var a = b.prototype._itemize.apply(this, arguments), c = 0, d = a.length; d > c; c++) {
            var e = a[c];
            e.id = this.itemGUID++
        }
        return this._updateItemsSortData(a), a
    }, m.prototype._initLayoutMode = function (a) {
        var b = g.modes[a], c = this.options[a] || {};
        this.options[a] = b.options ? e.extend(b.options, c) : c, this.modes[a] = new b(this)
    }, m.prototype.layout = function () {
        return !this._isLayoutInited && this.options.isInitLayout ? void this.arrange() : void this._layout()
    }, m.prototype._layout = function () {
        var a = this._getIsInstant();
        this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, a), this._isLayoutInited = !0
    }, m.prototype.arrange = function (a) {
        function b() {
            d.reveal(c.needReveal), d.hide(c.needHide)
        }

        this.option(a), this._getIsInstant();
        var c = this._filter(this.items);
        this.filteredItems = c.matches;
        var d = this;
        this._bindArrangeComplete(), this._isInstant ? this._noTransition(b) : b(), this._sort(), this._layout()
    }, m.prototype._init = m.prototype.arrange, m.prototype._getIsInstant = function () {
        var a = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
        return this._isInstant = a, a
    }, m.prototype._bindArrangeComplete = function () {
        function a() {
            b && c && d && e.dispatchEvent("arrangeComplete", null, [e.filteredItems])
        }

        var b, c, d, e = this;
        this.once("layoutComplete", function () {
            b = !0, a()
        }), this.once("hideComplete", function () {
            c = !0, a()
        }), this.once("revealComplete", function () {
            d = !0, a()
        })
    }, m.prototype._filter = function (a) {
        var b = this.options.filter;
        b = b || "*";
        for (var c = [], d = [], e = [], f = this._getFilterTest(b), g = 0, h = a.length; h > g; g++) {
            var i = a[g];
            if (!i.isIgnored) {
                var j = f(i);
                j && c.push(i), j && i.isHidden ? d.push(i) : j || i.isHidden || e.push(i)
            }
        }
        return {matches: c, needReveal: d, needHide: e}
    }, m.prototype._getFilterTest = function (a) {
        return i && this.options.isJQueryFiltering ? function (b) {
            return i(b.element).is(a)
        } : "function" == typeof a ? function (b) {
            return a(b.element)
        } : function (b) {
            return d(b.element, a)
        }
    }, m.prototype.updateSortData = function (a) {
        var b;
        a ? (a = e.makeArray(a), b = this.getItems(a)) : b = this.items, this._getSorters(), this._updateItemsSortData(b)
    }, m.prototype._getSorters = function () {
        var a = this.options.getSortData;
        for (var b in a) {
            var c = a[b];
            this._sorters[b] = n(c)
        }
    }, m.prototype._updateItemsSortData = function (a) {
        for (var b = a && a.length, c = 0; b && b > c; c++) {
            var d = a[c];
            d.updateSortData()
        }
    };
    var n = function () {
        function a(a) {
            if ("string" != typeof a)return a;
            var c = j(a).split(" "), d = c[0], e = d.match(/^\[(.+)\]$/), f = e && e[1], g = b(f, d),
                h = m.sortDataParsers[c[1]];
            return a = h ? function (a) {
                return a && h(g(a))
            } : function (a) {
                return a && g(a)
            }
        }

        function b(a, b) {
            var c;
            return c = a ? function (b) {
                return b.getAttribute(a)
            } : function (a) {
                var c = a.querySelector(b);
                return c && l(c)
            }
        }

        return a
    }();
    m.sortDataParsers = {
        parseInt: function (a) {
            return parseInt(a, 10)
        }, parseFloat: function (a) {
            return parseFloat(a)
        }
    }, m.prototype._sort = function () {
        var a = this.options.sortBy;
        if (a) {
            var b = [].concat.apply(a, this.sortHistory), c = h(b, this.options.sortAscending);
            this.filteredItems.sort(c), a != this.sortHistory[0] && this.sortHistory.unshift(a)
        }
    }, m.prototype._mode = function () {
        var a = this.options.layoutMode, b = this.modes[a];
        if (!b)throw new Error("No layout mode: " + a);
        return b.options = this.options[a], b
    }, m.prototype._resetLayout = function () {
        b.prototype._resetLayout.call(this), this._mode()._resetLayout()
    }, m.prototype._getItemLayoutPosition = function (a) {
        return this._mode()._getItemLayoutPosition(a)
    }, m.prototype._manageStamp = function (a) {
        this._mode()._manageStamp(a)
    }, m.prototype._getContainerSize = function () {
        return this._mode()._getContainerSize()
    }, m.prototype.needsResizeLayout = function () {
        return this._mode().needsResizeLayout()
    }, m.prototype.appended = function (a) {
        var b = this.addItems(a);
        if (b.length) {
            var c = this._filterRevealAdded(b);
            this.filteredItems = this.filteredItems.concat(c)
        }
    }, m.prototype.prepended = function (a) {
        var b = this._itemize(a);
        if (b.length) {
            this._resetLayout(), this._manageStamps();
            var c = this._filterRevealAdded(b);
            this.layoutItems(this.filteredItems), this.filteredItems = c.concat(this.filteredItems), this.items = b.concat(this.items)
        }
    }, m.prototype._filterRevealAdded = function (a) {
        var b = this._filter(a);
        return this.hide(b.needHide), this.reveal(b.matches), this.layoutItems(b.matches, !0), b.matches
    }, m.prototype.insert = function (a) {
        var b = this.addItems(a);
        if (b.length) {
            var c, d, e = b.length;
            for (c = 0; e > c; c++)d = b[c], this.element.appendChild(d.element);
            var f = this._filter(b).matches;
            for (c = 0; e > c; c++)b[c].isLayoutInstant = !0;
            for (this.arrange(), c = 0; e > c; c++)delete b[c].isLayoutInstant;
            this.reveal(f)
        }
    };
    var o = m.prototype.remove;
    return m.prototype.remove = function (a) {
        a = e.makeArray(a);
        var b = this.getItems(a);
        o.call(this, a);
        var c = b && b.length;
        if (c)for (var d = 0; c > d; d++) {
            var f = b[d];
            e.removeFrom(this.filteredItems, f)
        }
    }, m.prototype.shuffle = function () {
        for (var a = 0, b = this.items.length; b > a; a++) {
            var c = this.items[a];
            c.sortData.random = Math.random()
        }
        this.options.sortBy = "random", this._sort(), this._layout()
    }, m.prototype._noTransition = function (a) {
        var b = this.options.transitionDuration;
        this.options.transitionDuration = 0;
        var c = a.call(this);
        return this.options.transitionDuration = b, c
    }, m.prototype.getFilteredItemElements = function () {
        for (var a = [], b = 0, c = this.filteredItems.length; c > b; b++)a.push(this.filteredItems[b].element);
        return a
    }, m
});

/*! PhotoSwipe - v4.1.1 - 2015-12-24
 * http://photoswipe.com
 * Copyright (c) 2015 Dmitry Semenov; */
!function (a, b) {
    "function" == typeof define && define.amd ? define(b) : "object" == typeof exports ? module.exports = b() : a.PhotoSwipe = b()
}(this, function () {
    "use strict";
    var a = function (a, b, c, d) {
        var e = {
            features: null, bind: function (a, b, c, d) {
                var e = (d ? "remove" : "add") + "EventListener";
                b = b.split(" ");
                for (var f = 0; f < b.length; f++)b[f] && a[e](b[f], c, !1)
            }, isArray: function (a) {
                return a instanceof Array
            }, createEl: function (a, b) {
                var c = document.createElement(b || "div");
                return a && (c.className = a), c
            }, getScrollY: function () {
                var a = window.pageYOffset;
                return void 0 !== a ? a : document.documentElement.scrollTop
            }, unbind: function (a, b, c) {
                e.bind(a, b, c, !0)
            }, removeClass: function (a, b) {
                var c = new RegExp("(\\s|^)" + b + "(\\s|$)");
                a.className = a.className.replace(c, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "")
            }, addClass: function (a, b) {
                e.hasClass(a, b) || (a.className += (a.className ? " " : "") + b)
            }, hasClass: function (a, b) {
                return a.className && new RegExp("(^|\\s)" + b + "(\\s|$)").test(a.className)
            }, getChildByClass: function (a, b) {
                for (var c = a.firstChild; c;) {
                    if (e.hasClass(c, b))return c;
                    c = c.nextSibling
                }
            }, arraySearch: function (a, b, c) {
                for (var d = a.length; d--;)if (a[d][c] === b)return d;
                return -1
            }, extend: function (a, b, c) {
                for (var d in b)if (b.hasOwnProperty(d)) {
                    if (c && a.hasOwnProperty(d))continue;
                    a[d] = b[d]
                }
            }, easing: {
                sine: {
                    out: function (a) {
                        return Math.sin(a * (Math.PI / 2))
                    }, inOut: function (a) {
                        return -(Math.cos(Math.PI * a) - 1) / 2
                    }
                }, cubic: {
                    out: function (a) {
                        return --a * a * a + 1
                    }
                }
            }, detectFeatures: function () {
                if (e.features)return e.features;
                var a = e.createEl(), b = a.style, c = "", d = {};
                if (d.oldIE = document.all && !document.addEventListener, d.touch = "ontouchstart" in window, window.requestAnimationFrame && (d.raf = window.requestAnimationFrame, d.caf = window.cancelAnimationFrame), d.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !d.pointerEvent) {
                    var f = navigator.userAgent;
                    if (/iP(hone|od)/.test(navigator.platform)) {
                        var g = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
                        g && g.length > 0 && (g = parseInt(g[1], 10), g >= 1 && 8 > g && (d.isOldIOSPhone = !0))
                    }
                    var h = f.match(/Android\s([0-9\.]*)/), i = h ? h[1] : 0;
                    i = parseFloat(i), i >= 1 && (4.4 > i && (d.isOldAndroid = !0), d.androidVersion = i), d.isMobileOpera = /opera mini|opera mobi/i.test(f)
                }
                for (var j, k, l = ["transform", "perspective", "animationName"], m = ["", "webkit", "Moz", "ms", "O"],
                         n = 0; 4 > n; n++) {
                    c = m[n];
                    for (var o = 0; 3 > o; o++)j = l[o], k = c + (c ? j.charAt(0).toUpperCase() + j.slice(1) : j), !d[j] && k in b && (d[j] = k);
                    c && !d.raf && (c = c.toLowerCase(), d.raf = window[c + "RequestAnimationFrame"], d.raf && (d.caf = window[c + "CancelAnimationFrame"] || window[c + "CancelRequestAnimationFrame"]))
                }
                if (!d.raf) {
                    var p = 0;
                    d.raf = function (a) {
                        var b = (new Date).getTime(), c = Math.max(0, 16 - (b - p)), d = window.setTimeout(function () {
                            a(b + c)
                        }, c);
                        return p = b + c, d
                    }, d.caf = function (a) {
                        clearTimeout(a)
                    }
                }
                return d.svg = !!document.createElementNS && !!document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, e.features = d, d
            }
        };
        e.detectFeatures(), e.features.oldIE && (e.bind = function (a, b, c, d) {
            b = b.split(" ");
            for (var e, f = (d ? "detach" : "attach") + "Event", g = function () {
                c.handleEvent.call(c)
            }, h = 0; h < b.length; h++)if (e = b[h])if ("object" == typeof c && c.handleEvent) {
                if (d) {
                    if (!c["oldIE" + e])return !1
                } else c["oldIE" + e] = g;
                a[f]("on" + e, c["oldIE" + e])
            } else a[f]("on" + e, c)
        });
        var f = this, g = 25, h = 3, i = {
            allowPanToNext: !0,
            spacing: .12,
            bgOpacity: 1,
            mouseUsed: !1,
            loop: !0,
            pinchToClose: !0,
            closeOnScroll: !0,
            closeOnVerticalDrag: !0,
            verticalDragRange: .75,
            hideAnimationDuration: 333,
            showAnimationDuration: 333,
            showHideOpacity: !1,
            focus: !0,
            escKey: !0,
            arrowKeys: !0,
            mainScrollEndFriction: .35,
            panEndFriction: .35,
            isClickableElement: function (a) {
                return "A" === a.tagName
            },
            getDoubleTapZoom: function (a, b) {
                return a ? 1 : b.initialZoomLevel < .7 ? 1 : 1.33
            },
            maxSpreadZoom: 1.33,
            modal: !0,
            scaleMode: "fit"
        };
        e.extend(i, d);
        var j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S,
            T, U, V, W, X, Y, Z, $, _, aa, ba, ca, da, ea, fa, ga, ha, ia, ja, ka, la = function () {
                return {x: 0, y: 0}
            }, ma = la(), na = la(), oa = la(), pa = {}, qa = 0, ra = {}, sa = la(), ta = 0, ua = !0, va = [], wa = {},
            xa = !1, ya = function (a, b) {
                e.extend(f, b.publicMethods), va.push(a)
            }, za = function (a) {
                var b = _b();
                return a > b - 1 ? a - b : 0 > a ? b + a : a
            }, Aa = {}, Ba = function (a, b) {
                return Aa[a] || (Aa[a] = []), Aa[a].push(b)
            }, Ca = function (a) {
                var b = Aa[a];
                if (b) {
                    var c = Array.prototype.slice.call(arguments);
                    c.shift();
                    for (var d = 0; d < b.length; d++)b[d].apply(f, c)
                }
            }, Da = function () {
                return (new Date).getTime()
            }, Ea = function (a) {
                ia = a, f.bg.style.opacity = a * i.bgOpacity
            }, Fa = function (a, b, c, d, e) {
                (!xa || e && e !== f.currItem) && (d /= e ? e.fitRatio : f.currItem.fitRatio), a[E] = u + b + "px, " + c + "px" + v + " scale(" + d + ")"
            }, Ga = function (a) {
                da && (a && (s > f.currItem.fitRatio ? xa || (lc(f.currItem, !1, !0), xa = !0) : xa && (lc(f.currItem), xa = !1)), Fa(da, oa.x, oa.y, s))
            }, Ha = function (a) {
                a.container && Fa(a.container.style, a.initialPosition.x, a.initialPosition.y, a.initialZoomLevel, a)
            }, Ia = function (a, b) {
                b[E] = u + a + "px, 0px" + v
            }, Ja = function (a, b) {
                if (!i.loop && b) {
                    var c = m + (sa.x * qa - a) / sa.x, d = Math.round(a - sb.x);
                    (0 > c && d > 0 || c >= _b() - 1 && 0 > d) && (a = sb.x + d * i.mainScrollEndFriction)
                }
                sb.x = a, Ia(a, n)
            }, Ka = function (a, b) {
                var c = tb[a] - ra[a];
                return na[a] + ma[a] + c - c * (b / t)
            }, La = function (a, b) {
                a.x = b.x, a.y = b.y, b.id && (a.id = b.id)
            }, Ma = function (a) {
                a.x = Math.round(a.x), a.y = Math.round(a.y)
            }, Na = null, Oa = function () {
                Na && (e.unbind(document, "mousemove", Oa), e.addClass(a, "pswp--has_mouse"), i.mouseUsed = !0, Ca("mouseUsed")), Na = setTimeout(function () {
                    Na = null
                }, 100)
            }, Pa = function () {
                e.bind(document, "keydown", f), N.transform && e.bind(f.scrollWrap, "click", f), i.mouseUsed || e.bind(document, "mousemove", Oa), e.bind(window, "resize scroll", f), Ca("bindEvents")
            }, Qa = function () {
                e.unbind(window, "resize", f), e.unbind(window, "scroll", r.scroll), e.unbind(document, "keydown", f), e.unbind(document, "mousemove", Oa), N.transform && e.unbind(f.scrollWrap, "click", f), U && e.unbind(window, p, f), Ca("unbindEvents")
            }, Ra = function (a, b) {
                var c = hc(f.currItem, pa, a);
                return b && (ca = c), c
            }, Sa = function (a) {
                return a || (a = f.currItem), a.initialZoomLevel
            }, Ta = function (a) {
                return a || (a = f.currItem), a.w > 0 ? i.maxSpreadZoom : 1
            }, Ua = function (a, b, c, d) {
                return d === f.currItem.initialZoomLevel ? (c[a] = f.currItem.initialPosition[a], !0) : (c[a] = Ka(a, d), c[a] > b.min[a] ? (c[a] = b.min[a], !0) : c[a] < b.max[a] ? (c[a] = b.max[a], !0) : !1)
            }, Va = function () {
                if (E) {
                    var b = N.perspective && !G;
                    return u = "translate" + (b ? "3d(" : "("), void(v = N.perspective ? ", 0px)" : ")")
                }
                E = "left", e.addClass(a, "pswp--ie"), Ia = function (a, b) {
                    b.left = a + "px"
                }, Ha = function (a) {
                    var b = a.fitRatio > 1 ? 1 : a.fitRatio, c = a.container.style, d = b * a.w, e = b * a.h;
                    c.width = d + "px", c.height = e + "px", c.left = a.initialPosition.x + "px", c.top = a.initialPosition.y + "px"
                }, Ga = function () {
                    if (da) {
                        var a = da, b = f.currItem, c = b.fitRatio > 1 ? 1 : b.fitRatio, d = c * b.w, e = c * b.h;
                        a.width = d + "px", a.height = e + "px", a.left = oa.x + "px", a.top = oa.y + "px"
                    }
                }
            }, Wa = function (a) {
                var b = "";
                i.escKey && 27 === a.keyCode ? b = "close" : i.arrowKeys && (37 === a.keyCode ? b = "prev" : 39 === a.keyCode && (b = "next")), b && (a.ctrlKey || a.altKey || a.shiftKey || a.metaKey || (a.preventDefault ? a.preventDefault() : a.returnValue = !1, f[b]()))
            }, Xa = function (a) {
                a && (X || W || ea || S) && (a.preventDefault(), a.stopPropagation())
            }, Ya = function () {
                f.setScrollOffset(0, e.getScrollY())
            }, Za = {}, $a = 0, _a = function (a) {
                Za[a] && (Za[a].raf && I(Za[a].raf), $a--, delete Za[a])
            }, ab = function (a) {
                Za[a] && _a(a), Za[a] || ($a++, Za[a] = {})
            }, bb = function () {
                for (var a in Za)Za.hasOwnProperty(a) && _a(a)
            }, cb = function (a, b, c, d, e, f, g) {
                var h, i = Da();
                ab(a);
                var j = function () {
                    if (Za[a]) {
                        if (h = Da() - i, h >= d)return _a(a), f(c), void(g && g());
                        f((c - b) * e(h / d) + b), Za[a].raf = H(j)
                    }
                };
                j()
            }, db = {
                shout: Ca, listen: Ba, viewportSize: pa, options: i, isMainScrollAnimating: function () {
                    return ea
                }, getZoomLevel: function () {
                    return s
                }, getCurrentIndex: function () {
                    return m
                }, isDragging: function () {
                    return U
                }, isZooming: function () {
                    return _
                }, setScrollOffset: function (a, b) {
                    ra.x = a, M = ra.y = b, Ca("updateScrollOffset", ra)
                }, applyZoomPan: function (a, b, c, d) {
                    oa.x = b, oa.y = c, s = a, Ga(d)
                }, init: function () {
                    if (!j && !k) {
                        var c;
                        f.framework = e, f.template = a, f.bg = e.getChildByClass(a, "pswp__bg"), J = a.className, j = !0, N = e.detectFeatures(), H = N.raf, I = N.caf, E = N.transform, L = N.oldIE, f.scrollWrap = e.getChildByClass(a, "pswp__scroll-wrap"), f.container = e.getChildByClass(f.scrollWrap, "pswp__container"), n = f.container.style, f.itemHolders = y = [{
                            el: f.container.children[0],
                            wrap: 0,
                            index: -1
                        }, {el: f.container.children[1], wrap: 0, index: -1}, {
                            el: f.container.children[2],
                            wrap: 0,
                            index: -1
                        }], y[0].el.style.display = y[2].el.style.display = "none", Va(), r = {
                            resize: f.updateSize,
                            scroll: Ya,
                            keydown: Wa,
                            click: Xa
                        };
                        var d = N.isOldIOSPhone || N.isOldAndroid || N.isMobileOpera;
                        for (N.animationName && N.transform && !d || (i.showAnimationDuration = i.hideAnimationDuration = 0), c = 0; c < va.length; c++)f["init" + va[c]]();
                        if (b) {
                            var g = f.ui = new b(f, e);
                            g.init()
                        }
                        Ca("firstUpdate"), m = m || i.index || 0, (isNaN(m) || 0 > m || m >= _b()) && (m = 0), f.currItem = $b(m), (N.isOldIOSPhone || N.isOldAndroid) && (ua = !1), a.setAttribute("aria-hidden", "false"), i.modal && (ua ? a.style.position = "fixed" : (a.style.position = "absolute", a.style.top = e.getScrollY() + "px")), void 0 === M && (Ca("initialLayout"), M = K = e.getScrollY());
                        var l = "pswp--open ";
                        for (i.mainClass && (l += i.mainClass + " "), i.showHideOpacity && (l += "pswp--animate_opacity "), l += G ? "pswp--touch" : "pswp--notouch", l += N.animationName ? " pswp--css_animation" : "", l += N.svg ? " pswp--svg" : "", e.addClass(a, l), f.updateSize(), o = -1, ta = null, c = 0; h > c; c++)Ia((c + o) * sa.x, y[c].el.style);
                        L || e.bind(f.scrollWrap, q, f), Ba("initialZoomInEnd", function () {
                            f.setContent(y[0], m - 1), f.setContent(y[2], m + 1), y[0].el.style.display = y[2].el.style.display = "block", i.focus && a.focus(), Pa()
                        }), f.setContent(y[1], m), f.updateCurrItem(), Ca("afterInit"), ua || (w = setInterval(function () {
                            $a || U || _ || s !== f.currItem.initialZoomLevel || f.updateSize()
                        }, 1e3)), e.addClass(a, "pswp--visible")
                    }
                }, close: function () {
                    j && (j = !1, k = !0, Ca("close"), Qa(), bc(f.currItem, null, !0, f.destroy))
                }, destroy: function () {
                    Ca("destroy"), Wb && clearTimeout(Wb), a.setAttribute("aria-hidden", "true"), a.className = J, w && clearInterval(w), e.unbind(f.scrollWrap, q, f), e.unbind(window, "scroll", f), yb(), bb(), Aa = null
                }, panTo: function (a, b, c) {
                    c || (a > ca.min.x ? a = ca.min.x : a < ca.max.x && (a = ca.max.x), b > ca.min.y ? b = ca.min.y : b < ca.max.y && (b = ca.max.y)), oa.x = a, oa.y = b, Ga()
                }, handleEvent: function (a) {
                    a = a || window.event, r[a.type] && r[a.type](a)
                }, goTo: function (a) {
                    a = za(a);
                    var b = a - m;
                    ta = b, m = a, f.currItem = $b(m), qa -= b, Ja(sa.x * qa), bb(), ea = !1, f.updateCurrItem()
                }, next: function () {
                    f.goTo(m + 1)
                }, prev: function () {
                    f.goTo(m - 1)
                }, updateCurrZoomItem: function (a) {
                    if (a && Ca("beforeChange", 0), y[1].el.children.length) {
                        var b = y[1].el.children[0];
                        da = e.hasClass(b, "pswp__zoom-wrap") ? b.style : null
                    } else da = null;
                    ca = f.currItem.bounds, t = s = f.currItem.initialZoomLevel, oa.x = ca.center.x, oa.y = ca.center.y, a && Ca("afterChange")
                }, invalidateCurrItems: function () {
                    x = !0;
                    for (var a = 0; h > a; a++)y[a].item && (y[a].item.needsUpdate = !0)
                }, updateCurrItem: function (a) {
                    if (0 !== ta) {
                        var b, c = Math.abs(ta);
                        if (!(a && 2 > c)) {
                            f.currItem = $b(m), xa = !1, Ca("beforeChange", ta), c >= h && (o += ta + (ta > 0 ? -h : h), c = h);
                            for (var d = 0; c > d; d++)ta > 0 ? (b = y.shift(), y[h - 1] = b, o++, Ia((o + 2) * sa.x, b.el.style), f.setContent(b, m - c + d + 1 + 1)) : (b = y.pop(), y.unshift(b), o--, Ia(o * sa.x, b.el.style), f.setContent(b, m + c - d - 1 - 1));
                            if (da && 1 === Math.abs(ta)) {
                                var e = $b(z);
                                e.initialZoomLevel !== s && (hc(e, pa), lc(e), Ha(e))
                            }
                            ta = 0, f.updateCurrZoomItem(), z = m, Ca("afterChange")
                        }
                    }
                }, updateSize: function (b) {
                    if (!ua && i.modal) {
                        var c = e.getScrollY();
                        if (M !== c && (a.style.top = c + "px", M = c), !b && wa.x === window.innerWidth && wa.y === window.innerHeight)return;
                        wa.x = window.innerWidth, wa.y = window.innerHeight, a.style.height = wa.y + "px"
                    }
                    if (pa.x = f.scrollWrap.clientWidth, pa.y = f.scrollWrap.clientHeight, Ya(), sa.x = pa.x + Math.round(pa.x * i.spacing), sa.y = pa.y, Ja(sa.x * qa), Ca("beforeResize"), void 0 !== o) {
                        for (var d, g, j,
                                 k = 0; h > k; k++)d = y[k], Ia((k + o) * sa.x, d.el.style), j = m + k - 1, i.loop && _b() > 2 && (j = za(j)), g = $b(j), g && (x || g.needsUpdate || !g.bounds) ? (f.cleanSlide(g), f.setContent(d, j), 1 === k && (f.currItem = g, f.updateCurrZoomItem(!0)), g.needsUpdate = !1) : -1 === d.index && j >= 0 && f.setContent(d, j), g && g.container && (hc(g, pa), lc(g), Ha(g));
                        x = !1
                    }
                    t = s = f.currItem.initialZoomLevel, ca = f.currItem.bounds, ca && (oa.x = ca.center.x, oa.y = ca.center.y, Ga(!0)), Ca("resize")
                }, zoomTo: function (a, b, c, d, f) {
                    b && (t = s, tb.x = Math.abs(b.x) - oa.x, tb.y = Math.abs(b.y) - oa.y, La(na, oa));
                    var g = Ra(a, !1), h = {};
                    Ua("x", g, h, a), Ua("y", g, h, a);
                    var i = s, j = {x: oa.x, y: oa.y};
                    Ma(h);
                    var k = function (b) {
                        1 === b ? (s = a, oa.x = h.x, oa.y = h.y) : (s = (a - i) * b + i, oa.x = (h.x - j.x) * b + j.x, oa.y = (h.y - j.y) * b + j.y), f && f(b), Ga(1 === b)
                    };
                    c ? cb("customZoomTo", 0, 1, c, d || e.easing.sine.inOut, k) : k(1)
                }
            }, eb = 30, fb = 10, gb = {}, hb = {}, ib = {}, jb = {}, kb = {}, lb = [], mb = {}, nb = [], ob = {}, pb = 0,
            qb = la(), rb = 0, sb = la(), tb = la(), ub = la(), vb = function (a, b) {
                return a.x === b.x && a.y === b.y
            }, wb = function (a, b) {
                return Math.abs(a.x - b.x) < g && Math.abs(a.y - b.y) < g
            }, xb = function (a, b) {
                return ob.x = Math.abs(a.x - b.x), ob.y = Math.abs(a.y - b.y), Math.sqrt(ob.x * ob.x + ob.y * ob.y)
            }, yb = function () {
                Y && (I(Y), Y = null)
            }, zb = function () {
                U && (Y = H(zb), Pb())
            }, Ab = function () {
                return !("fit" === i.scaleMode && s === f.currItem.initialZoomLevel)
            }, Bb = function (a, b) {
                return a && a !== document ? a.getAttribute("class") && a.getAttribute("class").indexOf("pswp__scroll-wrap") > -1 ? !1 : b(a) ? a : Bb(a.parentNode, b) : !1
            }, Cb = {}, Db = function (a, b) {
                return Cb.prevent = !Bb(a.target, i.isClickableElement), Ca("preventDragEvent", a, b, Cb), Cb.prevent
            }, Eb = function (a, b) {
                return b.x = a.pageX, b.y = a.pageY, b.id = a.identifier, b
            }, Fb = function (a, b, c) {
                c.x = .5 * (a.x + b.x), c.y = .5 * (a.y + b.y)
            }, Gb = function (a, b, c) {
                if (a - P > 50) {
                    var d = nb.length > 2 ? nb.shift() : {};
                    d.x = b, d.y = c, nb.push(d), P = a
                }
            }, Hb = function () {
                var a = oa.y - f.currItem.initialPosition.y;
                return 1 - Math.abs(a / (pa.y / 2))
            }, Ib = {}, Jb = {}, Kb = [], Lb = function (a) {
                for (; Kb.length > 0;)Kb.pop();
                return F ? (ka = 0, lb.forEach(function (a) {
                    0 === ka ? Kb[0] = a : 1 === ka && (Kb[1] = a), ka++
                })) : a.type.indexOf("touch") > -1 ? a.touches && a.touches.length > 0 && (Kb[0] = Eb(a.touches[0], Ib), a.touches.length > 1 && (Kb[1] = Eb(a.touches[1], Jb))) : (Ib.x = a.pageX, Ib.y = a.pageY, Ib.id = "", Kb[0] = Ib), Kb
            }, Mb = function (a, b) {
                var c, d, e, g, h = 0, j = oa[a] + b[a], k = b[a] > 0, l = sb.x + b.x, m = sb.x - mb.x;
                return c = j > ca.min[a] || j < ca.max[a] ? i.panEndFriction : 1, j = oa[a] + b[a] * c, !i.allowPanToNext && s !== f.currItem.initialZoomLevel || (da ? "h" !== fa || "x" !== a || W || (k ? (j > ca.min[a] && (c = i.panEndFriction, h = ca.min[a] - j, d = ca.min[a] - na[a]), (0 >= d || 0 > m) && _b() > 1 ? (g = l, 0 > m && l > mb.x && (g = mb.x)) : ca.min.x !== ca.max.x && (e = j)) : (j < ca.max[a] && (c = i.panEndFriction, h = j - ca.max[a], d = na[a] - ca.max[a]), (0 >= d || m > 0) && _b() > 1 ? (g = l, m > 0 && l < mb.x && (g = mb.x)) : ca.min.x !== ca.max.x && (e = j))) : g = l, "x" !== a) ? void(ea || Z || s > f.currItem.fitRatio && (oa[a] += b[a] * c)) : (void 0 !== g && (Ja(g, !0), Z = g === mb.x ? !1 : !0), ca.min.x !== ca.max.x && (void 0 !== e ? oa.x = e : Z || (oa.x += b.x * c)), void 0 !== g)
            }, Nb = function (a) {
                if (!("mousedown" === a.type && a.button > 0)) {
                    if (Zb)return void a.preventDefault();
                    if (!T || "mousedown" !== a.type) {
                        if (Db(a, !0) && a.preventDefault(), Ca("pointerDown"), F) {
                            var b = e.arraySearch(lb, a.pointerId, "id");
                            0 > b && (b = lb.length), lb[b] = {x: a.pageX, y: a.pageY, id: a.pointerId}
                        }
                        var c = Lb(a), d = c.length;
                        $ = null, bb(), U && 1 !== d || (U = ga = !0, e.bind(window, p, f), R = ja = ha = S = Z = X = V = W = !1, fa = null, Ca("firstTouchStart", c), La(na, oa), ma.x = ma.y = 0, La(jb, c[0]), La(kb, jb), mb.x = sa.x * qa, nb = [{
                            x: jb.x,
                            y: jb.y
                        }], P = O = Da(), Ra(s, !0), yb(), zb()), !_ && d > 1 && !ea && !Z && (t = s, W = !1, _ = V = !0, ma.y = ma.x = 0, La(na, oa), La(gb, c[0]), La(hb, c[1]), Fb(gb, hb, ub), tb.x = Math.abs(ub.x) - oa.x, tb.y = Math.abs(ub.y) - oa.y, aa = ba = xb(gb, hb))
                    }
                }
            }, Ob = function (a) {
                if (a.preventDefault(), F) {
                    var b = e.arraySearch(lb, a.pointerId, "id");
                    if (b > -1) {
                        var c = lb[b];
                        c.x = a.pageX, c.y = a.pageY
                    }
                }
                if (U) {
                    var d = Lb(a);
                    if (fa || X || _) $ = d; else if (sb.x !== sa.x * qa) fa = "h"; else {
                        var f = Math.abs(d[0].x - jb.x) - Math.abs(d[0].y - jb.y);
                        Math.abs(f) >= fb && (fa = f > 0 ? "h" : "v", $ = d)
                    }
                }
            }, Pb = function () {
                if ($) {
                    var a = $.length;
                    if (0 !== a)if (La(gb, $[0]), ib.x = gb.x - jb.x, ib.y = gb.y - jb.y, _ && a > 1) {
                        if (jb.x = gb.x, jb.y = gb.y, !ib.x && !ib.y && vb($[1], hb))return;
                        La(hb, $[1]), W || (W = !0, Ca("zoomGestureStarted"));
                        var b = xb(gb, hb), c = Ub(b);
                        c > f.currItem.initialZoomLevel + f.currItem.initialZoomLevel / 15 && (ja = !0);
                        var d = 1, e = Sa(), g = Ta();
                        if (e > c)if (i.pinchToClose && !ja && t <= f.currItem.initialZoomLevel) {
                            var h = e - c, j = 1 - h / (e / 1.2);
                            Ea(j), Ca("onPinchClose", j), ha = !0
                        } else d = (e - c) / e, d > 1 && (d = 1), c = e - d * (e / 3); else c > g && (d = (c - g) / (6 * e), d > 1 && (d = 1), c = g + d * e);
                        0 > d && (d = 0), aa = b, Fb(gb, hb, qb), ma.x += qb.x - ub.x, ma.y += qb.y - ub.y, La(ub, qb), oa.x = Ka("x", c), oa.y = Ka("y", c), R = c > s, s = c, Ga()
                    } else {
                        if (!fa)return;
                        if (ga && (ga = !1, Math.abs(ib.x) >= fb && (ib.x -= $[0].x - kb.x), Math.abs(ib.y) >= fb && (ib.y -= $[0].y - kb.y)), jb.x = gb.x, jb.y = gb.y, 0 === ib.x && 0 === ib.y)return;
                        if ("v" === fa && i.closeOnVerticalDrag && !Ab()) {
                            ma.y += ib.y, oa.y += ib.y;
                            var k = Hb();
                            return S = !0, Ca("onVerticalDrag", k), Ea(k), void Ga()
                        }
                        Gb(Da(), gb.x, gb.y), X = !0, ca = f.currItem.bounds;
                        var l = Mb("x", ib);
                        l || (Mb("y", ib), Ma(oa), Ga())
                    }
                }
            }, Qb = function (a) {
                if (N.isOldAndroid) {
                    if (T && "mouseup" === a.type)return;
                    a.type.indexOf("touch") > -1 && (clearTimeout(T), T = setTimeout(function () {
                        T = 0
                    }, 600))
                }
                Ca("pointerUp"), Db(a, !1) && a.preventDefault();
                var b;
                if (F) {
                    var c = e.arraySearch(lb, a.pointerId, "id");
                    if (c > -1)if (b = lb.splice(c, 1)[0], navigator.pointerEnabled) b.type = a.pointerType || "mouse"; else {
                        var d = {4: "mouse", 2: "touch", 3: "pen"};
                        b.type = d[a.pointerType], b.type || (b.type = a.pointerType || "mouse")
                    }
                }
                var g, h = Lb(a), j = h.length;
                if ("mouseup" === a.type && (j = 0), 2 === j)return $ = null, !0;
                1 === j && La(kb, h[0]), 0 !== j || fa || ea || (b || ("mouseup" === a.type ? b = {
                    x: a.pageX,
                    y: a.pageY,
                    type: "mouse"
                } : a.changedTouches && a.changedTouches[0] && (b = {
                        x: a.changedTouches[0].pageX,
                        y: a.changedTouches[0].pageY,
                        type: "touch"
                    })), Ca("touchRelease", a, b));
                var k = -1;
                if (0 === j && (U = !1, e.unbind(window, p, f), yb(), _ ? k = 0 : -1 !== rb && (k = Da() - rb)), rb = 1 === j ? Da() : -1, g = -1 !== k && 150 > k ? "zoom" : "swipe", _ && 2 > j && (_ = !1, 1 === j && (g = "zoomPointerUp"), Ca("zoomGestureEnded")), $ = null, X || W || ea || S)if (bb(), Q || (Q = Rb()), Q.calculateSwipeSpeed("x"), S) {
                    var l = Hb();
                    if (l < i.verticalDragRange) f.close(); else {
                        var m = oa.y, n = ia;
                        cb("verticalDrag", 0, 1, 300, e.easing.cubic.out, function (a) {
                            oa.y = (f.currItem.initialPosition.y - m) * a + m, Ea((1 - n) * a + n), Ga()
                        }), Ca("onVerticalDrag", 1)
                    }
                } else {
                    if ((Z || ea) && 0 === j) {
                        var o = Tb(g, Q);
                        if (o)return;
                        g = "zoomPointerUp"
                    }
                    if (!ea)return "swipe" !== g ? void Vb() : void(!Z && s > f.currItem.fitRatio && Sb(Q))
                }
            }, Rb = function () {
                var a, b, c = {
                    lastFlickOffset: {},
                    lastFlickDist: {},
                    lastFlickSpeed: {},
                    slowDownRatio: {},
                    slowDownRatioReverse: {},
                    speedDecelerationRatio: {},
                    speedDecelerationRatioAbs: {},
                    distanceOffset: {},
                    backAnimDestination: {},
                    backAnimStarted: {},
                    calculateSwipeSpeed: function (d) {
                        nb.length > 1 ? (a = Da() - P + 50, b = nb[nb.length - 2][d]) : (a = Da() - O, b = kb[d]), c.lastFlickOffset[d] = jb[d] - b, c.lastFlickDist[d] = Math.abs(c.lastFlickOffset[d]), c.lastFlickDist[d] > 20 ? c.lastFlickSpeed[d] = c.lastFlickOffset[d] / a : c.lastFlickSpeed[d] = 0, Math.abs(c.lastFlickSpeed[d]) < .1 && (c.lastFlickSpeed[d] = 0), c.slowDownRatio[d] = .95, c.slowDownRatioReverse[d] = 1 - c.slowDownRatio[d], c.speedDecelerationRatio[d] = 1
                    },
                    calculateOverBoundsAnimOffset: function (a, b) {
                        c.backAnimStarted[a] || (oa[a] > ca.min[a] ? c.backAnimDestination[a] = ca.min[a] : oa[a] < ca.max[a] && (c.backAnimDestination[a] = ca.max[a]), void 0 !== c.backAnimDestination[a] && (c.slowDownRatio[a] = .7, c.slowDownRatioReverse[a] = 1 - c.slowDownRatio[a], c.speedDecelerationRatioAbs[a] < .05 && (c.lastFlickSpeed[a] = 0, c.backAnimStarted[a] = !0, cb("bounceZoomPan" + a, oa[a], c.backAnimDestination[a], b || 300, e.easing.sine.out, function (b) {
                            oa[a] = b, Ga()
                        }))))
                    },
                    calculateAnimOffset: function (a) {
                        c.backAnimStarted[a] || (c.speedDecelerationRatio[a] = c.speedDecelerationRatio[a] * (c.slowDownRatio[a] + c.slowDownRatioReverse[a] - c.slowDownRatioReverse[a] * c.timeDiff / 10), c.speedDecelerationRatioAbs[a] = Math.abs(c.lastFlickSpeed[a] * c.speedDecelerationRatio[a]), c.distanceOffset[a] = c.lastFlickSpeed[a] * c.speedDecelerationRatio[a] * c.timeDiff, oa[a] += c.distanceOffset[a])
                    },
                    panAnimLoop: function () {
                        return Za.zoomPan && (Za.zoomPan.raf = H(c.panAnimLoop), c.now = Da(), c.timeDiff = c.now - c.lastNow, c.lastNow = c.now, c.calculateAnimOffset("x"), c.calculateAnimOffset("y"), Ga(), c.calculateOverBoundsAnimOffset("x"), c.calculateOverBoundsAnimOffset("y"), c.speedDecelerationRatioAbs.x < .05 && c.speedDecelerationRatioAbs.y < .05) ? (oa.x = Math.round(oa.x), oa.y = Math.round(oa.y), Ga(), void _a("zoomPan")) : void 0
                    }
                };
                return c
            }, Sb = function (a) {
                return a.calculateSwipeSpeed("y"), ca = f.currItem.bounds, a.backAnimDestination = {}, a.backAnimStarted = {}, Math.abs(a.lastFlickSpeed.x) <= .05 && Math.abs(a.lastFlickSpeed.y) <= .05 ? (a.speedDecelerationRatioAbs.x = a.speedDecelerationRatioAbs.y = 0, a.calculateOverBoundsAnimOffset("x"), a.calculateOverBoundsAnimOffset("y"), !0) : (ab("zoomPan"), a.lastNow = Da(), void a.panAnimLoop())
            }, Tb = function (a, b) {
                var c;
                ea || (pb = m);
                var d;
                if ("swipe" === a) {
                    var g = jb.x - kb.x, h = b.lastFlickDist.x < 10;
                    g > eb && (h || b.lastFlickOffset.x > 20) ? d = -1 : -eb > g && (h || b.lastFlickOffset.x < -20) && (d = 1)
                }
                var j;
                d && (m += d, 0 > m ? (m = i.loop ? _b() - 1 : 0, j = !0) : m >= _b() && (m = i.loop ? 0 : _b() - 1, j = !0), (!j || i.loop) && (ta += d, qa -= d, c = !0));
                var k, l = sa.x * qa, n = Math.abs(l - sb.x);
                return c || l > sb.x == b.lastFlickSpeed.x > 0 ? (k = Math.abs(b.lastFlickSpeed.x) > 0 ? n / Math.abs(b.lastFlickSpeed.x) : 333, k = Math.min(k, 400), k = Math.max(k, 250)) : k = 333, pb === m && (c = !1), ea = !0, Ca("mainScrollAnimStart"), cb("mainScroll", sb.x, l, k, e.easing.cubic.out, Ja, function () {
                    bb(), ea = !1, pb = -1, (c || pb !== m) && f.updateCurrItem(), Ca("mainScrollAnimComplete")
                }), c && f.updateCurrItem(!0), c
            }, Ub = function (a) {
                return 1 / ba * a * t
            }, Vb = function () {
                var a = s, b = Sa(), c = Ta();
                b > s ? a = b : s > c && (a = c);
                var d, g = 1, h = ia;
                return ha && !R && !ja && b > s ? (f.close(), !0) : (ha && (d = function (a) {
                    Ea((g - h) * a + h)
                }), f.zoomTo(a, 0, 200, e.easing.cubic.out, d), !0)
            };
        ya("Gestures", {
            publicMethods: {
                initGestures: function () {
                    var a = function (a, b, c, d, e) {
                        A = a + b, B = a + c, C = a + d, D = e ? a + e : ""
                    };
                    F = N.pointerEvent, F && N.touch && (N.touch = !1), F ? navigator.pointerEnabled ? a("pointer", "down", "move", "up", "cancel") : a("MSPointer", "Down", "Move", "Up", "Cancel") : N.touch ? (a("touch", "start", "move", "end", "cancel"), G = !0) : a("mouse", "down", "move", "up"), p = B + " " + C + " " + D, q = A, F && !G && (G = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1), f.likelyTouchDevice = G, r[A] = Nb, r[B] = Ob, r[C] = Qb, D && (r[D] = r[C]), N.touch && (q += " mousedown", p += " mousemove mouseup", r.mousedown = r[A], r.mousemove = r[B], r.mouseup = r[C]), G || (i.allowPanToNext = !1)
                }
            }
        });
        var Wb, Xb, Yb, Zb, $b, _b, ac, bc = function (b, c, d, g) {
            Wb && clearTimeout(Wb), Zb = !0, Yb = !0;
            var h;
            b.initialLayout ? (h = b.initialLayout, b.initialLayout = null) : h = i.getThumbBoundsFn && i.getThumbBoundsFn(m);
            var j = d ? i.hideAnimationDuration : i.showAnimationDuration, k = function () {
                _a("initialZoom"), d ? (f.template.removeAttribute("style"), f.bg.removeAttribute("style")) : (Ea(1), c && (c.style.display = "block"), e.addClass(a, "pswp--animated-in"), Ca("initialZoom" + (d ? "OutEnd" : "InEnd"))), g && g(), Zb = !1
            };
            if (!j || !h || void 0 === h.x)return Ca("initialZoom" + (d ? "Out" : "In")), s = b.initialZoomLevel, La(oa, b.initialPosition), Ga(), a.style.opacity = d ? 0 : 1, Ea(1), void(j ? setTimeout(function () {
                k()
            }, j) : k());
            var n = function () {
                var c = l, g = !f.currItem.src || f.currItem.loadError || i.showHideOpacity;
                b.miniImg && (b.miniImg.style.webkitBackfaceVisibility = "hidden"), d || (s = h.w / b.w, oa.x = h.x, oa.y = h.y - K, f[g ? "template" : "bg"].style.opacity = .001, Ga()), ab("initialZoom"), d && !c && e.removeClass(a, "pswp--animated-in"), g && (d ? e[(c ? "remove" : "add") + "Class"](a, "pswp--animate_opacity") : setTimeout(function () {
                    e.addClass(a, "pswp--animate_opacity")
                }, 30)), Wb = setTimeout(function () {
                    if (Ca("initialZoom" + (d ? "Out" : "In")), d) {
                        var f = h.w / b.w, i = {x: oa.x, y: oa.y}, l = s, m = ia, n = function (b) {
                            1 === b ? (s = f, oa.x = h.x, oa.y = h.y - M) : (s = (f - l) * b + l, oa.x = (h.x - i.x) * b + i.x, oa.y = (h.y - M - i.y) * b + i.y), Ga(), g ? a.style.opacity = 1 - b : Ea(m - b * m)
                        };
                        c ? cb("initialZoom", 0, 1, j, e.easing.cubic.out, n, k) : (n(1), Wb = setTimeout(k, j + 20))
                    } else s = b.initialZoomLevel, La(oa, b.initialPosition), Ga(), Ea(1), g ? a.style.opacity = 1 : Ea(1), Wb = setTimeout(k, j + 20)
                }, d ? 25 : 90)
            };
            n()
        }, cc = {}, dc = [], ec = {
            index: 0,
            errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
            forceProgressiveLoading: !1,
            preload: [1, 1],
            getNumItemsFn: function () {
                return Xb.length
            }
        }, fc = function () {
            return {center: {x: 0, y: 0}, max: {x: 0, y: 0}, min: {x: 0, y: 0}}
        }, gc = function (a, b, c) {
            var d = a.bounds;
            d.center.x = Math.round((cc.x - b) / 2), d.center.y = Math.round((cc.y - c) / 2) + a.vGap.top, d.max.x = b > cc.x ? Math.round(cc.x - b) : d.center.x, d.max.y = c > cc.y ? Math.round(cc.y - c) + a.vGap.top : d.center.y, d.min.x = b > cc.x ? 0 : d.center.x, d.min.y = c > cc.y ? a.vGap.top : d.center.y
        }, hc = function (a, b, c) {
            if (a.src && !a.loadError) {
                var d = !c;
                if (d && (a.vGap || (a.vGap = {
                        top: 0,
                        bottom: 0
                    }), Ca("parseVerticalMargin", a)), cc.x = b.x, cc.y = b.y - a.vGap.top - a.vGap.bottom, d) {
                    var e = cc.x / a.w, f = cc.y / a.h;
                    a.fitRatio = f > e ? e : f;
                    var g = i.scaleMode;
                    "orig" === g ? c = 1 : "fit" === g && (c = a.fitRatio), c > 1 && (c = 1), a.initialZoomLevel = c, a.bounds || (a.bounds = fc())
                }
                if (!c)return;
                return gc(a, a.w * c, a.h * c), d && c === a.initialZoomLevel && (a.initialPosition = a.bounds.center), a.bounds
            }
            return a.w = a.h = 0, a.initialZoomLevel = a.fitRatio = 1, a.bounds = fc(), a.initialPosition = a.bounds.center, a.bounds
        }, ic = function (a, b, c, d, e, g) {
            b.loadError || d && (b.imageAppended = !0, lc(b, d, b === f.currItem && xa), c.appendChild(d), g && setTimeout(function () {
                b && b.loaded && b.placeholder && (b.placeholder.style.display = "none", b.placeholder = null)
            }, 500))
        }, jc = function (a) {
            a.loading = !0, a.loaded = !1;
            var b = a.img = e.createEl("pswp__img", "img"), c = function () {
                a.loading = !1, a.loaded = !0, a.loadComplete ? a.loadComplete(a) : a.img = null, b.onload = b.onerror = null, b = null
            };
            return b.onload = c, b.onerror = function () {
                a.loadError = !0, c()
            }, b.src = a.src, b
        }, kc = function (a, b) {
            return a.src && a.loadError && a.container ? (b && (a.container.innerHTML = ""), a.container.innerHTML = i.errorMsg.replace("%url%", a.src), !0) : void 0
        }, lc = function (a, b, c) {
            if (a.src) {
                b || (b = a.container.lastChild);
                var d = c ? a.w : Math.round(a.w * a.fitRatio), e = c ? a.h : Math.round(a.h * a.fitRatio);
                a.placeholder && !a.loaded && (a.placeholder.style.width = d + "px", a.placeholder.style.height = e + "px"), b.style.width = d + "px", b.style.height = e + "px"
            }
        }, mc = function () {
            if (dc.length) {
                for (var a,
                         b = 0; b < dc.length; b++)a = dc[b], a.holder.index === a.index && ic(a.index, a.item, a.baseDiv, a.img, !1, a.clearPlaceholder);
                dc = []
            }
        };
        ya("Controller", {
            publicMethods: {
                lazyLoadItem: function (a) {
                    a = za(a);
                    var b = $b(a);
                    b && (!b.loaded && !b.loading || x) && (Ca("gettingData", a, b), b.src && jc(b))
                }, initController: function () {
                    e.extend(i, ec, !0), f.items = Xb = c, $b = f.getItemAt, _b = i.getNumItemsFn, ac = i.loop, _b() < 3 && (i.loop = !1), Ba("beforeChange", function (a) {
                        var b, c = i.preload, d = null === a ? !0 : a >= 0, e = Math.min(c[0], _b()),
                            g = Math.min(c[1], _b());
                        for (b = 1; (d ? g : e) >= b; b++)f.lazyLoadItem(m + b);
                        for (b = 1; (d ? e : g) >= b; b++)f.lazyLoadItem(m - b)
                    }), Ba("initialLayout", function () {
                        f.currItem.initialLayout = i.getThumbBoundsFn && i.getThumbBoundsFn(m)
                    }), Ba("mainScrollAnimComplete", mc), Ba("initialZoomInEnd", mc), Ba("destroy", function () {
                        for (var a,
                                 b = 0; b < Xb.length; b++)a = Xb[b], a.container && (a.container = null), a.placeholder && (a.placeholder = null), a.img && (a.img = null), a.preloader && (a.preloader = null), a.loadError && (a.loaded = a.loadError = !1);
                        dc = null
                    })
                }, getItemAt: function (a) {
                    return a >= 0 && void 0 !== Xb[a] ? Xb[a] : !1
                }, allowProgressiveImg: function () {
                    return i.forceProgressiveLoading || !G || i.mouseUsed || screen.width > 1200
                }, setContent: function (a, b) {
                    i.loop && (b = za(b));
                    var c = f.getItemAt(a.index);
                    c && (c.container = null);
                    var d, g = f.getItemAt(b);
                    if (!g)return void(a.el.innerHTML = "");
                    Ca("gettingData", b, g), a.index = b, a.item = g;
                    var h = g.container = e.createEl("pswp__zoom-wrap");
                    if (!g.src && g.html && (g.html.tagName ? h.appendChild(g.html) : h.innerHTML = g.html), kc(g), hc(g, pa), !g.src || g.loadError || g.loaded) g.src && !g.loadError && (d = e.createEl("pswp__img", "img"), d.style.opacity = 1, d.src = g.src, lc(g, d), ic(b, g, h, d, !0)); else {
                        if (g.loadComplete = function (c) {
                                if (j) {
                                    if (a && a.index === b) {
                                        if (kc(c, !0))return c.loadComplete = c.img = null, hc(c, pa), Ha(c), void(a.index === m && f.updateCurrZoomItem());
                                        c.imageAppended ? !Zb && c.placeholder && (c.placeholder.style.display = "none", c.placeholder = null) : N.transform && (ea || Zb) ? dc.push({
                                            item: c,
                                            baseDiv: h,
                                            img: c.img,
                                            index: b,
                                            holder: a,
                                            clearPlaceholder: !0
                                        }) : ic(b, c, h, c.img, ea || Zb, !0)
                                    }
                                    c.loadComplete = null, c.img = null, Ca("imageLoadComplete", b, c)
                                }
                            }, e.features.transform) {
                            var k = "pswp__img pswp__img--placeholder";
                            k += g.msrc ? "" : " pswp__img--placeholder--blank";
                            var l = e.createEl(k, g.msrc ? "img" : "");
                            g.msrc && (l.src = g.msrc), lc(g, l), h.appendChild(l), g.placeholder = l
                        }
                        g.loading || jc(g), f.allowProgressiveImg() && (!Yb && N.transform ? dc.push({
                            item: g,
                            baseDiv: h,
                            img: g.img,
                            index: b,
                            holder: a
                        }) : ic(b, g, h, g.img, !0, !0))
                    }
                    Yb || b !== m ? Ha(g) : (da = h.style, bc(g, d || g.img)), a.el.innerHTML = "", a.el.appendChild(h)
                }, cleanSlide: function (a) {
                    a.img && (a.img.onload = a.img.onerror = null), a.loaded = a.loading = a.img = a.imageAppended = !1
                }
            }
        });
        var nc, oc = {}, pc = function (a, b, c) {
            var d = document.createEvent("CustomEvent"),
                e = {origEvent: a, target: a.target, releasePoint: b, pointerType: c || "touch"};
            d.initCustomEvent("pswpTap", !0, !0, e), a.target.dispatchEvent(d)
        };
        ya("Tap", {
            publicMethods: {
                initTap: function () {
                    Ba("firstTouchStart", f.onTapStart), Ba("touchRelease", f.onTapRelease), Ba("destroy", function () {
                        oc = {}, nc = null
                    })
                }, onTapStart: function (a) {
                    a.length > 1 && (clearTimeout(nc), nc = null)
                }, onTapRelease: function (a, b) {
                    if (b && !X && !V && !$a) {
                        var c = b;
                        if (nc && (clearTimeout(nc), nc = null, wb(c, oc)))return void Ca("doubleTap", c);
                        if ("mouse" === b.type)return void pc(a, b, "mouse");
                        var d = a.target.tagName.toUpperCase();
                        if ("BUTTON" === d || e.hasClass(a.target, "pswp__single-tap"))return void pc(a, b);
                        La(oc, c), nc = setTimeout(function () {
                            pc(a, b), nc = null
                        }, 300)
                    }
                }
            }
        });
        var qc;
        ya("DesktopZoom", {
            publicMethods: {
                initDesktopZoom: function () {
                    L || (G ? Ba("mouseUsed", function () {
                        f.setupDesktopZoom()
                    }) : f.setupDesktopZoom(!0))
                }, setupDesktopZoom: function (b) {
                    qc = {};
                    var c = "wheel mousewheel DOMMouseScroll";
                    Ba("bindEvents", function () {
                        e.bind(a, c, f.handleMouseWheel)
                    }), Ba("unbindEvents", function () {
                        qc && e.unbind(a, c, f.handleMouseWheel)
                    }), f.mouseZoomedIn = !1;
                    var d, g = function () {
                        f.mouseZoomedIn && (e.removeClass(a, "pswp--zoomed-in"), f.mouseZoomedIn = !1), 1 > s ? e.addClass(a, "pswp--zoom-allowed") : e.removeClass(a, "pswp--zoom-allowed"), h()
                    }, h = function () {
                        d && (e.removeClass(a, "pswp--dragging"), d = !1)
                    };
                    Ba("resize", g), Ba("afterChange", g), Ba("pointerDown", function () {
                        f.mouseZoomedIn && (d = !0, e.addClass(a, "pswp--dragging"))
                    }), Ba("pointerUp", h), b || g()
                }, handleMouseWheel: function (a) {
                    if (s <= f.currItem.fitRatio)return i.modal && (!i.closeOnScroll || $a || U ? a.preventDefault() : E && Math.abs(a.deltaY) > 2 && (l = !0, f.close())), !0;
                    if (a.stopPropagation(), qc.x = 0, "deltaX" in a) 1 === a.deltaMode ? (qc.x = 18 * a.deltaX, qc.y = 18 * a.deltaY) : (qc.x = a.deltaX, qc.y = a.deltaY); else if ("wheelDelta" in a) a.wheelDeltaX && (qc.x = -.16 * a.wheelDeltaX), a.wheelDeltaY ? qc.y = -.16 * a.wheelDeltaY : qc.y = -.16 * a.wheelDelta; else {
                        if (!("detail" in a))return;
                        qc.y = a.detail
                    }
                    Ra(s, !0);
                    var b = oa.x - qc.x, c = oa.y - qc.y;
                    (i.modal || b <= ca.min.x && b >= ca.max.x && c <= ca.min.y && c >= ca.max.y) && a.preventDefault(), f.panTo(b, c)
                }, toggleDesktopZoom: function (b) {
                    b = b || {x: pa.x / 2 + ra.x, y: pa.y / 2 + ra.y};
                    var c = i.getDoubleTapZoom(!0, f.currItem), d = s === c;
                    f.mouseZoomedIn = !d, f.zoomTo(d ? f.currItem.initialZoomLevel : c, b, 333), e[(d ? "remove" : "add") + "Class"](a, "pswp--zoomed-in")
                }
            }
        });
        var rc, sc, tc, uc, vc, wc, xc, yc, zc, Ac, Bc, Cc, Dc = {history: !0, galleryUID: 1}, Ec = function () {
            return Bc.hash.substring(1)
        }, Fc = function () {
            rc && clearTimeout(rc), tc && clearTimeout(tc)
        }, Gc = function () {
            var a = Ec(), b = {};
            if (a.length < 5)return b;
            var c, d = a.split("&");
            for (c = 0; c < d.length; c++)if (d[c]) {
                var e = d[c].split("=");
                e.length < 2 || (b[e[0]] = e[1])
            }
            if (i.galleryPIDs) {
                var f = b.pid;
                for (b.pid = 0, c = 0; c < Xb.length; c++)if (Xb[c].pid === f) {
                    b.pid = c;
                    break
                }
            } else b.pid = parseInt(b.pid, 10) - 1;
            return b.pid < 0 && (b.pid = 0), b
        }, Hc = function () {
            if (tc && clearTimeout(tc), $a || U)return void(tc = setTimeout(Hc, 500));
            uc ? clearTimeout(sc) : uc = !0;
            var a = m + 1, b = $b(m);
            b.hasOwnProperty("pid") && (a = b.pid);
            var c = xc + "&gid=" + i.galleryUID + "&pid=" + a;
            yc || -1 === Bc.hash.indexOf(c) && (Ac = !0);
            var d = Bc.href.split("#")[0] + "#" + c;
            Cc ? "#" + c !== window.location.hash && history[yc ? "replaceState" : "pushState"]("", document.title, d) : yc ? Bc.replace(d) : Bc.hash = c, yc = !0, sc = setTimeout(function () {
                uc = !1
            }, 60)
        };
        ya("History", {
            publicMethods: {
                initHistory: function () {
                    if (e.extend(i, Dc, !0), i.history) {
                        Bc = window.location, Ac = !1, zc = !1, yc = !1, xc = Ec(), Cc = "pushState" in history, xc.indexOf("gid=") > -1 && (xc = xc.split("&gid=")[0], xc = xc.split("?gid=")[0]), Ba("afterChange", f.updateURL), Ba("unbindEvents", function () {
                            e.unbind(window, "hashchange", f.onHashChange)
                        });
                        var a = function () {
                            wc = !0, zc || (Ac ? history.back() : xc ? Bc.hash = xc : Cc ? history.pushState("", document.title, Bc.pathname + Bc.search) : Bc.hash = ""), Fc()
                        };
                        Ba("unbindEvents", function () {
                            l && a()
                        }), Ba("destroy", function () {
                            wc || a()
                        }), Ba("firstUpdate", function () {
                            m = Gc().pid
                        });
                        var b = xc.indexOf("pid=");
                        b > -1 && (xc = xc.substring(0, b), "&" === xc.slice(-1) && (xc = xc.slice(0, -1))), setTimeout(function () {
                            j && e.bind(window, "hashchange", f.onHashChange)
                        }, 40)
                    }
                }, onHashChange: function () {
                    return Ec() === xc ? (zc = !0, void f.close()) : void(uc || (vc = !0, f.goTo(Gc().pid), vc = !1))
                }, updateURL: function () {
                    Fc(), vc || (yc ? rc = setTimeout(Hc, 800) : Hc())
                }
            }
        }), e.extend(f, db)
    };
    return a
});

/**
 * @module       PhotoSwipe  Default UI
 * @author       Dmitry Semenov
 * @see          http://photoswipe.com
 * @version      4.1.1
 */
!function (a, b) {
    "function" == typeof define && define.amd ? define(b) : "object" == typeof exports ? module.exports = b() : a.PhotoSwipeUI_Default = b()
}(this, function () {
    "use strict";
    var a = function (a, b) {
        var c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v = this, w = !1, x = !0, y = !0, z = {
            barsSize: {top: 44, bottom: "auto"},
            closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"],
            timeToIdle: 4e3,
            timeToIdleOutside: 1e3,
            loadingIndicatorDelay: 1e3,
            addCaptionHTMLFn: function (a, b) {
                return a.title ? (b.children[0].innerHTML = a.title, !0) : (b.children[0].innerHTML = "", !1)
            },
            closeEl: !0,
            captionEl: !1,
            fullscreenEl: !0,
            zoomEl: !0,
            shareEl: !0,
            counterEl: !0,
            arrowEl: !0,
            preloaderEl: !0,
            tapToClose: !1,
            tapToToggleControls: !0,
            clickToCloseNonZoomable: !0,
            shareButtons: [{
                id: "facebook",
                label: "Share on Facebook",
                url: "https://www.facebook.com/sharer/sharer.php?u={{url}}"
            }, {
                id: "twitter",
                label: "Tweet",
                url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}"
            }, {
                id: "pinterest",
                label: "Pin it",
                url: "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}"
            }, {id: "download", label: "Download image", url: "{{raw_image_url}}", download: !0}],
            getImageURLForShare: function () {
                return a.currItem.src || ""
            },
            getPageURLForShare: function () {
                return window.location.href
            },
            getTextForShare: function () {
                return a.currItem.title || ""
            },
            indexIndicatorSep: " / ",
            fitControlsWidth: 1200
        }, A = function (a) {
            if (r)return !0;
            a = a || window.event, q.timeToIdle && q.mouseUsed && !k && K();
            for (var c, d, e = a.target || a.srcElement, f = e.getAttribute("class") || "",
                     g = 0; g < S.length; g++)c = S[g], c.onTap && f.indexOf("pswp__" + c.name) > -1 && (c.onTap(), d = !0);
            if (d) {
                a.stopPropagation && a.stopPropagation(), r = !0;
                var h = b.features.isOldAndroid ? 600 : 30;
                s = setTimeout(function () {
                    r = !1
                }, h)
            }
        }, B = function () {
            return !a.likelyTouchDevice || q.mouseUsed || screen.width > q.fitControlsWidth
        }, C = function (a, c, d) {
            b[(d ? "add" : "remove") + "Class"](a, "pswp__" + c)
        }, D = function () {
            var a = 1 === q.getNumItemsFn();
            a !== p && (C(d, "ui--one-slide", a), p = a)
        }, E = function () {
            C(i, "share-modal--hidden", y)
        }, F = function () {
            return y = !y, y ? (b.removeClass(i, "pswp__share-modal--fade-in"), setTimeout(function () {
                y && E()
            }, 300)) : (E(), setTimeout(function () {
                y || b.addClass(i, "pswp__share-modal--fade-in")
            }, 30)), y || H(), !1
        }, G = function (b) {
            b = b || window.event;
            var c = b.target || b.srcElement;
            return a.shout("shareLinkClick", b, c), c.href ? c.hasAttribute("download") ? !0 : (window.open(c.href, "pswp_share", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 100)), y || F(), !1) : !1
        }, H = function () {
            for (var a, b, c, d, e, f = "",
                     g = 0; g < q.shareButtons.length; g++)a = q.shareButtons[g], c = q.getImageURLForShare(a), d = q.getPageURLForShare(a), e = q.getTextForShare(a), b = a.url.replace("{{url}}", encodeURIComponent(d)).replace("{{image_url}}", encodeURIComponent(c)).replace("{{raw_image_url}}", c).replace("{{text}}", encodeURIComponent(e)), f += '<a href="' + b + '" target="_blank" class="pswp__share--' + a.id + '"' + (a.download ? "download" : "") + ">" + a.label + "</a>", q.parseShareButtonOut && (f = q.parseShareButtonOut(a, f));
            i.children[0].innerHTML = f, i.children[0].onclick = G
        }, I = function (a) {
            for (var c = 0; c < q.closeElClasses.length; c++)if (b.hasClass(a, "pswp__" + q.closeElClasses[c]))return !0
        }, J = 0, K = function () {
            clearTimeout(u), J = 0, k && v.setIdle(!1)
        }, L = function (a) {
            a = a ? a : window.event;
            var b = a.relatedTarget || a.toElement;
            b && "HTML" !== b.nodeName || (clearTimeout(u), u = setTimeout(function () {
                v.setIdle(!0)
            }, q.timeToIdleOutside))
        }, M = function () {
            q.fullscreenEl && !b.features.isOldAndroid && (c || (c = v.getFullscreenAPI()), c ? (b.bind(document, c.eventK, v.updateFullscreen), v.updateFullscreen(), b.addClass(a.template, "pswp--supports-fs")) : b.removeClass(a.template, "pswp--supports-fs"))
        }, N = function () {
            q.preloaderEl && (O(!0), l("beforeChange", function () {
                clearTimeout(o), o = setTimeout(function () {
                    a.currItem && a.currItem.loading ? (!a.allowProgressiveImg() || a.currItem.img && !a.currItem.img.naturalWidth) && O(!1) : O(!0)
                }, q.loadingIndicatorDelay)
            }), l("imageLoadComplete", function (b, c) {
                a.currItem === c && O(!0)
            }))
        }, O = function (a) {
            n !== a && (C(m, "preloader--active", !a), n = a)
        }, P = function (a) {
            var c = a.vGap;
            if (B()) {
                var g = q.barsSize;
                if (q.captionEl && "auto" === g.bottom)if (f || (f = b.createEl("pswp__caption pswp__caption--fake"), f.appendChild(b.createEl("pswp__caption__center")), d.insertBefore(f, e), b.addClass(d, "pswp__ui--fit")), q.addCaptionHTMLFn(a, f, !0)) {
                    var h = f.clientHeight;
                    c.bottom = parseInt(h, 10) || 44
                } else c.bottom = g.top; else c.bottom = "auto" === g.bottom ? 0 : g.bottom;
                c.top = g.top
            } else c.top = c.bottom = 0
        }, Q = function () {
            q.timeToIdle && l("mouseUsed", function () {
                b.bind(document, "mousemove", K), b.bind(document, "mouseout", L), t = setInterval(function () {
                    J++, 2 === J && v.setIdle(!0)
                }, q.timeToIdle / 2)
            })
        }, R = function () {
            l("onVerticalDrag", function (a) {
                x && .95 > a ? v.hideControls() : !x && a >= .95 && v.showControls()
            });
            var a;
            l("onPinchClose", function (b) {
                x && .9 > b ? (v.hideControls(), a = !0) : a && !x && b > .9 && v.showControls()
            }), l("zoomGestureEnded", function () {
                a = !1, a && !x && v.showControls()
            })
        }, S = [{
            name: "caption", option: "captionEl", onInit: function (a) {
                e = a
            }
        }, {
            name: "share-modal", option: "shareEl", onInit: function (a) {
                i = a
            }, onTap: function () {
                F()
            }
        }, {
            name: "button--share", option: "shareEl", onInit: function (a) {
                h = a
            }, onTap: function () {
                F()
            }
        }, {name: "button--zoom", option: "zoomEl", onTap: a.toggleDesktopZoom}, {
            name: "counter",
            option: "counterEl",
            onInit: function (a) {
                g = a
            }
        }, {name: "button--close", option: "closeEl", onTap: a.close}, {
            name: "button--arrow--left",
            option: "arrowEl",
            onTap: a.prev
        }, {name: "button--arrow--right", option: "arrowEl", onTap: a.next}, {
            name: "button--fs",
            option: "fullscreenEl",
            onTap: function () {
                c.isFullscreen() ? c.exit() : c.enter()
            }
        }, {
            name: "preloader", option: "preloaderEl", onInit: function (a) {
                m = a
            }
        }], T = function () {
            var a, c, e, f = function (d) {
                if (d)for (var f = d.length, g = 0; f > g; g++) {
                    a = d[g], c = a.className;
                    for (var h = 0; h < S.length; h++)e = S[h], c.indexOf("pswp__" + e.name) > -1 && (q[e.option] ? (b.removeClass(a, "pswp__element--disabled"), e.onInit && e.onInit(a)) : b.addClass(a, "pswp__element--disabled"))
                }
            };
            f(d.children);
            var g = b.getChildByClass(d, "pswp__top-bar");
            g && f(g.children)
        };
        v.init = function () {
            b.extend(a.options, z, !0), q = a.options, d = b.getChildByClass(a.scrollWrap, "pswp__ui"), l = a.listen, R(), l("beforeChange", v.update), l("doubleTap", function (b) {
                var c = a.currItem.initialZoomLevel;
                a.getZoomLevel() !== c ? a.zoomTo(c, b, 333) : a.zoomTo(q.getDoubleTapZoom(!1, a.currItem), b, 333)
            }), l("preventDragEvent", function (a, b, c) {
                var d = a.target || a.srcElement;
                d && d.getAttribute("class") && a.type.indexOf("mouse") > -1 && (d.getAttribute("class").indexOf("__caption") > 0 || /(SMALL|STRONG|EM)/i.test(d.tagName)) && (c.prevent = !1)
            }), l("bindEvents", function () {
                b.bind(d, "pswpTap click", A), b.bind(a.scrollWrap, "pswpTap", v.onGlobalTap), a.likelyTouchDevice || b.bind(a.scrollWrap, "mouseover", v.onMouseOver)
            }), l("unbindEvents", function () {
                y || F(), t && clearInterval(t), b.unbind(document, "mouseout", L), b.unbind(document, "mousemove", K), b.unbind(d, "pswpTap click", A), b.unbind(a.scrollWrap, "pswpTap", v.onGlobalTap), b.unbind(a.scrollWrap, "mouseover", v.onMouseOver), c && (b.unbind(document, c.eventK, v.updateFullscreen), c.isFullscreen() && (q.hideAnimationDuration = 0, c.exit()), c = null)
            }), l("destroy", function () {
                q.captionEl && (f && d.removeChild(f), b.removeClass(e, "pswp__caption--empty")), i && (i.children[0].onclick = null), b.removeClass(d, "pswp__ui--over-close"), b.addClass(d, "pswp__ui--hidden"), v.setIdle(!1)
            }), q.showAnimationDuration || b.removeClass(d, "pswp__ui--hidden"), l("initialZoomIn", function () {
                q.showAnimationDuration && b.removeClass(d, "pswp__ui--hidden")
            }), l("initialZoomOut", function () {
                b.addClass(d, "pswp__ui--hidden")
            }), l("parseVerticalMargin", P), T(), q.shareEl && h && i && (y = !0), D(), Q(), M(), N()
        }, v.setIdle = function (a) {
            k = a, C(d, "ui--idle", a)
        }, v.update = function () {
            x && a.currItem ? (v.updateIndexIndicator(), q.captionEl && (q.addCaptionHTMLFn(a.currItem, e), C(e, "caption--empty", !a.currItem.title)), w = !0) : w = !1, y || F(), D()
        }, v.updateFullscreen = function (d) {
            d && setTimeout(function () {
                a.setScrollOffset(0, b.getScrollY())
            }, 50), b[(c.isFullscreen() ? "add" : "remove") + "Class"](a.template, "pswp--fs")
        }, v.updateIndexIndicator = function () {
            q.counterEl && (g.innerHTML = a.getCurrentIndex() + 1 + q.indexIndicatorSep + q.getNumItemsFn())
        }, v.onGlobalTap = function (c) {
            c = c || window.event;
            var d = c.target || c.srcElement;
            if (!r)if (c.detail && "mouse" === c.detail.pointerType) {
                if (I(d))return void a.close();
                b.hasClass(d, "pswp__img") && (1 === a.getZoomLevel() && a.getZoomLevel() <= a.currItem.fitRatio ? q.clickToCloseNonZoomable && a.close() : a.toggleDesktopZoom(c.detail.releasePoint))
            } else if (q.tapToToggleControls && (x ? v.hideControls() : v.showControls()), q.tapToClose && (b.hasClass(d, "pswp__img") || I(d)))return void a.close()
        }, v.onMouseOver = function (a) {
            a = a || window.event;
            var b = a.target || a.srcElement;
            C(d, "ui--over-close", I(b))
        }, v.hideControls = function () {
            b.addClass(d, "pswp__ui--hidden"), x = !1
        }, v.showControls = function () {
            x = !0, w || v.update(), b.removeClass(d, "pswp__ui--hidden")
        }, v.supportsFullscreen = function () {
            var a = document;
            return !!(a.exitFullscreen || a.mozCancelFullScreen || a.webkitExitFullscreen || a.msExitFullscreen)
        }, v.getFullscreenAPI = function () {
            var b, c = document.documentElement, d = "fullscreenchange";
            return c.requestFullscreen ? b = {
                enterK: "requestFullscreen",
                exitK: "exitFullscreen",
                elementK: "fullscreenElement",
                eventK: d
            } : c.mozRequestFullScreen ? b = {
                enterK: "mozRequestFullScreen",
                exitK: "mozCancelFullScreen",
                elementK: "mozFullScreenElement",
                eventK: "moz" + d
            } : c.webkitRequestFullscreen ? b = {
                enterK: "webkitRequestFullscreen",
                exitK: "webkitExitFullscreen",
                elementK: "webkitFullscreenElement",
                eventK: "webkit" + d
            } : c.msRequestFullscreen && (b = {
                    enterK: "msRequestFullscreen",
                    exitK: "msExitFullscreen",
                    elementK: "msFullscreenElement",
                    eventK: "MSFullscreenChange"
                }), b && (b.enter = function () {
                return j = q.closeOnScroll, q.closeOnScroll = !1, "webkitRequestFullscreen" !== this.enterK ? a.template[this.enterK]() : void a.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
            }, b.exit = function () {
                return q.closeOnScroll = j, document[this.exitK]()
            }, b.isFullscreen = function () {
                return document[this.elementK]
            }), b
        }
    };
    return a
});