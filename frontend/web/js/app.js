"use strict";
var userAgent = navigator.userAgent.toLowerCase(),
    initialDate = new Date(),
    $document = $(document),
    $window = $(window),
    $html = $("html"),
    isDesktop = $html.hasClass("desktop"),
    isIE = userAgent.indexOf("msie") != -1 ? parseInt(userAgent.split("msie")[1]) : userAgent.indexOf("trident") != -1 ? 11 : userAgent.indexOf("edge") != -1 ? 12 : false,
    isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
    isTouch = "ontouchstart" in window,
    plugins = {
        rdNavbar: $(".rd-navbar"),
        rdParallax: $(".rd-parallax"),
        isotope: $(".isotope"),
        photoSwipeGallery: $("[data-photo-swipe-item]"),
        owl: $(".owl-carousel"),
        customWaypoints: $('[data-custom-scroll-to]')
    };
$document.ready(function () {
    function isScrolledIntoView(elem) {
        var $window = $(window);
        return elem.offset().top + elem.outerHeight() >= $window.scrollTop() && elem.offset().top <= $window.scrollTop() + $window.height();
    }

    function resizeOnImageLoad(image) {
        image.onload = function () {
            $window.trigger("resize");
        };
    }

    if (plugins.customWaypoints.length) {
        var i;
        $document.delegate("[data-custom-scroll-to]", "click", function (e) {
            e.preventDefault();
            $("body, html").stop().animate({scrollTop: $("#" + $(this).attr('data-custom-scroll-to')).offset().top}, 1000, function () {
                $(window).trigger("resize");
            });
        });
    }

    if (plugins.rdNavbar.length) {
        plugins.rdNavbar.RDNavbar({
            stickUpClone: (plugins.rdNavbar.attr("data-stick-up-clone")) ? plugins.rdNavbar.attr("data-stick-up-clone") === 'true' : false,
            stickUpOffset: (plugins.rdNavbar.attr("data-stick-up-offset")) ? plugins.rdNavbar.attr("data-stick-up-offset") : 1
        });
    }

    if (plugins.rdParallax.length) {
        var i;
        $.RDParallax();
        if (!isIE && !isMobile) {
            $(window).on("scroll", function () {
                for (i = 0; i < plugins.rdParallax.length; i++) {
                    var parallax = $(plugins.rdParallax[i]);
                    if (isScrolledIntoView(parallax)) {
                        parallax.find(".rd-parallax-inner").css("position", "fixed");
                    } else {
                        parallax.find(".rd-parallax-inner").css("position", "absolute");
                    }
                }
            });
        }
        $("a[href='#']").on("click", function (e) {
            setTimeout(function () {
                $(window).trigger("resize");
            }, 300);
        });
    }

    if (plugins.isotope.length) {
        var i, isogroup = [];
        for (i = 0; i < plugins.isotope.length; i++) {
            var isotopeItem = plugins.isotope[i], iso = new Isotope(isotopeItem, {
                itemSelector: '.isotope-item',
                layoutMode: isotopeItem.getAttribute('data-isotope-layout') ? isotopeItem.getAttribute('data-isotope-layout') : 'masonry',
                filter: '*'
            });
            isogroup.push(iso);
        }

        $(window).on('load', function () {
            var items = $('.isotope-item');

            var i;
            setTimeout(function () {
                var i;
                for (i = 0; i < isogroup.length; i++) {
                    isogroup[i].element.className += " isotope--loaded";
                    isogroup[i].layout();
                }
                $window.trigger("resize");
            }, 600);
        });

        var resizeTimout;
        $("[data-isotope-filter]").on("click", function (e) {
            e.preventDefault();
            var filter = $(this);
            var group = this.getAttribute("data-isotope-group");
            clearTimeout(resizeTimout);
            filter.parents(".isotope-filters").find('.active').removeClass("active");
            filter.addClass("active");
            var iso = $('.isotope[data-isotope-group="' + group + '"]');

            var activeItem = $('[data-filter*="' + this.getAttribute("data-isotope-filter") + '"]');

            var items = $('.isotope-item');
            var i;
            for (i = 0; i < items.length; i++) {
                $(items[i]).removeClass('col-12').addClass('col-lg-4');
            }

            if (activeItem.length) {
                activeItem.removeClass('col-lg-4');
                activeItem.addClass('col-12');
            }

            iso.isotope({
                itemSelector: '.isotope-item',
                layoutMode: iso.attr('data-isotope-layout') ? iso.attr('data-isotope-layout') : 'masonry',
                filter: this.getAttribute("data-isotope-filter") === '*' ? '*' : '[data-filter*="' + this.getAttribute("data-isotope-filter") + '"]'
            });

            resizeTimout = setTimeout(function () {
                $window.trigger('resize');
            }, 300);

        }).eq(0).trigger("click");
    }

    if (plugins.photoSwipeGallery.length) {
        $document.delegate("[data-photo-swipe-item]", "click", function (event) {
            event.preventDefault();
            var $el = $(this),
                $galleryItems = $el.parents("[data-photo-swipe-gallery]").find("a[data-photo-swipe-item]"),
                pswpElement = document.querySelectorAll('.pswp')[0], encounteredItems = {}, pswpItems = [], options,
                pswpIndex = 0, pswp;
            if ($galleryItems.length == 0) {
                $galleryItems = $el;
            }
            $galleryItems.each(function () {
                var $item = $(this), src = $item.attr('href'), size = $item.attr('data-size').split('x'), pswdItem;
                if ($item.is(':visible')) {
                    if (!encounteredItems[src]) {
                        pswdItem = {src: src, w: parseInt(size[0], 10), h: parseInt(size[1], 10), el: $item};
                        encounteredItems[src] = {item: pswdItem, index: pswpIndex};
                        pswpItems.push(pswdItem);
                        pswpIndex++;
                    }
                }
            });

            options = {
                index: encounteredItems[$el.attr('href')].index, getThumbBoundsFn: function (index) {
                    var $el = pswpItems[index].el, offset = $el.offset();
                    return {x: offset.left, y: offset.top, w: $el.width()};
                }
            };
            pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, pswpItems, options);
            pswp.init();
        });
    }
});