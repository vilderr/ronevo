<?php

namespace frontend\controllers;

use frontend\repositories\ServiceRepository;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class ServiceController
 * @package frontend\controllers
 */
class ServicesController extends BaseController
{
    protected $services;

    public function __construct($id, $module, ServiceRepository $services, array $config = [])
    {
        $this->services = $services;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        $this->view->params['title'] = 'Услуги';
        $this->view->title = $this->view->params['title'];
        $this->view->registerMetaTag(['name' => 'description', 'content' => $this->view->title]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $this->view->title]);

        return $this->render('index');
    }

    public function actionView($id): string
    {
        if (!$service = $this->services->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $items = [];
        foreach (ServiceRepository::getList() as $item) {
            $items[] = [
                'label' => $item->name,
                'url'   => ['/services/' . $item->slug],
            ];
        }
        $route = Url::toRoute(['services/view', 'id' => $service->id]);

        $this->view->beginBlock('reserveButton');
        ?>
        <div class="btn-group mt-4" role="group">
            <a href="#" data-custom-scroll-to="reserve-now"
               class="btn btn-lg btn-primary text-uppercase">Зарезервировать</a>
        </div>
        <?
        $this->view->endBlock();

        $this->view->params['breadcrumbs'][] = [
            'label' => 'Услуги',
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $service->name,
        ];

        $this->view->registerMetaTag(['name' => 'description', 'content' => $service->meta->description]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $service->meta->keywords]);
        $this->view->title = $service->meta->title ? $service->meta->title : $service->title;
        $this->view->params['title'] = $service->title;

        return $this->render('view', [
            'service' => $service,
            'items'   => $items,
            'route'   => ltrim($route, '/'),
        ]);
    }
}