<?php

namespace frontend\controllers;

use frontend\repositories\PlaceRepository;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class PlaceController
 * @package frontend\controllers
 */
class PlacesController extends BaseController
{
    protected $places;

    public function __construct($id, $module, PlaceRepository $places, array $config = [])
    {
        $this->places = $places;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Площадки',
        ];
        $this->view->registerMetaTag(['name' => 'description', 'content' => $this->view->title]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $this->view->title]);
        $this->view->title = 'Площадки';
        $this->view->params['title'] = 'Площадки';

        return $this->render('index');
    }

    public function actionView($id): string
    {
        if (!$place = $this->places->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $items = [];
        foreach ($this->places::getList() as $item) {
            $items[] = [
                'label' => $item->name,
                'url'   => ['/places/' . $item->slug],
            ];
        }
        $route = Url::toRoute(['places/view', 'id' => $place->id]);
        $pictures = $place->pictures;

        $this->view->beginBlock('reserveButton');
        ?>
        <div class="btn-group mt-4" role="group">
            <a href="#" data-custom-scroll-to="reserve-now"
               class="btn btn-lg btn-primary text-uppercase">Зарезервировать</a>
        </div>
        <?
        $this->view->endBlock();

        $this->view->registerMetaTag(['name' => 'description', 'content' => $place->meta->description]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $place->meta->keywords]);
        $this->view->title = $place->meta->title ? $place->meta->title : $place->title;
        $this->view->params['title'] = $place->title;
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Площадки',
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $place->name,
        ];

        return $this->render('view', [
            'place'    => $place,
            'items'    => $items,
            'route'    => ltrim($route, '/'),
            'pictures' => $pictures,
        ]);
    }
}