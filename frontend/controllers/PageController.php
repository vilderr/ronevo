<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use backend\repositories\PageRepository;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends BaseController
{
    protected $pages;

    /**
     * PageController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param PageRepository   $pages
     * @param array            $config
     */
    public function __construct($id, $module, PageRepository $pages, array $config = [])
    {
        $this->pages = $pages;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $this->page = 'main';

        $this->view->beginBlock('introPhone');
        ?>
        <div class="d-flex justify-content-center align-items-center">
            <div class="intro-phone-icon"><i class="mdi mdi-phone"></i></div>
            <div class="intro-phone"><?= Yii::$app->params['supportPhone']; ?></div>
        </div>
        <?
        $this->view->endBlock();
        $this->view->beginBlock('introSlogan');
        ?>
        <p class="mt-4 lead d-none d-md-block">Пара предложений о охренительной услуге,
            которая порвет рынок, из которых я пойму, нахуй я должен нажать на кнопку
            внизу.</p>
        <?
        $this->view->endBlock();
        $this->view->beginBlock('reserveButton');
        ?>
        <div class="btn-group mt-4" role="group">
            <a href="#" data-custom-scroll-to="reserve-now"
               class="btn btn-lg btn-primary text-uppercase">Зарезервировать</a>
        </div>
        <?
        $this->view->endBlock();

        $this->view->params['title'] = 'Ronevo Catering';
        $this->view->title = $this->view->params['title'];
        $this->view->registerMetaTag(['name' => 'description', 'content' => $this->view->title]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $this->view->title]);

        return $this->render('index');
    }

    public function actionContacts()
    {
        $form = new ContactForm();

        if ($form->load(\Yii::$app->request->post()) && $form->sendEmail()) {
            Yii::$app->session->setFlash('mainContactFormSubmitted');
        }

        $this->view->params['title'] = 'Контакты';
        $this->view->title = $this->view->params['title'];
        $this->view->registerMetaTag(['name' => 'description', 'content' => $this->view->title]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $this->view->title]);

        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render('contacts', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (!$page = $this->pages->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->view->registerMetaTag(['name' => 'description', 'content' => $page->meta->description]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $page->meta->keywords]);
        $this->view->title = $page->meta->title ? $page->meta->title : $page->title;
        $this->view->params['title'] = $page->title;
        $this->view->params['breadcrumbs'][] = [
            'label' => $page->name,
        ];

        return $this->render('view', [
            'page' => $page,
        ]);
    }
}