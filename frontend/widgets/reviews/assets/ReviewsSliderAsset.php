<?php

namespace frontend\widgets\reviews\assets;

use yii\web\AssetBundle;

/**
 * Class ReviewsSliderAsset
 * @package frontend\widgets\reviews\assets
 */
class ReviewsSliderAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/reviews/assets/dist';

    public $depends = [
        'common\assets\owl\OwlCarouselAsset',
    ];


    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/reviewsslider.css?' . time(),
        ];
    }
}