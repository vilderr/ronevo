<?php

namespace frontend\widgets\reviews;


use backend\models\review\Review;
use frontend\widgets\reviews\assets\ReviewsSliderAsset;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class ReviewsSlider
 * @package frontend\widgets\reviews
 */
class ReviewsSlider extends Widget
{
    public $options = [];
    /** @var Review[] */
    public $items = [];
    public $title = 'Отзывы';
    public $slogan = 'Что говорят люди';

    public function init()
    {
        Html::addCssClass($this->options, ['widget' => 'owl owl-carousel owl-default']);
        parent::init();
    }

    public function run()
    {
        if (empty($this->items))
            return null;

        ReviewsSliderAsset::register($this->getView());

        $id = $this->options['id'];
        $js = <<<EOT
$(document).ready(function(){
    $('#$id').owlCarousel({
        loop:true,
        nav:true,
        dots:true,
        smartSpeed:1000,
        items: 1,
        navText: ["",""],
        navClass: ["owl-prev mdi mdi-chevron-left","owl-next mdi mdi-chevron-right"],
        mouseDrag: false
    });
});
EOT;
        $this->view->registerJs($js);

        return implode("\n", [
                Html::beginTag('section', ['class' => 'intro testimonials text-center']),
                Html::beginTag('div', ['class' => 'container']),
                Html::tag('div', Html::tag('span', $this->title), ['class' => 'h1 text-uppercase']),
                Html::tag('p', $this->slogan, ['class' => 'display-4 text-uppercase']),
                Html::tag('hr', '', ['class' => 'divider bg-mantis']),
                Html::beginTag('div', $this->options),
                $this->renderItems(),
                Html::endTag('div'),
                Html::endTag('div'),
                Html::endTag('section'),
            ]) . "\n";
    }

    protected function renderItems()
    {
        $ar = [];

        foreach ($this->items as $item) {
            $ar[] = implode("\n", [
                Html::beginTag('div'),
                Html::img($item->picture->getThumbFileUrl('file'), ['class' => 'rounded-circle img-thumbnail']),
                Html::beginTag('blockquote', ['class' => 'quote']),
                Html::tag('p', $item->name.' - '.$item->description, ['class' => 'lead text-uppercase text-normal pt-4']),
                Html::tag('p', Html::tag('q', $item->announcement)),
                Html::endTag('blockquote'),
                Html::endTag('div'),
            ]);
        }

        return implode("\n", $ar);
    }
}