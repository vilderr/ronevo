<?php

namespace frontend\widgets\forms;

use Yii;
use yii\base\Widget;
use frontend\models\ReservationForm;

/**
 * Class Reservation
 * @package frontend\widgets\forms
 */
class Reservation extends Widget
{
    /** @var  ReservationForm */
    public $model;

    public function run()
    {
        if ($this->model->load(\Yii::$app->request->post()) && $this->model->reserve()) {
            Yii::$app->session->setFlash('reservationFormSubmitted');
        }

        return $this->render('reserve', [
            'model' => $this->model,
        ]);
    }
}