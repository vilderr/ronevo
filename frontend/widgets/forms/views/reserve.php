<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $this  \yii\web\View
 * @var $model \frontend\models\ReservationForm
 */
?>
<?php yii\widgets\Pjax::begin(['id' => 'reservation-form']); ?>

<? if (Yii::$app->session->hasFlash('reservationFormSubmitted')): ?>
    <div class="text-center">
        <div class="h3 text-uppercase">Спасибо!</div>
        <p class="lead">Ваша заявка успешно зарегистрирована!</p>
    </div>
<? else: ?>
    <div class="text-center mb-4">
        <div class="h1 text-uppercase">Зарезервируйте сейчас</div>
        <p class="display-4">Назначьте день Вашего торжества</p>
    </div>
    <?php $form = ActiveForm::begin([
        'id'               => 'reservation-form',
        'options'          => ['data-pjax' => true],
        'validateOnChange' => false,
        'validateOnBlur'   => false,
    ]); ?>
    <div class="form-row align-items-center">
        <?= $form->field($model, 'name', ['options' => ['class' => 'col-lg-3 group']])->textInput(); ?>
        <?= $form->field($model, 'email', ['options' => ['class' => 'col-lg-3 group']])->textInput(); ?>
        <?= $form->field($model, 'phone', ['options' => ['class' => 'col-lg-3 group']])->textInput(); ?>
        <?= $form->field($model, 'date', ['options' => ['class' => 'col-lg-3 group']])->textInput(); ?>
    </div>
    <div class="text-center mt-2">
        <?= \yii\helpers\Html::submitButton('Зарезервировать', ['class' => 'btn btn-primary btn-lg']); ?>
    </div>

    <?php ActiveForm::end(); ?>
<? endif; ?>
<?php Pjax::end(); ?>
