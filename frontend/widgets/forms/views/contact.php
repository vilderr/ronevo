<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

/**
 * @var $this       \yii\web\View
 * @var $model      \frontend\models\ContactForm
 * @var $formId     string
 * @var $showLabels boolean
 */
?>
<div class="h6 text-uppercase mb-3">Напишите нам</div>
<?php Pjax::begin(); ?>
<? if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
    <p class="lead">Спасибо! Ваше сообщение отправлено!</p>
<? else: ?>
    <?php $form = ActiveForm::begin([
        'id'               => $formId,
        'options'          => ['data-pjax' => true],
        'validateOnChange' => false,
        'validateOnBlur'   => false,
        'fieldConfig'      => [
            'errorOptions' => ['class' => 'help-block small'],
        ],
    ]); ?>
    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Ваше имя:'])->label($showLabels); ?>
    <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail:'])->label($showLabels); ?>
    <?= $form->field($model, 'message')->textarea(['placeholder' => 'Сообщение:'])->label($showLabels); ?>
    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary text-uppercase']); ?>
    <?php ActiveForm::end(); ?>
<? endif; ?>
<?php Pjax::end(); ?>
