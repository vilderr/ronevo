<?php

namespace frontend\widgets\forms;

use Yii;
use yii\base\Widget;
use frontend\models\ContactForm;
use yii\helpers\StringHelper;

/**
 * Class Contact
 * @package frontend\widgets\forms
 */
class Contact extends Widget
{
    /** @var  ContactForm */
    public $model;
    public $formId;
    public $showLabels = true;

    public function init()
    {
        if (!$this->formId) {
            $this->formId = 'contact-' . Yii::$app->security->generateRandomString(6);
        }

        parent::init();
    }

    public function run()
    {
        if ($this->model->load(\Yii::$app->request->post()) && $this->model->sendEmail()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render('contact', [
            'model'      => $this->model,
            'formId'     => $this->formId,
            'showLabels' => $this->showLabels,
        ]);
    }
}