<?php

namespace frontend\widgets\place;

use backend\helpers\StringHelper;
use backend\models\place\Place;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Isotop
 * @package frontend\widgets\place
 */
class Isotop extends Widget
{
    public $options = [];
    public $filterOptions = [];
    /** @var Place[] */
    public $items = [];
    public $filterLabel = 'Выбор площадки';
    public $inline = false;

    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, ['widget' => 'isotope-wrap']);
        Html::addCssClass($this->filterOptions, ['widget' => 'isotope-filters']);

    }

    public function run()
    {
        if (empty($this->items))
            return null;

        return implode("\n", [
                Html::beginTag('div', $this->options),
                $this->renderFilter(),
                $this->renderLayout(),
                Html::endTag('div'),
            ]) . "\n";
    }

    protected function renderFilter(): string
    {
        return implode("\n", [
                Html::beginTag('div', $this->filterOptions),
                Html::beginTag('ul', ['class' => $this->inline ? 'list-inline' : 'list']),
                Html::beginTag('li', ['class' => $this->inline ? 'list-inline-item' : 'list-item']),
                Html::tag('button', $this->filterLabel, [
                    'class'                               => 'isotope-filters-toggle btn btn-primary',
                    'data-custom-toggle'                  => 'isotope',
                    ' data-custom-toggle-disable-on-blur' => 'true',
                ]),
                Html::beginTag('ul', ['id' => 'isotop', 'class' => 'list-inline isotope-filters-list text-uppercase font-weight-semibold']),

                Html::tag('li', Html::a('Все', '#', [
                    'class'               => 'active',
                    'data-isotope-filter' => '*',
                    'data-isotope-group'  => 'menu',
                ])),

                $this->renderFilterItems(),

                Html::endTag('ul'),
                Html::endTag('li'),
                Html::endTag('ul'),
                Html::endTag('div'),
            ]) . "\n";
    }

    protected function renderFilterItems(): string
    {
        $ar = [];
        foreach ($this->items as $item) {
            $ar[] = Html::tag('li', Html::a($item->name, '#', [
                'data-isotope-filter' => $item->slug,
                'data-isotope-group'  => 'menu',
            ]));
        }

        return implode("\n", $ar);
    }

    protected function renderLayout()
    {
        $argc = [];

        $ar[] = Html::beginTag('div', [
            'class'               => 'isotope',
            'data-isotope-layout' => 'fitRows',
            'data-isotope-group'  => 'menu',
        ]);
        $ar[] = Html::beginTag('div', ['class' => 'row']);

        foreach ($this->items as $item) {
            $ar[] = implode("\n", [
                Html::beginTag('div', [
                    'class'       => 'col col-lg-4 col-isotope isotope-item',
                    'data-filter' => $item->slug,
                ]),
                Html::beginTag('div', ['class' => 'row align-items-start']),
                Html::tag('div', Html::a(Html::img($item->getMainPictureSrc(), ['class' => 'img-fluid']), Url::toRoute([StringHelper::toLower($item->formName()) . 's/' . $item->slug])), ['class' => 'col image mb - 4 mb - lg - 0']),
                Html::tag('div', $item->announcement, ['class' => 'text text-left col-lg -6']),
                Html::endTag('div'),
                Html::endTag('div'),
            ]);
        }

        $ar[] = Html::endTag('div');
        $ar[] = Html::endTag('div');

        return implode("\n", $ar);
    }
}