<?php

namespace frontend\urls;

use yii\base\Object;
use yii\web\UrlRuleInterface;
use yii\caching\Cache;
use yii\base\InvalidParamException;
use backend\models\place\Place;
use frontend\repositories\PlaceRepository;

class PlaceUrlRule extends Object implements UrlRuleInterface
{
    protected $repository;
    protected $cache;

    public function __construct(PlaceRepository $repository, Cache $cache, array $config = [])
    {
        $this->repository = $repository;
        $this->cache = $cache;

        parent::__construct($config);
    }

    public function parseRequest($manager, $request)
    {
        $path = $request->pathInfo;

        $result = $this->cache->getOrSet(['place_route', 'path' => $path], function () use ($path) {
            if (!$place = $this->repository->findBySlug($this->getPathSlug($path))) {
                return ['id' => null, 'path' => null];
            }

            return ['id' => $place->id, 'path' => $this->getPlaceSlug($place)];
        });

        if (empty($result['id'])) {
            return false;
        }

        if ($path != $result['path']) {
            return false;
        }

        return ['places/view', ['id' => $result['id']]];
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'places/view') {
            if (empty($params['id'])) {
                throw new InvalidParamException('Empty id.');
            }
            $id = $params['id'];

            $url = $this->cache->getOrSet(['place_route', 'id' => $id], function () use ($id) {
                if (!$place = $this->repository->find($id)) {
                    return null;
                }

                return $this->getPlaceSlug($place);
            });

            if (!$url) {
                throw new InvalidParamException('Undefined id.');
            }

            unset($params['id']);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }

            return $url;
        }

        return false;
    }

    private function getPathSlug($path): string
    {
        $chunks = explode('/', $path);

        return end($chunks);
    }

    private function getPlaceSlug(Place $page): string
    {
        return 'places/' . $page->slug;
    }
}