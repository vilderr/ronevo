<?php

namespace frontend\urls;

use yii\base\Object;
use yii\web\UrlRuleInterface;
use yii\caching\Cache;
use yii\base\InvalidParamException;
use backend\models\service\Service;
use frontend\repositories\ServiceRepository;

/**
 * Class ServiceUrlRule
 * @package frontend\urls
 */
class ServiceUrlRule extends Object implements UrlRuleInterface
{
    protected $repository;
    protected $cache;

    public function __construct(ServiceRepository $repository, Cache $cache, array $config = [])
    {
        $this->repository = $repository;
        $this->cache = $cache;

        parent::__construct($config);
    }

    public function parseRequest($manager, $request)
    {
        $path = $request->pathInfo;

        $result = $this->cache->getOrSet(['service_route', 'path' => $path], function () use ($path) {
            if (!$service = $this->repository->findBySlug($this->getPathSlug($path))) {
                return ['id' => null, 'path' => null];
            }

            return ['id' => $service->id, 'path' => $this->getServiceSlug($service)];
        });

        if (empty($result['id'])) {
            return false;
        }

        if ($path != $result['path']) {
            return false;
        }

        return ['services/view', ['id' => $result['id']]];
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'services/view') {
            if (empty($params['id'])) {
                throw new InvalidParamException('Empty id.');
            }
            $id = $params['id'];

            $url = $this->cache->getOrSet(['service_route', 'id' => $id], function () use ($id) {
                if (!$service = $this->repository->find($id)) {
                    return null;
                }

                return $this->getServiceSlug($service);
            });

            if (!$url) {
                throw new InvalidParamException('Undefined id.');
            }

            unset($params['id']);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }

            return $url;
        }

        return false;
    }

    private function getPathSlug($path): string
    {
        $chunks = explode('/', $path);

        return end($chunks);
    }

    private function getServiceSlug(Service $service): string
    {
        return 'services/' . $service->slug;
    }
}