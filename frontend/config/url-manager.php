<?php
return [
    'class'               => 'yii\web\UrlManager',
    'hostInfo'            => $params['frontendHostInfo'],
    'baseUrl'             => '/',
    'enablePrettyUrl'     => true,
    'showScriptName'      => false,
    'enableStrictParsing' => true,
    'rules'               => [
        ''                      => 'page/index',
        '<_a:(contacts)>' => 'page/<_a>',
        ['class' => 'frontend\urls\PageUrlRule'],
        ['class' => 'frontend\urls\ServiceUrlRule'],
        ['class' => 'frontend\urls\PlaceUrlRule'],
        '<_c:[\w\-]+>'          => '<_c>/index',
    ],
];