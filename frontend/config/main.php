<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'aliases' => [
        '@staticRoot' => $params['staticPath'],
        '@static'   => $params['staticHostInfo'],
    ],
    'bootstrap'           => [
        'log',
        'common\bootstrap\Set',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'request'            => [
            'csrfParam'           => '_csrf-frontend',
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],
        'cache'       => [
            'class'     => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache',
        ],
        'user'               => [
            'identityClass'   => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie'  => [
                'name'     => '_identity',
                'httpOnly' => true,
                'domain'   => $params['cookieDomain'],
            ],
            'loginUrl'        => ['auth/login'],
        ],
        'session'            => [
            'name'         => '_session',
            'cookieParams' => [
                'domain'   => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log'               => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'          => 'yii\log\FileTarget',
                    'exportInterval' => 1,
                    'categories'     => ['info'],
                    'levels'         => ['info'],
                    'logFile'        => '@runtime/logs/info.log',
                    'logVars'        => [],
                ],
            ],
        ],
        'errorHandler'       => [
            'errorAction' => 'site/error',
        ],
        'frontendUrlManager' => require __DIR__ . '/url-manager.php',
        'urlManager'         => function () {
            return Yii::$app->get('frontendUrlManager');
        },
    ],
    'params'              => $params,
];
