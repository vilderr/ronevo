<?php

namespace frontend\repositories;

use backend\models\service\Service;

/**
 * Class ServiceRepository
 * @package frontend\repositories
 */
class ServiceRepository
{
    /**
     * @return Service[]
     */
    public static function getList()
    {
        return Service::find()->active()->orderBy('sort')->all();
    }

    /**
     * @param $id
     *
     * @return Service
     */
    public function find($id)
    {
        return Service::findOne(['id' => $id, 'status' => 1]);
    }

    /**
     * @param string $slug
     *
     * @return Service
     */
    public function findBySlug(string $slug)
    {
        return Service::findOne(['slug' => $slug, 'status' => 1]);
    }
}