<?php

namespace frontend\repositories;

use backend\models\place\Place;

class PlaceRepository extends \backend\repositories\PlaceRepository
{
    /**
     * @return Place[]
     */
    public static function getList()
    {
        return Place::find()->active()->orderBy('sort')->all();
    }

    /**
     * @param $id
     *
     * @return Place
     */
    public function find($id)
    {
        return Place::findOne(['id' => $id, 'status' => 1]);
    }

    /**
     * @param string $slug
     *
     * @return Place
     */
    public function findBySlug(string $slug)
    {
        return Place::findOne(['slug' => $slug, 'status' => 1]);
    }
}