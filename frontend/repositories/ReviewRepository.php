<?php

namespace frontend\repositories;


use backend\models\review\Review;

class ReviewRepository
{
    public static function getList()
    {
        return Review::find()->active()->orderBy('sort')->all();
    }
}