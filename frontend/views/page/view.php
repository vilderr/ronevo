<?php
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $page \backend\models\Page
 */
?>
<article class="container">

    <?= Yii::$app->formatter->asHtml($page->content, [
        'Attr.AllowedRel'      => array('nofollow'),
        'HTML.SafeObject'      => true,
        'Output.FlashCompat'   => true,
        'HTML.SafeIframe'      => true,
        'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
    ]) ?>

</article>
