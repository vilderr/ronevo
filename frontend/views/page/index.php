<?php
use frontend\widgets\place\Isotop;
use frontend\repositories\PlaceRepository;
use frontend\widgets\reviews\ReviewsSlider;
use frontend\repositories\ReviewRepository;
use kartik\icons\Icon;

/* @var $this yii\web\View */
?>
    <section class="intro-about text-center">
        <div class="container">
            <div class="row text-lg-left justify-content-sm-center">
                <div class="col-sm-10 col-lg-6 order-lg-2">
                    <div class="intro-video">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://www.youtube.com/embed/MG6jHxAfwQk?wmode=transparent"
                                    class="embed-responsive-item"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 col-lg-6 order-lg-1 mt-5 mt-lg-0">
                    <div class="pr-lg-3">
                        <div class="h1">Добро пожаловать</div>
                        <hr class="divider hr-lg-left-0 bg-mantis">
                        <p class="text-gray">Еще пара предложений о охренительной услуге, которая порвет рынок, из
                            которых я пойму, нахуй я должен нажать на кнопку внизу.
                            Пара предложений о охренительной услуге, которая порвет рынок, из
                            которых я пойму, нахуй я должен нажать на кнопку внизу.
                            Пара предложений о охренительной услуге, которая порвет рынок, из
                            которых я пойму, нахуй я должен нажать на кнопку внизу.
                            Пара предложений о охренительной услуге, которая порвет рынок, из
                            которых я пойму, нахуй я должен нажать на кнопку внизу.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="intro-places text-center">
        <div data-on="false" data-md-on="true" class="rd-parallax">
            <div data-speed="0.35" data-type="media" data-url="/images/p2-1920x963.jpg"
                 class="rd-parallax-layer"></div>
            <div data-speed="0" data-type="html" class="rd-parallax-layer">
                <div class="container">
                    <div class="row section-66 section-sm-110 align-items-center justify-content-center">
                        <div class="col text-uppercase text-white">
                            <div class="h1">Наши площадки</div>
                            <p class="display-4">Для Ваших незабываемых торжеств</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="places">
            <div class="container section-66">
                <?= Isotop::widget([
                    'items'         => PlaceRepository::getList(),
                    'filterOptions' => [
                        'class' => 'isotope-filters-horizontal',
                    ],
                    'inline'        => true,
                    'filterLabel'   => 'Выбор площадки' . Icon::show('angle-down'),
                ]); ?>
            </div>
        </div>
    </section>
<?= ReviewsSlider::widget([
    'options' => [
        'id' => 'reviews-slider',
    ],
    'items'   => ReviewRepository::getList(),
]); ?>