<?php
/**
 * @var $this    \yii\web\View
 * @var $model   \frontend\models\ContactForm
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

?>
<article class="container">
    <div class="row">
        <div class="col-lg-4">
            <div class="h2 text-uppercase text-center text-lg-left">Как нас найти</div>
            <hr class="divider bg-mantis ml-lg-0"/>
            <p class="text-gray">Пишите нам через форму обратной связи по всем интересующим Вас вопросам.
                Или используйте любой удобный Вам способ связи, указанный ниже</p>
            <ul class="list-unstyled">
                <li class="font-weight-semibold"><i
                            class="mdi mdi-phone-classic mr-2 text-gray"></i> <?= Yii::$app->params['supportPhone']; ?>
                </li>
                <li class="font-weight-semibold">
                    <i class="mdi mdi-mail-ru mr-2 text-gray"></i> <?= Html::a(Yii::$app->params['adminEmail'], Yii::$app->params['supportEmail'], ['class' => 'text-dark']); ?>
                </li>
            </ul>
            <hr class="divider bg-mantis ml-lg-0"/>
            <p>Вы также можете воспользоватся одной из социальных сетей. Мы на связи!</p>
        </div>
        <div class="col-lg-8">
            <div class="h2 text-uppercase text-center text-lg-left">Напишите</div>
            <hr class="divider bg-mantis ml-lg-0"/>
            <?php Pjax::begin(); ?>
            <? if (Yii::$app->session->hasFlash('mainContactFormSubmitted')): ?>
                <div class="alert alert-light">
                    <div class="d-flex align-items-center">
                        <i class="mdi mdi-telegram mdi-48px mr-3 text-info"></i> <span>Спасибо! Ваше сообщение отправлено!</span>
                    </div>
                </div>
            <? else: ?>
                <?php $form = ActiveForm::begin([
                    'options'          => ['data-pjax' => true],
                    'validateOnChange' => false,
                    'validateOnBlur'   => false,
                    'fieldConfig'      => [
                        'errorOptions' => ['class' => 'help-block small text-danger'],
                    ],
                ]); ?>
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <?= $form->field($model, 'name')->textInput(); ?>
                    </div>
                    <div class="col-12 col-lg-6">
                        <?= $form->field($model, 'email')->textInput(); ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'message')->textarea(['rows' => 8]); ?>
                    </div>
                    <div class="col-12">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-lg text-uppercase']); ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            <?php endif; ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</article>