<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\widgets\forms\Contact;
use frontend\models\ContactForm;
use frontend\widgets\forms\Reservation;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="wide wow-animation smoothscroll scrollTo">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.png" type="image/png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= $this->context->page; ?>">
<?php $this->beginBody() ?>
<div id="page">
    <header>
        <div class="rd-navbar-wrap">
            <nav class="rd-navbar" data-auto-height="true" data-md-layout="rd-navbar-fixed"
                 data-lg-layout="rd-navbar-static" data-lg-stick-up="true" role="navigation">
                <div class="rd-navbar-inner">
                    <div class="rd-navbar-panel">
                        <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle">
                            <span></span></button>
                        <div class="rd-navbar-brand d-flex align-items-end">
                            <a href="/"><?= Html::img('/images/logo.png'); ?></a>
                            <span class="rd-navbar-brand-slogan font-accent">Catering</span>
                        </div>
                    </div>
                    <div class="rd-navbar-menu-wrap">
                        <div class="rd-navbar-nav-wrap">
                            <div class="rd-navbar-mobile-scroll">
                                <div class="rd-navbar-mobile-brand">
                                    <a href="/"><?= Html::img('/images/logo.png'); ?></a>
                                </div>
                                <ul class="rd-navbar-nav">
                                    <li class="active"><a href="/">Главная</a></li>
                                    <li><a href="/about">О нас</a></li>
                                    <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                                        <a href="/services">Услуги</a>
                                        <ul class="rd-navbar-dropdown">
                                            <li><a href="/services/obsluzhivanie-svadeb">Обслуживание свадеб</a></li>
                                            <li><a href="/services/bankety">Банкеты</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                                        <a href="/places">Площадки</a>
                                        <ul class="rd-navbar-dropdown">
                                            <li><a href="/places/kremlevskiy-dvorets">Кремлевский дворец</a></li>
                                            <li><a href="/places/kvartira-putina">Квартира путина</a></li>
                                            <li><a href="/places/kakoy-to-saray">Какой то сарай</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/contacts">Контакты</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <main>
        <section class="intro text-white text-center">
            <div data-on="false" data-md-on="true" class="rd-parallax">
                <div data-speed="0.45" data-type="media" data-url="/images/p1-1920x955.jpg"
                     class="rd-parallax-layer"></div>
                <div data-speed="0.3" data-type="html" data-fade="true" class="rd-parallax-layer">
                    <div class="container">
                        <div class="row cover align-items-center justify-content-center">
                            <div class="col-md-8">
                                <img src="/images/intro-logo.png"/>
                                <h1 class="mt-4"><?= $this->params['title']; ?></h1>
                                <?= Breadcrumbs::widget([
                                    'tag'                => 'ol',
                                    'homeLink'           => [
                                        'label' => 'Главная',
                                        'url'   => '/',
                                    ],
                                    'links'              => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                    'itemTemplate'       => "<li class=\"breadcrumb-item\">{link}</li>\n",
                                    'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                                ]); ?>
                                <?php if (isset($this->blocks['introPhone'])): ?>
                                    <?= $this->blocks['introPhone']; ?>
                                <?php endif; ?>
                                <?php if (isset($this->blocks['introSlogan'])): ?>
                                    <?= $this->blocks['introSlogan']; ?>
                                <?php endif; ?>
                                <?php if (isset($this->blocks['reserveButton'])): ?>
                                    <?= $this->blocks['reserveButton'] ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="content">
            <?= $content; ?>
        </section>
    </main>
    <footer class="text-white">
        <div id="reserve-now" class="top context-dark">
            <div class="container">
                <?= Reservation::widget([
                    'model' => new \frontend\models\ReservationForm(),
                ]); ?>
            </div>
        </div>
        <div class="bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-12 order-xl-3 order-lg-3 mb-xl-0 mb-5">
                        <?= Contact::widget([
                            'model'      => new ContactForm(),
                            'showLabels' => false,
                        ]); ?>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 order-xl-2 order-lg-2 mb-xl-0 mb-5">
                        <div class="h6 text-uppercase mb-3">Меню</div>
                        <?= \frontend\widgets\menu\LeftMenu::widget([
                            'options'      => [
                                'class' => 'bottom-menu list-unstyled',
                            ],
                            'items'        => [
                                [
                                    'label' => 'Главная',
                                    'url'   => ['/index'],
                                ],
                                [
                                    'label' => 'О нас',
                                    'url'   => ['/about'],
                                ],
                                [
                                    'label' => 'Услуги',
                                    'url'   => ['/services'],
                                ],
                                [
                                    'label' => 'Площадки',
                                    'url'   => ['/places'],
                                ],
                                [
                                    'label' => 'Контакты',
                                    'url'   => ['/contacts'],
                                ],
                            ],
                            'encodeLabels' => false,
                        ]); ?>
                    </div>
                    <div class="col-xl-4 col-lg-12 order-xl-1 order-lg-4">
                        <div class="footer-brand"><a href="/"><?= Html::img('/images/logo.png'); ?></a></div>
                        <address class="contact-info pt-4">
                            <div>
                                <dl>
                                    <dt class="text-white font-weight-semibold">Адрес:</dt>
                                    <dd class="text-gray">
                                        г. Москва, ул. Первомайская, д. 16, оф. 223
                                    </dd>
                                </dl>
                            </div>
                            <div>
                                <dl>
                                    <dt class="text-white font-weight-semibold">Телефон:</dt>
                                    <dd class="text-gray">
                                        <?= Yii::$app->params['supportPhone']; ?>
                                    </dd>
                                </dl>
                            </div>
                            <div>
                                <dl>
                                    <dt class="text-white font-weight-semibold">Email:</dt>
                                    <dd class="text-gray">
                                        <?= Html::a(Yii::$app->params['supportEmail'], Yii::$app->params['supportEmail'], ['class' => 'text-gray']) ?>
                                    </dd>
                                </dl>
                            </div>
                        </address>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="text-center small pt-5 text-dark">Ronevo catering &copy; 2017</div>
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
                <button title="Share" class="pswp__button pswp__button--share"></button>
                <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
                <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
            <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php $this->endPage() ?>

