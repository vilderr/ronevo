<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */
?>
<?php $this->beginContent('@frontend/views/layouts/layout.php'); ?>
<?= $content; ?>
<?php $this->endContent(); ?>
