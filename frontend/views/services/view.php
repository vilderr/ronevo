<?php
/**
 * @var $this  \yii\web\View
 * @var $service \backend\models\service\Service
 * @var $items array
 * @var $route string
 */
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-3">
            <?= \frontend\widgets\menu\LeftMenu::widget([
                'options'      => [
                    'class' => 'left-menu list-unstyled',
                ],
                'items'        => $items,
                'encodeLabels' => false,
                'route'        => $route,
            ]); ?>
        </div>
        <div class="col-12 col-lg-9">
            <?= Yii::$app->formatter->asHtml($service->content, [
                'Attr.AllowedRel'      => array('nofollow'),
                'HTML.SafeObject'      => true,
                'Output.FlashCompat'   => true,
                'HTML.SafeIframe'      => true,
                'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
            ]) ?>
        </div>
    </div>
</div>
