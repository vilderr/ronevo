<?php
use frontend\widgets\place\Isotop;
use frontend\repositories\ServiceRepository;

/**
 * @var $this \yii\web\View
 */
?>
<div class="container" style="margin-top: -50px;">
    <?= Isotop::widget([
        'options'       => [
            'class' => 'text-center mb-4 mb-lg-5',
        ],
        'items'         => ServiceRepository::getList(),
        'filterOptions' => [
            'class' => 'isotope-filters-horizontal',
        ],
        'inline'        => true,
        'filterLabel'   => 'Выбор услуги',
    ]); ?>
</div>
