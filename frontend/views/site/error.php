<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->context->page = 'inner';
$this->title = $name;
?>
<div class="container text-center">
    <div class="h2 text-uppercase">Произошло непредвиденное :(</div>
    <hr class="divider bg-mantis" />
    <p>Вы можете нажать волшебную кнопку</p>
    <p><a href="#" data-custom-scroll-to="reserve-now"
          class="btn btn-lg btn-primary text-uppercase">Зарезервировать</a></p>
    <p>Или просто позвонить нам!</p>
    <div class="display-4">+7(495) 000-00-00</div>
</div>
