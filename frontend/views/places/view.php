<?php
use yii\helpers\Html;

/**
 * @var $this     \yii\web\View
 * @var $place    \backend\models\place\Place
 * @var $items    array
 * @var $route    string
 * @var $pictures \backend\models\place\Picture[]
 */
?>
<article class="container">
    <div class="row">
        <div class="col-12 col-lg-3">
            <?= \frontend\widgets\menu\LeftMenu::widget([
                'options'      => [
                    'class' => 'left-menu list-unstyled',
                ],
                'items'        => $items,
                'encodeLabels' => false,
                'route'        => $route,
            ]); ?>
        </div>
        <div class="col-12 col-lg-9">
            <?= Yii::$app->formatter->asHtml($place->content, [
                'Attr.AllowedRel'      => array('nofollow'),
                'HTML.SafeObject'      => true,
                'Output.FlashCompat'   => true,
                'HTML.SafeIframe'      => true,
                'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
            ]) ?>
            <?php if (!empty($pictures)): ?>
                <div class="h2 mb-3"><?= $place->name; ?> в картинках</div>
                <div data-photo-swipe-gallery class="row">
                    <?php foreach ($pictures as $picture): ?>
                        <div class="col-6 col-sm-4 col-md-3 col-lg-3 col-xl-2">
                            <?= Html::a(Html::tag('figure', Html::img($picture->getThumbFileUrl('file', 'small'), ['class' => 'img-thumbnail'])), $picture->getThumbFileUrl('file'), ['class' => 'thumbnail-classic', 'data-photo-swipe-item' => '', 'data-size' => '600x450']); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</article>
