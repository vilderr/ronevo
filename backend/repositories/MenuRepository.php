<?php

namespace backend\repositories;

use Yii;
use backend\models\Menu;
use yii\db\ActiveQuery;

class MenuRepository
{
    public function get($id): Menu
    {
        if (!$brand = Menu::findOne($id)) {
            throw new \DomainException('Menu item is not found.');
        }

        return $brand;
    }

    /**
     * @param Menu $item
     */
    public function save(Menu $item)
    {
        if (!$item->save()) {
            throw new \RuntimeException(Yii::t('app', 'Saving error'));
        }
    }

    /**
     * @param Menu $item
     */
    public function delete(Menu $item): void
    {
        if (!$item->deleteWithChildren()) {
            throw new \RuntimeException('Menu item removing error.');
        }
    }

    /**
     * Получаем подпункты меню
     * - для верхнего уровня, когда нет параметров
     * - для конкретного пункта меню, когда параметр [[item]] задан
     *
     * @param Menu|null $item
     * @param           $sort
     *
     * @return ActiveQuery
     */
    public static function getRoots(Menu $item = null, $sort = 'sort')
    {
        if ($item) {
            $rows = $item->getDescendants(1)->orderBy($sort);
        } else {
            $rows = Menu::find()->roots()->orderBy($sort);
        }

        return $rows;
    }

    /**
     * Получаем полное дерево меню ввиде массива
     * элементы которого отсортированы в порядке [[sort]]
     *
     * @param Menu|null $item
     * @param string    $sort
     *
     * @return array
     */
    public static function getTree(Menu $item = null, $sort = 'sort')
    {
        $tree = [];
        $rows = static::getRoots($item, $sort);

        /** @var Menu $row */
        foreach ($rows->each() as $row) {
            $item = [
                'label' => $row->name,
                'url'   => [$row->route],
                'icon'  => $row->icon,
            ];
            if (empty($item['url'])) {
                unset($item['url']);
            }

            $item['items'] = static::getTree($row, $sort);
            if (empty($item['items'])) {
                unset($item['items']);
            }

            $tree[] = $item;
        }

        return $tree;
    }
}