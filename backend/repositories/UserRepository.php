<?php

namespace backend\repositories;

use backend\models\user\User;

/**
 * Class UserRepository
 * @package backend\repositories
 */
class UserRepository
{
    /**
     * @param $id
     *
     * @return User
     */
    public function get($id)
    {
        if (!$user = User::findOne($id)) {
            throw new \DomainException('User is not found.');
        }

        return $user;
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param User $user
     */
    public function remove(User $user)
    {
        if ($user->id == 1) {
            throw new \RuntimeException('Нельзя удалить главного администратора!');
        }

        if (!$user->delete()) {
            throw new \RuntimeException('Ошибка удаления');
        }
    }

    /**
     * @param $username
     *
     * @return User
     */
    public function findActiveByUsername($username)
    {
        return User::findOne(['username' => $username, 'status' => User::STATUS_ACTIVE]);
    }

    /**
     * @param $value
     *
     * @return array|null|User
     */
    public function findByUsernameOrEmail($value)
    {
        return User::find()->andWhere(['or', ['username' => $value], ['email' => $value]])->one();
    }

    /**
     * @param $id
     *
     * @return User|null
     */
    public function findActiveById($id)
    {
        return User::findOne(['id' => $id, 'status' => User::STATUS_ACTIVE]);
    }
}