<?php

namespace backend\repositories;

use Yii;
use backend\models\review\Review;

/**
 * Class ReviewRepository
 * @package backend\repositories
 */
class ReviewRepository
{
    public function get($id): Review
    {
        if (!$review = Review::findOne($id)) {
            throw new \DomainException('Page is not found.');
        }

        return $review;
    }

    public function save(Review $review)
    {
        if (!$review->save()) {
            throw new \RuntimeException(Yii::t('app', 'Saving error'));
        }
    }

    public function delete(Review $review)
    {
        if (!$review->delete()) {
            throw new \RuntimeException('Place removing error.');
        }
    }
}