<?php

namespace backend\repositories;

use Yii;
use backend\models\Page;

/**
 * Class PageRepository
 * @package backend\repositories
 */
class PageRepository
{
    /**
     * @param $id
     *
     * @return Page
     */
    public function find($id)
    {
        return Page::findOne($id);
    }

    /**
     * @param $id
     *
     * @return Page
     */
    public function get($id)
    {
        if (!$page = Page::findOne($id)) {
            throw new \DomainException('Page is not found.');
        }

        return $page;
    }

    public function save(Page $page)
    {
        if (!$page->save()) {
            throw new \RuntimeException(Yii::t('app', 'Saving error'));
        }
    }

    public function delete(Page $page)
    {
        if (!$page->deleteWithChildren()) {
            throw new \RuntimeException('Menu item removing error.');
        }
    }

    public function getPages(Page $page = null, $sort = 'sort')
    {
        if ($page) {
            $rows = $page->getDescendants(1)->orderBy($sort);
        } else {
            $rows = Page::find()->roots()->orderBy($sort);
        }

        return $rows;
    }

    /**
     * @param $slug
     *
     * @return Page
     */
    public function findBySlug($slug)
    {
        return Page::findOne(['slug' => $slug]);
    }
}