<?php

namespace backend\repositories;

use Yii;
use backend\models\service\Service;

/**
 * Class ServiceRepository
 * @package backend\repositories
 */
class ServiceRepository
{
    public function get($id): Service
    {
        if (!$service = Service::findOne($id)) {
            throw new \DomainException('Page is not found.');
        }

        return $service;
    }

    public function save(Service $service)
    {
        if (!$service->save()) {
            throw new \RuntimeException(Yii::t('app', 'Saving error'));
        }
    }

    public function delete(Service $service)
    {
        if (!$service->delete()) {
            throw new \RuntimeException('Place removing error.');
        }
    }
}