<?php

namespace backend\repositories;

use Yii;
use backend\models\place\Place;

/**
 * Class PlaceRepository
 * @package backend\repositories
 */
class PlaceRepository
{
    public function get($id): Place
    {
        if (!$place = Place::findOne($id)) {
            throw new \DomainException('Page is not found.');
        }

        return $place;
    }

    public function save(Place $place)
    {
        if (!$place->save()) {
            throw new \RuntimeException(Yii::t('app', 'Saving error'));
        }
    }

    public function delete(Place $place)
    {
        if (!$place->delete()) {
            throw new \RuntimeException('Place removing error.');
        }
    }
}