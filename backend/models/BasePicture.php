<?php

namespace backend\models;

use backend\helpers\StringHelper;
use yii\web\UploadedFile;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%pictures}}".
 *
 * @property integer $id
 * @property string  $model_name
 * @property integer $model_id
 * @property string  $file
 */
class BasePicture extends ActiveRecord
{
    public static function create(UploadedFile $file, $model_name): self
    {
        $picture = new static();
        $picture->file = $file;
        $picture->model_name = StringHelper::toLower($model_name);

        return $picture;
    }

    public static function tableName(): string
    {
        return '{{%pictures}}';
    }
}