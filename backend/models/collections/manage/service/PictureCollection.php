<?php

namespace backend\models\collections\manage\service;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PictureCollection
 * @package backend\models\collections\manage\service
 */
class PictureCollection extends Model
{
    public $file;

    public function rules(): array
    {
        return [
            ['file', 'image', 'extensions' => 'png, jpg, jpeg, gif'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Картинка',
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->file = UploadedFile::getInstance($this, 'file');

            return true;
        }

        return false;
    }
}