<?php

namespace backend\models\collections\manage\service;

use backend\helpers\ServiceHelper;
use backend\models\collections\CompositeCollection;
use backend\models\service\Service;
use backend\models\collections\manage\MetaCollection;
use backend\models\validators\Slug;

/**
 * Class ServiceCollection
 * @package backend\models\collections\manage\service
 */
class ServiceCollection extends CompositeCollection
{
    public $name;
    public $title;
    public $slug;
    public $announcement;
    public $content;
    public $sort;
    public $status;

    public $service;

    public function __construct(Service $service = null, array $config = [])
    {
        if ($service) {
            $this->name = $service->name;
            $this->title = $service->title;
            $this->slug = $service->slug;
            $this->announcement = $service->announcement;
            $this->content = $service->content;
            $this->sort = $service->sort;
            $this->status = $service->status;
            $this->meta = new MetaCollection($service->meta);
        } else {
            $this->sort = ServiceHelper::DEFAULT_SORT;
            $this->status = ServiceHelper::DEFAULT_STATUS;
            $this->meta = new MetaCollection();
        }

        $this->service = $service;
        $this->picture = new PictureCollection();
        parent::__construct($config = []);
    }

    public function rules(): array
    {
        return [
            [['name', 'title', 'slug', 'sort'], 'required'],
            [['name', 'title', 'slug'], 'string', 'max' => 255],
            [['announcement', 'content'], 'string'],
            [['sort'], 'integer'],
            [['status'], 'boolean'],
            ['slug', Slug::class],
            [['slug'], 'unique', 'targetClass' => Service::class, 'filter' => $this->service ? ['<>', 'id', $this->service->id] : null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'         => 'Название',
            'title'        => 'Заголовок',
            'slug'         => 'Символьный код',
            'announcement' => 'Анонс',
            'content'      => 'Контент',
            'sort'         => 'Сортировка',
            'status'       => 'Опубликована',
        ];
    }

    public function attributePlaceholders()
    {
        return [
            'name'  => 'Введите название',
            'title' => 'Введите заголовок',
            'slug'  => 'Введите или сгенерируйте символьный код',
            'sort'  => 'Укажите порядок сортировки',
        ];
    }

    public function internalCollections()
    {
        return ['meta', 'picture'];
    }
}