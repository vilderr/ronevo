<?php

namespace backend\models\collections\manage;

use backend\models\collections\BaseCollection;
use backend\models\validators\Password;
use backend\models\validators\Phone;
use backend\models\validators\Username;
use Yii;
use backend\models\user\User;
use yii\helpers\ArrayHelper;

/**
 * Class UserCollection
 * @package backend\models\collections\manage
 */
class UserCollection extends BaseCollection
{
    const SCENARIO_CREATE = 'create';

    public $username;
    public $email;
    public $phone;
    public $password;
    public $status;
    public $role;

    private $_user;

    /**
     * UserCollection constructor.
     *
     * @param User|null $user
     * @param array     $config
     */
    public function __construct(User $user = null, array $config = [])
    {
        if ($user) {
            $roles = Yii::$app->authManager->getRolesByUser($user->id);

            $this->username = $user->username;
            $this->email = $user->email;
            $this->phone = $user->phone;
            $this->status = $user->status;
            $this->role = $roles ? reset($roles)->name : null;

            $this->_user = $user;
        } else {
            $this->status = User::STATUS_ACTIVE;
            $this->scenario = self::SCENARIO_CREATE;
        }

        parent::__construct($config);
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email'    => 'E-mail',
            'phone'    => 'Телефон',
            'status'   => 'Активность',
            'role'     => 'Группа',
        ];
    }

    public function attributePlaceholders()
    {
        return [
            'username' => 'Введите имя пользователя',
            'email'    => 'Введите адрес электронной почты',
            'phone'    => 'Введите номер телефона',
            'password' => 'Введите пароль',
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'email', 'phone', 'role'], 'required'],
            ['password', 'string', 'min' => 6, 'max' => 12],
            ['password', 'required', 'on' => self::SCENARIO_CREATE],
            ['password', Password::class],
            ['username', Username::class],
            ['phone', Phone::class],
            ['email', 'email'],
            [['username', 'email'], 'string', 'max' => 255],
            ['status', 'boolean'],
            [['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => $this->_user ? ['<>', 'id', $this->_user->id] : null],
        ];
    }

    /**
     * @return array
     */
    public function rolesList()
    {
        return ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
    }
}