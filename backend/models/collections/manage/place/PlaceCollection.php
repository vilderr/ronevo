<?php

namespace backend\models\collections\manage\place;

use backend\helpers\ModelHelper;
use backend\models\collections\CompositeCollection;
use backend\models\collections\manage\MetaCollection;
use backend\models\place\Place;
use backend\models\validators\Slug;

/**
 * Class PlaceCollection
 * @package backend\models\collections\manage\place
 */
class PlaceCollection extends CompositeCollection
{
    public $name;
    public $title;
    public $slug;
    public $announcement;
    public $content;
    public $sort;
    public $status;

    public $place;

    public function __construct(Place $place = null, array $config = [])
    {
        if ($place) {
            $this->name = $place->name;
            $this->title = $place->title;
            $this->slug = $place->slug;
            $this->announcement = $place->announcement;
            $this->content = $place->content;
            $this->sort = $place->sort;
            $this->status = $place->status;
            $this->meta = new MetaCollection($place->meta);

            $this->place = $place;
        } else {
            $this->sort = ModelHelper::DEFAULT_SORT;
            $this->status = ModelHelper::DEFAULT_STATUS;
            $this->meta = new MetaCollection();
        }

        $this->pictures = new PictureCollection();

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'title', 'slug', 'sort'], 'required'],
            [['name', 'title', 'slug'], 'string', 'max' => 255],
            [['announcement', 'content'], 'string'],
            [['sort'], 'integer'],
            [['status'], 'boolean'],
            ['slug', Slug::class],
            [['slug'], 'unique', 'targetClass' => Place::class, 'filter' => $this->place ? ['<>', 'id', $this->place->id] : null],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name'         => 'Название',
            'title'        => 'Заголовок',
            'slug'         => 'Символьный код',
            'sort'         => 'Сортировка',
            'announcement' => 'Анонс',
            'content'      => 'Контент',
            'status'       => 'Активность',
        ];
    }

    public function attributePlaceholders(): array
    {
        return [
            'name'  => 'Введите название',
            'title' => 'Введите заголовок',
            'slug'  => 'Введите или сгенерируйте символьный код',
            'sort'  => 'Укажите порядок сортировки',
        ];
    }

    public function internalCollections(): array
    {
        return ['meta', 'pictures'];
    }
}