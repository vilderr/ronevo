<?php

namespace backend\models\collections\manage\place;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PictureCollection
 * @package backend\models\collections\manage\place
 */
class PictureCollection extends Model
{
    public $files;

    public function rules(): array
    {
        return [
            ['files', 'each', 'rule' => ['image', 'extensions' => 'png, jpg, jpeg, gif']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'files' => 'Картинки',
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->files = UploadedFile::getInstances($this, 'files');

            return true;
        }

        return false;
    }
}