<?php

namespace backend\models\collections\manage;

use backend\models\collections\BaseCollection;
use backend\models\Menu;
use yii\base\Model;

/**
 * Class MenuCollection
 * @package backend\models\collections\manage
 */
class MenuCollection extends BaseCollection
{
    const DEFAULT_ICON = 'angle-right';

    public $name;
    public $sort;
    public $route;
    public $icon;

    public $parent;
    protected $item;

    /**
     * MenuCollection constructor.
     *
     * @param Menu|null $parent
     * @param Menu|null $item
     * @param array     $config
     */
    public function __construct(Menu $parent = null, Menu $item = null, array $config = [])
    {
        if ($item) {
            $this->name = $item->name;
            $this->sort = $item->sort;
            $this->route = $item->route;
            $this->icon = $item->icon;
        } else {
            $this->icon = self::DEFAULT_ICON;
            $this->sort = self::DEFAULT_SORT;
        }

        $this->parent = $parent;
        $this->item = $item;

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name', 'route', 'icon'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'  => 'Название',
            'route' => 'Путь',
            'icon'  => 'Иконка',
            'sort'  => 'Сортировка',
        ];
    }

    public function attributePlaceholders()
    {
        return [
            'name'  => 'Название пункта меню',
            'route' => 'Путь к странице',
            'icon'  => 'Иконка пункта меню',
            'sort'  => 'Сортировка пункта меню',
        ];
    }

    /**
     * @return bool
     */
    public function isNewRecord()
    {
        return $this->item ? false : true;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->parent ? false : true;
    }
}