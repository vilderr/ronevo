<?php

namespace backend\models\collections\manage;

use backend\helpers\ModelHelper;
use backend\models\Page;
use backend\models\collections\CompositeCollection;

/**
 * Class PageCollection
 * @package backend\models\collections\manage
 */
class PageCollection extends CompositeCollection
{
    public $name;
    public $title;
    public $slug;
    public $content;
    public $sort;
    public $status;

    public $parent;
    protected $page;

    public function __construct(Page $parent = null, Page $page = null, array $config = [])
    {
        if ($page) {
            $this->name = $page->name;
            $this->title = $page->title;
            $this->slug = $page->slug;
            $this->content = $page->content;
            $this->sort = $page->sort;
            $this->status = $page->status;

            $this->meta = new MetaCollection($page->meta);
        } else {
            $this->sort = ModelHelper::DEFAULT_SORT;
            $this->status = ModelHelper::DEFAULT_STATUS;

            $this->meta = new MetaCollection();
        }

        $this->parent = $parent;
        $this->page = $page;

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name', 'title', 'slug', 'sort'], 'required'],
            [['content'], 'string'],
            [['sort'], 'integer'],
            ['status', 'boolean'],
            [['name', 'title', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique', 'targetClass' => Page::class, 'filter' => $this->page ? ['<>', 'id', $this->page->id] : null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'    => 'Название',
            'title'   => 'Заголовок',
            'slug'    => 'Символьный код',
            'content' => 'Контент',
            'sort'    => 'Сортировка',
            'status'  => 'Опубликовано',
        ];
    }

    public function attributePlaceholders()
    {
        return [
            'name'  => 'Введите название',
            'title' => 'Введите заголовок',
            'slug'  => 'Введите или сгенерируйте символьный код',
            'sort'  => 'Укажите порядок сортировки',
        ];
    }

    public function internalCollections()
    {
        return ['meta'];
    }

    /**
     * @return bool
     */
    public function isNewRecord()
    {
        return $this->page ? false : true;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->parent ? false : true;
    }
}