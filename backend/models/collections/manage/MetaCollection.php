<?php

namespace backend\models\collections\manage;

use yii\base\Model;
use backend\models\Meta;

/**
 * Class MetaCollection
 * @package backend\models\collections\manage
 */
class MetaCollection extends Model
{
    public $title;
    public $description;
    public $keywords;

    public function __construct(Meta $meta = null, $config = [])
    {
        if ($meta) {
            $this->title = $meta->title;
            $this->description = $meta->description;
            $this->keywords = $meta->keywords;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['description', 'keywords'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'       => 'Заголовок страницы',
            'description' => 'Описание страницы',
            'keywords'    => 'Ключевые слова',
        ];
    }
}