<?php

namespace backend\models\collections\manage\review;

use backend\helpers\ModelHelper;
use backend\models\collections\CompositeCollection;
use backend\models\review\Review;

/**
 * Class ReviewCollection
 * @package backend\models\collections\manage\review
 */
class ReviewCollection extends CompositeCollection
{
    public $name;
    public $description;
    public $announcement;
    public $sort;
    public $status;

    public $review;

    public function __construct(Review $review = null, array $config = [])
    {
        if ($review) {
            $this->name = $review->name;
            $this->description = $review->description;
            $this->announcement = $review->announcement;
            $this->sort = $review->sort;
            $this->status = $review->status;
        } else {
            $this->sort = ModelHelper::DEFAULT_SORT;
            $this->status = ModelHelper::DEFAULT_STATUS;
        }

        $this->picture = new PictureCollection();
        $this->review = $review;

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['name', 'announcement', 'sort'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
            [['announcement'], 'string'],
            [['sort'], 'integer'],
            [['status'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'         => 'Имя пользователя',
            'description'  => 'Описание пользователя',
            'announcement' => 'Текст отзыва',
            'sort'         => 'Сортировка',
            'status'       => 'Статус',
        ];
    }

    public function attributePlaceholders()
    {
        return [
            'name'        => 'Введите имя пользовтеля, оставившего отзыв',
            'description' => 'Опишите пользователя',
            'sort'        => 'Введите индекс сортировки',
        ];
    }

    public function internalCollections()
    {
        return ['picture'];
    }
}