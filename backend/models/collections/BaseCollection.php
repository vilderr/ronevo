<?php

namespace backend\models\collections;

use yii\base\Model;

/**
 * Class BaseCollection
 * @package backend\models\collections
 */
abstract class BaseCollection extends Model
{
    const DEFAULT_ACTIVE = 1;
    const DEFAULT_SORT = 500;

    public function attributePlaceholders()
    {
        return [];
    }

    public function getAttributePlaceholder($attribute)
    {
        $labels = $this->attributePlaceholders();

        return isset($labels[$attribute]) ? $labels[$attribute] : $this->generateAttributeLabel($attribute);
    }
}