<?php

namespace backend\models\collections;

use Yii;
use yii\base\Model;

/**
 * Class LoginCollection
 * @package backend\models\collections
 */
class LoginCollection extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'   => Yii::t('app', 'Username or email'),
            'password'   => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember Me')
        ];
    }
}