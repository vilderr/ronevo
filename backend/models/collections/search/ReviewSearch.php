<?php

namespace backend\models\collections\search;

use backend\models\review\Review;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class ReviewSearch
 * @package backend\models\collections\search
 */
class ReviewSearch extends Model
{
    public $id;
    public $name;
    public $sort;
    public $status;

    public function rules(): array
    {
        return [
            [['id', 'sort', 'status'], 'integer'],
            ['name', 'safe'],
        ];
    }

    public function search($params): ActiveDataProvider
    {
        $query = Review::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'sort'   => $this->sort,
            'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}