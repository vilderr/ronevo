<?php

namespace backend\models\collections\search;

use backend\models\Page;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class PageSearch
 * @package backend\models\collections\search
 */
class PageSearch extends Model
{
    public $id;
    public $name;
    public $title;
    public $slug;
    public $sort;
    public $status;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'sort', 'status'], 'integer'],
            [['name', 'title', 'slug'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Page::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'sort'   => $this->sort,
            'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}