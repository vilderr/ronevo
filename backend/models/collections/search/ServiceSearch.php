<?php

namespace backend\models\collections\search;

use yii\base\Model;
use backend\models\service\Service;
use yii\data\ActiveDataProvider;

/**
 * Class ServiceSearch
 * @package backend\models\collections\search
 */
class ServiceSearch extends Model
{
    public $id;
    public $name;
    public $title;
    public $slug;
    public $sort;
    public $status;

    public function rules(): array
    {
        return [
            [['id', 'sort', 'status'], 'integer'],
            [['name', 'title', 'slug'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Service::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'sort'   => $this->sort,
            'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}