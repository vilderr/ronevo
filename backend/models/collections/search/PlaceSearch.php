<?php

namespace backend\models\collections\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\place\Place;

/**
 * Class PlaceSearch
 * @package backend\models\collections\search
 */
class PlaceSearch extends Model
{
    public $id;
    public $name;
    public $slug;
    public $sort;
    public $status;

    public function rules(): array
    {
        return [
            [['id', 'sort', 'status'], 'integer'],
            [['name', 'slug'], 'safe'],
        ];
    }

    public function search($params): ActiveDataProvider
    {
        $query = Place::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'sort'   => $this->sort,
            'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}