<?php

namespace backend\models\query;

use paulzi\nestedsets\NestedSetsQueryTrait;

/**
 * This is the ActiveQuery class for [[\backend\models\Page]].
 *
 * @see \backend\models\Page
 */
class PageQuery extends \yii\db\ActiveQuery
{
    use NestedSetsQueryTrait;
}
