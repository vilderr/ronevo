<?php

namespace backend\models\query;

use paulzi\nestedsets\NestedSetsQueryTrait;
use yii\db\ActiveQuery;

/**
 * Class MenuQuery
 * @package backend\models\query
 */
class MenuQuery extends ActiveQuery
{
    use NestedSetsQueryTrait;
}