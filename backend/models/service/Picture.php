<?php

namespace backend\models\service;

use backend\models\BasePicture;
use backend\models\behaviors\ImageUploadBehavior;

/**
 * Class Picture
 * @package backend\models\service
 * @mixin ImageUploadBehavior
 */
class Picture extends BasePicture
{
    public function behaviors()
    {
        return [
            'upload' => [
                'class'                 => ImageUploadBehavior::class,
                'attribute'             => 'file',
                'createThumbsOnRequest' => true,
                'filePath'              => '@common/upload/origin/[[attribute_model_name]]/[[id]].[[extension]]',
                'fileUrl'               => '@static/origin/[[attribute_model_name]]/[[id]].[[extension]]',
                'thumbPath'             => '@common/upload/thumbs/[[attribute_model_name]]/[[attribute_model_id]]/[[profile]]_[[id]].[[extension]]',
                'thumbUrl'              => '@static/thumbs/[[attribute_model_name]]/[[attribute_model_id]]/[[profile]]_[[id]].[[extension]]',
                'thumbs'                => [
                    'admin' => ['width' => 200, 'height' => 200, 'jpegQuality' => 100],
                    'thumb' => ['width' => 600, 'height' => 450, 'jpegQuality' => 75, 'resizeUp' => true],
                    'small' => ['width' => 120, 'height' => 90, 'jpegQuality' => 75, 'resizeUp' => true],
                ],
            ],
        ];
    }
}