<?php

namespace backend\models\service;

use backend\models\behaviors\MetaBehavior;
use backend\models\collections\manage\service\ServiceCollection;
use Yii;
use yii\behaviors\TimestampBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\web\UploadedFile;
use backend\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%services}}".
 *
 * @property integer   $id
 * @property string    $name
 * @property string    $title
 * @property string    $slug
 * @property string    $announcement
 * @property string    $content
 * @property integer   $picture_id
 * @property string    $meta_json
 * @property integer   $sort
 * @property integer   $created_at
 * @property integer   $updated_at
 * @property integer   $status
 *
 * @property Picture   $picture
 * @property Picture[] $pictures
 *
 * @mixin SaveRelationsBehavior
 */
class Service extends \yii\db\ActiveRecord
{
    public $meta;

    public static function create(ServiceCollection $collection): self
    {
        $service = new static();
        $service->name = $collection->name;
        $service->title = $collection->title;
        $service->slug = $collection->slug;
        $service->announcement = $collection->announcement;
        $service->content = $collection->content;
        $service->sort = $collection->sort;
        $service->status = $collection->status;
        $service->meta = $collection->meta;

        return $service;
    }

    public function edit(ServiceCollection $collection)
    {
        $this->name = $collection->name;
        $this->title = $collection->title;
        $this->slug = $collection->slug;
        $this->announcement = $collection->announcement;
        $this->content = $collection->content;
        $this->sort = $collection->sort;
        $this->status = $collection->status;
        $this->meta = $collection->meta;
    }

    public function addPicture(UploadedFile $picture)
    {
        $pictures = [Picture::create($picture, $this->formName())];
        $this->updatePictures($pictures);
    }

    public function removePicture($id)
    {
        if ($id == $this->picture->id) {
            $this->updatePictures([]);

            return;
        }

        throw new \DomainException('Picture is not found');
    }

    public function getMainPictureSrc($type = 'thumb')
    {
        return $this->picture ? $this->picture->getThumbFileUrl('file', $type) : Url::to(Yii::getAlias('@frontend/images/no_photo.png'));
    }

    protected function updatePictures($pictures)
    {
        $this->pictures = $pictures;
        $this->populateRelation('picture', reset($pictures));
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%services}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'name'         => 'Название',
            'title'        => 'Заголовок страницы',
            'slug'         => 'Символьный код',
            'announcement' => 'Анонс',
            'content'      => 'Контент',
            'picture_id'   => Yii::t('app', 'Picture ID'),
            'meta_json'    => Yii::t('app', 'Meta Json'),
            'sort'         => 'Сортировка',
            'created_at'   => Yii::t('app', 'Created At'),
            'updated_at'   => Yii::t('app', 'Updated At'),
            'status'       => 'Статус',
        ];
    }


    public function getPicture(): ActiveQuery
    {
        return $this->hasOne(Picture::className(), ['id' => 'picture_id'])->andWhere(['model_name' => StringHelper::toLower($this->formName())]);
    }

    public function getPictures(): ActiveQuery
    {
        return $this->hasMany(Picture::class, ['model_id' => 'id'])->andWhere(['model_name' => StringHelper::toLower($this->formName())]);
    }

    /**
     * @inheritdoc
     * @return \backend\models\service\query\ServicesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\service\query\ServicesQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'meta'      => MetaBehavior::class,
            'relations' => [
                'class'     => SaveRelationsBehavior::class,
                'relations' => ['pictures'],
            ],
        ];
    }

    public function beforeDelete(): bool
    {
        if (parent::beforeDelete()) {
            foreach ($this->pictures as $picture) {
                $picture->delete();
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        if (ArrayHelper::keyExists('picture', $related)) {
            $this->updateAttributes(['picture_id' => $related['picture'] ? $related['picture']->id : null]);
        }
    }
}
