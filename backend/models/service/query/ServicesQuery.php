<?php

namespace backend\models\service\query;

/**
 * This is the ActiveQuery class for [[\backend\models\service\Service]].
 *
 * @see \backend\models\service\Service
 */
class ServicesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    /**
     * @inheritdoc
     * @return \backend\models\service\Service[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\service\Service|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
