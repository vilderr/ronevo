<?php

namespace backend\models;

use backend\models\behaviors\MetaBehavior;
use backend\models\collections\manage\PageCollection;
use paulzi\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $title
 * @property string  $slug
 * @property string  $content
 * @property string  $meta_json
 * @property integer $sort
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $tree
 *
 * @mixin NestedSetsBehavior
 */
class Page extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 0;
    const STATUS_ACTIVE = 1;

    public $meta;

    /**
     * @param PageCollection $collection
     *
     * @return Page
     */
    public static function create(PageCollection $collection)
    {
        $page = new static();
        $page->name = $collection->name;
        $page->title = $collection->title;
        $page->slug = $collection->slug;
        $page->content = $collection->content;
        $page->meta = $collection->meta;
        $page->sort = $collection->sort;
        $page->status = $collection->status;

        return $page;
    }

    /**
     * @param PageCollection $collection
     */
    public function edit(PageCollection $collection)
    {
        $this->name = $collection->name;
        $this->title = $collection->title;
        $this->slug = $collection->slug;
        $this->content = $collection->content;
        $this->meta = $collection->meta;
        $this->sort = $collection->sort;
        $this->status = $collection->status;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'    => 'Название',
            'title'   => 'Заголовок',
            'slug'    => 'Символьный код',
            'content' => 'Контент',
            'sort'    => 'Сортировка',
            'status'  => 'Статус',
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\query\PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\PageQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'tree'      => [
                'class'         => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
            ],
            'meta'      => MetaBehavior::class,
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
}
