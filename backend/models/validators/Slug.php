<?php

namespace backend\models\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Class Slug
 * @package backend\models\validators
 */
class Slug extends RegularExpressionValidator
{
    public $pattern = '#^[a-z0-9_-]*$#s';
    public $message = 'Символьный код может содержать только цифры, знаки "-" и "_", латинские буквы в нижнем регистре';
}