<?php

namespace backend\models\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Class Password
 * @package backend\models\validators
 */
class Password extends RegularExpressionValidator
{
    public $pattern = '#^[a-z0-9_-]*$#s';
    public $message = 'Пароль должен содержать только цифры, знаки "-" и "_", латинские буквы в нижнем регистре';
}