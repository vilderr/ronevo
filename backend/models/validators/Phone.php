<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 28.09.17
 * Time: 14:28
 */

namespace backend\models\validators;


use yii\validators\RegularExpressionValidator;

class Phone extends RegularExpressionValidator
{
    public $pattern = '#^[0-9]*$#s';
    public $message = 'Номер телефона должен содержать только цифры без пробелов';
}