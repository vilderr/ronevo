<?php

namespace backend\models\place;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use backend\models\behaviors\MetaBehavior;
use backend\models\collections\manage\place\PlaceCollection;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\helpers\Url;
use yii\db\ActiveQuery;
use backend\helpers\StringHelper;

/**
 * This is the model class for table "{{%places}}".
 *
 * @property integer   $id
 * @property string    $name
 * @property string    $title
 * @property string    $slug
 * @property string    $announcement
 * @property string    $content
 * @property integer   $picture_id
 * @property string    $meta_json
 * @property integer   $sort
 * @property integer   $created_at
 * @property integer   $updated_at
 * @property integer   $status
 *
 * @property Picture[] $pictures
 * @property Picture   $picture
 */
class Place extends \yii\db\ActiveRecord
{
    public $meta;

    public static function create(PlaceCollection $collection): self
    {
        $place = new static();
        $place->name = $collection->name;
        $place->title = $collection->title;
        $place->slug = $collection->slug;
        $place->announcement = $collection->announcement;
        $place->content = $collection->content;
        $place->sort = $collection->sort;
        $place->status = $collection->status;
        $place->meta = $collection->meta;

        return $place;
    }

    public function edit(PlaceCollection $collection): void
    {
        $this->name = $collection->name;
        $this->title = $collection->title;
        $this->slug = $collection->slug;
        $this->announcement = $collection->announcement;
        $this->content = $collection->content;
        $this->sort = $collection->sort;
        $this->status = $collection->status;
        $this->meta = $collection->meta;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%places}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'name'         => 'Название',
            'title'        => 'Заголовок',
            'slug'         => 'Символьный код',
            'announcement' => 'Анонс',
            'content'      => 'Контент',
            'picture_id'   => Yii::t('app', 'Picture ID'),
            'meta_json'    => Yii::t('app', 'Meta Json'),
            'sort'         => 'Сортировка',
            'created_at'   => Yii::t('app', 'Created At'),
            'updated_at'   => Yii::t('app', 'Updated At'),
            'status'       => 'Статус',
        ];
    }

    // pictures

    public function addPicture(UploadedFile $picture)
    {
        $pictures = $this->pictures;
        $pictures[] = Picture::create($picture, $this->formName());
        $this->updatePictures($pictures);
    }

    public function removePicture($id)
    {
        $pictures = $this->pictures;
        foreach ($pictures as $key => $picture) {
            if ($id == $picture->id) {
                unset($pictures[$key]);
                $this->updatePictures($pictures);

                return;
            }
        }

        throw new \DomainException('Picture is not found');
    }

    public function getMainPictureSrc($type = 'thumb')
    {
        return $this->mainPicture ? $this->mainPicture->getThumbFileUrl('file', $type) : Url::to(Yii::getAlias('@frontend/images/no_photo.png'));
    }

    /**
     * @param Picture[] $pictures
     */
    public function updatePictures($pictures)
    {
        $this->pictures = $pictures;
        $this->populateRelation('mainPicture', reset($pictures));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPictures()
    {
        return $this->hasMany(Picture::className(), ['model_id' => 'id'])->andWhere(['model_name' => StringHelper::toLower($this->formName())]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPicture()
    {
        return $this->hasOne(Picture::className(), ['id' => 'picture_id'])->andWhere(['model_name' => StringHelper::toLower($this->formName())]);
    }

    /**
     * @return ActiveQuery
     */
    public function getMainPicture()
    {
        return $this->hasOne(Picture::class, ['id' => 'picture_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\place\query\PlacesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\place\query\PlacesQuery(get_called_class());
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function behaviors(): array
    {
        return [
            'timestamp'  => [
                'class' => TimestampBehavior::class,
            ],
            'meta'       => [
                'class' => MetaBehavior::class,
            ],
            'relations' => [
                'class'     => SaveRelationsBehavior::class,
                'relations' => ['pictures'],
            ],
        ];
    }

    public function beforeDelete(): bool
    {
        if (parent::beforeDelete()) {
            foreach ($this->pictures as $picture) {
                $picture->delete();
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        if (ArrayHelper::keyExists('mainPicture', $related)) {
            $this->updateAttributes(['picture_id' => $related['mainPicture'] ? $related['mainPicture']->id : null]);
        }
    }
}
