<?php

namespace backend\models\place\query;

/**
 * This is the ActiveQuery class for [[\backend\models\place\Place]].
 *
 * @see \backend\models\place\Place
 */
class PlacesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    /**
     * @inheritdoc
     * @return \backend\models\place\Place[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\place\Place|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
