<?php

namespace backend\models\review;

use backend\helpers\StringHelper;
use backend\models\collections\manage\review\ReviewCollection;
use Yii;
use yii\behaviors\TimestampBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property integer   $id
 * @property string    $name
 * @property string    $description
 * @property string    $announcement
 * @property integer   $picture_id
 * @property integer   $sort
 * @property integer   $created_at
 * @property integer   $updated_at
 * @property integer   $status
 *
 * @property Picture[] $pictures
 * @property Picture   $picture
 */
class Review extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 0;
    const STATUS_ACTIVE = 1;

    public static function create(ReviewCollection $collection): self
    {
        $review = new static();
        $review->name = $collection->name;
        $review->description = $collection->description;
        $review->announcement = $collection->announcement;
        $review->sort = $collection->sort;
        $review->status = $collection->status;

        return $review;
    }

    public function edit(ReviewCollection $collection)
    {
        $this->name = $collection->name;
        $this->description = $collection->description;
        $this->announcement = $collection->announcement;
        $this->sort = $collection->sort;
        $this->status = $collection->status;
    }

    public function addPicture(UploadedFile $picture)
    {
        $pictures = [Picture::create($picture, $this->formName())];
        $this->updatePictures($pictures);
    }

    public function removePicture($id)
    {
        if ($id == $this->picture->id) {
            $this->updatePictures([]);

            return;
        }

        throw new \DomainException('Picture is not found');
    }

    protected function updatePictures($pictures)
    {
        $this->pictures = $pictures;
        $this->populateRelation('picture', reset($pictures));
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    public static function find()
    {
        return new \backend\models\review\query\ReviewsQuery(get_called_class());
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'name'         => 'Имя пользователя',
            'description'  => 'Описание пользователя',
            'announcement' => 'Отзыв',
            'picture_id'   => Yii::t('app', 'Picture ID'),
            'sort'         => 'Сортировка',
            'created_at'   => Yii::t('app', 'Created At'),
            'updated_at'   => Yii::t('app', 'Updated At'),
            'status'       => 'Статус',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPicture()
    {
        return $this->hasOne(Picture::className(), ['id' => 'picture_id'])->andWhere(['model_name' => StringHelper::toLower($this->formName())]);
    }

    public function getPictures()
    {
        return $this->hasMany(Picture::class, ['model_id' => 'id'])->andWhere(['model_name' => StringHelper::toLower($this->formName())]);
    }


    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'relations' => [
                'class'     => SaveRelationsBehavior::class,
                'relations' => ['pictures'],
            ],
        ];
    }

    public function beforeDelete(): bool
    {
        if (parent::beforeDelete()) {
            foreach ($this->pictures as $picture) {
                $picture->delete();
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        if (ArrayHelper::keyExists('picture', $related)) {
            $this->updateAttributes(['picture_id' => $related['picture'] ? $related['picture']->id : null]);
        }
    }
}
