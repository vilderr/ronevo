<?php

namespace backend\models;

/**
 * Class Meta
 * @package backend\models
 */
class Meta
{
    public $title;
    public $description;
    public $keywords;

    public function __construct(string $title, string $description, string $keywords)
    {
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;
    }
}