<?php

namespace backend\models;

use backend\models\collections\manage\MenuCollection;
use backend\models\query\MenuQuery;
use Yii;
use paulzi\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "{{%backend_menu}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $route
 * @property string  $icon
 * @property integer $sort
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $tree
 *
 * @mixin NestedSetsBehavior
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @param MenuCollection $collection
     *
     * @return Menu
     */
    public static function create(MenuCollection $collection)
    {
        return new static([
            'name'  => $collection->name,
            'route' => $collection->route,
            'icon'  => $collection->icon,
            'sort'  => $collection->sort,
        ]);
    }

    /**
     * @param MenuCollection $collection
     */
    public function edit(MenuCollection $collection)
    {
        $this->name = $collection->name;
        $this->route = $collection->route;
        $this->icon = $collection->icon;
        $this->sort = $collection->sort;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%backend_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'  => 'Название',
            'route' => 'Путь',
            'icon'  => 'Иконка',
            'sort'  => 'Сортировка',
        ];
    }

    /**
     * @return MenuQuery
     */
    public static function find()
    {
        return new MenuQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'tree' => [
                'class'         => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
            ],
        ];
    }
}
