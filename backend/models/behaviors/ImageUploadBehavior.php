<?php

namespace backend\models\behaviors;

/**
 * Переопределеям методы родительского класса
 * - Добавляем опциональную возможность удаления оригиналов картинок
 * - Удаляем пустые папки элементов после удаления картинок/элементов
 *
 * Class ImageUploadBehavior
 * @package backend\models\behaviors
 */
class ImageUploadBehavior extends \yiidreamteam\upload\ImageUploadBehavior
{
    /**
     * После создания превью в методе родительского класса, удаляем оригиналы картинок
     * (Вынесем возможность опционального удаления оригиналов в отдельную настройку)
     */
    public function createThumbs()
    {
        parent::createThumbs();

        $path = $this->getUploadedFilePath($this->attribute);
        @unlink($path);
    }
}