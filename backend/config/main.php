<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-backend',
    'name'                => 'Ronevo',
    'basePath'            => dirname(__DIR__),
    'aliases' => [
        '@staticRoot' => $params['staticPath'],
        '@static'   => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute'        => 'default',
    'bootstrap'           => [
        'log',
        'common\bootstrap\Set',
    ],
    'modules'             => [
        'dynagrid' => [
            'class'           => '\kartik\dynagrid\Module',
            'dbSettings'      => [
                'tableName' => '{{%dynagrid}}',
            ],
            'dbSettingsDtl'   => [
                'tableName' => '{{%dynagrid_dtl}}',
            ],
            'dynaGridOptions' => [
                'storage'     => 'db',
                'gridOptions' => [
                    'toolbar' => [
                        '{dynagrid}',
                        '{toggleData}',
                    ],
                    'export'  => false,

                ],
            ],

        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['admin'],
            'plugin' => [
                [
                    'class'=>'\mihaildev\elfinder\plugin\Sluggable',
                    'lowercase' => true,
                    'replacement' => '-'
                ]
            ],
            'roots' => [
                [
                    'baseUrl'=>'@static',
                    'basePath'=>'@staticRoot',
                    'path' => 'files',
                    'name' => 'Global'
                ],
            ],
        ],
    ],
    'components'          => [
        'request'           => [
            'csrfParam'           => '_csrf-backend',
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],
        'user'              => [
            'identityClass'   => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie'  => [
                'name'     => '_identity',
                'httpOnly' => true,
                'domain'   => $params['cookieDomain'],
            ],
            'loginUrl'        => ['auth/login'],
        ],
        'session'           => [
            'name'         => '_session',
            'cookieParams' => [
                'domain'   => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log'               => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'          => 'yii\log\FileTarget',
                    'exportInterval' => 1,
                    'categories'     => ['info'],
                    'levels'         => ['info'],
                    'logFile'        => '@runtime/info.log',
                    'logVars'        => [],
                ],
            ],
        ],
        'errorHandler'      => [
            'errorAction' => 'default/error',
        ],
        'backendUrlManager' => require __DIR__ . '/url-manager.php',
        'urlManager'        => function () {
            return Yii::$app->get('backendUrlManager');
        },
    ],
    'as access'           => [
        'class'        => 'yii\filters\AccessControl',
        'except'       => ['auth/login', 'default/make-slug'],
        'rules'        => [
            [
                'allow' => true,
                'roles' => ['admin'],
            ],
        ],
        'denyCallback' => function ($rule, $action) {
            return \Yii::$app->getUser()->loginRequired();
        },
    ],
    'params'              => $params,
];
