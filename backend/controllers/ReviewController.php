<?php

namespace backend\controllers;

use backend\models\collections\search\ReviewSearch;
use Yii;
use backend\managers\ReviewManager;
use backend\models\collections\manage\review\ReviewCollection;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class ReviewController
 * @package backend\controllers
 */
class ReviewController extends Controller
{
    protected $manager;

    public function __construct($id, $module, ReviewManager $manager, array $config = [])
    {
        $this->manager = $manager;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Список отзывов';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Отзывы',
        ];

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $collection = new ReviewCollection();

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $review = $this->manager->create($collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        $this->redirect($returnUrl);
                        break;
                    default:
                        $this->redirect([
                            'update',
                            'id'        => $review->id,
                            'returnUrl' => $returnUrl,
                        ]);
                        break;
                }

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Новый отзыв';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Отзывы',
            'url'   => Url::to(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render('create', [
            'model' => $collection,
        ]);
    }

    public function actionUpdate($id)
    {
        $review = $this->manager->repository->get($id);
        $collection = new ReviewCollection($review);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $this->manager->edit($review, $collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'update',
                            'id'        => $review->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Редактирование отзыва';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Отзывы',
            'url'   => Url::to(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $review->name,
        ];

        return $this->render('update', [
            'model' => $collection,
        ]);
    }

    public function actionDelete($id)
    {
        try {
            $review = $this->manager->repository->get($id);
            $this->manager->remove($review);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        $this->redirect(['index']);
    }

    public function actionDeletePicture($id, $picture_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result['sucess'] = 'success';
        try {
            $this->manager->removePicture($id, $picture_id);
        } catch (\DomainException $e) {
            $result['error'] = $e->getMessage();
        }

        return $result;
    }
}