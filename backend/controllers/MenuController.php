<?php

namespace backend\controllers;

use backend\managers\MenuManager;
use backend\models\collections\manage\MenuCollection;
use Yii;
use backend\models\Menu;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BackendMenuController implements the CRUD actions for BackendMenu model.
 */
class MenuController extends Controller
{
    protected $manager;

    public function __construct($id, $module, MenuManager $manager, array $config = [])
    {
        $this->manager = $manager;
        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all BackendMenu models.
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
        $item = null;
        if (intval($id) > 0) {
            $item = $this->manager->repository->get($id);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $this->manager->repository::getRoots($item),
        ]);

        $chainItems = $this->manager->getNavChain($item);
        $this->view->title = 'Пункты меню';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
            'url'   => $item ? Url::toRoute('/menu') : null,
        ];

        foreach ($chainItems as $chain) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $chain->name,
                'url'   => Url::toRoute(['index', 'id' => $chain->id]),
            ];
        }

        if ($item) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $item->name,
            ];
        }

        return $this->render('index', [
            'model'        => $item,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new BackendMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = 0)
    {
        $parent = null;
        if (intval($id) > 0) {
            $parent = $this->manager->repository->get($id);
        }

        $collection = new MenuCollection($parent);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $item = $this->manager->create($collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $item->parent ? $item->parent->id : 0]));

                return $this->redirect($returnUrl);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Новый пункт меню';

        return $this->render('create', [
            'model' => $collection,
        ]);
    }

    /**
     * Updates an existing BackendMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $item = $this->manager->repository->get($id);

        $collection = new MenuCollection($item->parent, $item);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $this->manager->update($item, $collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $item->parent ? $item->parent->id : 0]));

                return $this->redirect($returnUrl);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Редактирование пункта меню';

        return $this->render('update', [
            'model' => $collection,
        ]);
    }

    /**
     * Deletes an existing BackendMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $item = $this->manager->repository->get($id);
            $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $item->parent ? $item->parent->id : 0]));
            $this->manager->remove($item);

            return $this->redirect($returnUrl);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}
