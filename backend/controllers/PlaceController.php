<?php

namespace backend\controllers;

use backend\models\collections\search\PlaceSearch;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\managers\PlaceManager;
use backend\models\collections\manage\place\PlaceCollection;
use yii\web\Response;

/**
 * Class PlaceController
 * @package backend\controllers
 */
class PlaceController extends Controller
{
    protected $manager;

    public function __construct($id, $module, PlaceManager $manager, array $config = [])
    {
        $this->manager = $manager;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex(): string
    {
        $searchModel = new PlaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Площадки';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate(): string
    {
        $collection = new PlaceCollection();

        if ($collection->load(\Yii::$app->request->post()) && $collection->validate()) {
            try {
                $place = $this->manager->create($collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        $this->redirect($returnUrl);
                        break;
                    default:
                        $this->redirect([
                            'update',
                            'id'        => $place->id,
                            'returnUrl' => $returnUrl,
                        ]);
                        break;
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Новая плащадка';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Площадки',
            'url'   => Url::to(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render('create', [
            'model' => $collection,
        ]);
    }

    public function actionUpdate($id)
    {
        $place = $this->manager->repostitory->get($id);
        $collection = new PlaceCollection($place);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $this->manager->edit($place, $collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'update',
                            'id'        => $place->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Редактирование площадки';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Площадки',
            'url'   => Url::to(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $place->name,
        ];

        return $this->render('update', [
            'model' => $collection,
        ]);
    }

    public function actionDeletePicture($id, $picture_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result['sucess'] = 'success';
        try {
            $this->manager->removePicture($id, $picture_id);
        } catch (\DomainException $e) {
            $result['error'] = $e->getMessage();
        }

        return $result;
    }

    public function actionDelete($id)
    {
        try {
            $place = $this->manager->repostitory->get($id);
            $this->manager->remove($place);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        $this->redirect(['index']);
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}