<?php

namespace backend\controllers;

use backend\models\collections\manage\service\ServiceCollection;
use Yii;
use yii\web\Controller;
use backend\managers\ServiceManager;
use backend\models\collections\search\ServiceSearch;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Class ServiceController
 * @package backend\controllers
 */
class ServiceController extends Controller
{
    protected $manager;

    public function __construct($id, $module, ServiceManager $manager, array $config = [])
    {
        $this->manager = $manager;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Список услуг';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Услуги',
        ];

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $collection = new ServiceCollection();

        if ($collection->load(\Yii::$app->request->post()) && $collection->validate()) {
            try {
                $service = $this->manager->create($collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        $this->redirect($returnUrl);
                        break;
                    default:
                        $this->redirect([
                            'update',
                            'id'        => $service->id,
                            'returnUrl' => $returnUrl,
                        ]);
                        break;
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Новая услуга';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Услуги',
            'url'   => Url::to(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
        ];

        return $this->render('create', [
            'model' => $collection,
        ]);
    }

    public function actionUpdate($id)
    {
        $service = $this->manager->repository->get($id);
        $collection = new ServiceCollection($service);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $this->manager->edit($service, $collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::to(['index']));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'update',
                            'id'        => $service->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Редактирование услуги';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'Услуги',
            'url'   => Url::to(['index']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $service->name,
        ];

        return $this->render('update', [
            'model' => $collection,
        ]);
    }

    public function actionDelete($id)
    {
        try {
            $place = $this->manager->repository->get($id);
            $this->manager->remove($place);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        $this->redirect(['index']);
    }

    public function actionDeletePicture($id, $picture_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result['sucess'] = 'success';
        try {
            $this->manager->removePicture($id, $picture_id);
        } catch (\DomainException $e) {
            $result['error'] = $e->getMessage();
        }

        return $result;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}