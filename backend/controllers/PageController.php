<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use backend\managers\PageManager;
use backend\models\collections\manage\PageCollection;
use backend\models\collections\search\PageSearch;

/**
 * Class PageController
 * @package backend\controllers
 */
class PageController extends Controller
{
    protected $manager;

    public function __construct($id, $module, PageManager $manager, array $config = [])
    {
        $this->manager = $manager;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex($id = 0): string
    {
        $page = null;
        if (intval($id) > 0) {
            $page = $this->manager->repository->get($id);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $this->manager->repository->getPages($page),
        ]);

        $chainItems = $this->manager->getNavChain($page);
        $this->view->title = 'Список страниц';
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
            'url'   => $page ? Url::toRoute('/page') : null,
        ];
        foreach ($chainItems as $chain) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $chain->name,
                'url'   => Url::toRoute(['index', 'id' => $chain->id]),
            ];
        }

        if ($page) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $page->name,
            ];
        }

        return $this->render('index', [
            'model'        => $page,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($id = 0): string
    {
        $parent = null;
        if (intval($id) > 0) {
            $parent = $this->manager->repository->get($id);
        }
        $collection = new PageCollection($parent);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $page = $this->manager->create($collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $page->parent ? $page->parent->id : 0]));

                $this->redirect($returnUrl);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Новая страница';

        return $this->render('create', [
            'model' => $collection,
        ]);
    }

    public function actionUpdate($id): string
    {
        $page = $this->manager->repository->get($id);
        $collection = new PageCollection($page->parent, $page);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $this->manager->update($page, $collection);
                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $page->parent ? $page->parent->id : 0]));

                $this->redirect($returnUrl);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Редактирование страницы';

        return $this->render('update', [
            'model' => $collection,
        ]);
    }

    public function actionDelete($id): void
    {
        try {
            $page = $this->manager->repository->get($id);
            $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $page->parent ? $page->parent->id : 0]));

            $this->manager->remove($page);

            $this->redirect($returnUrl);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        $this->redirect(['index']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}