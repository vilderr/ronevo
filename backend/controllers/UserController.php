<?php

namespace backend\controllers;

use backend\models\collections\manage\UserCollection;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use backend\managers\UserManager;
use backend\models\collections\search\UserSearch;

/**
 * Class UserController
 * @package backend\controllers
 */
class UserController extends Controller
{
    protected $manager;

    /**
     * UserController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param UserManager      $manager
     * @param array            $config
     */
    public function __construct($id, $module, UserManager $manager, array $config = [])
    {
        $this->manager = $manager;
        parent::__construct($id, $module, $config);
    }

    /**
     * Список пользователей с поиском
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Список пользователей';

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Добавдение нового пользователя
     *
     * @return string
     */
    public function actionCreate()
    {
        $collection = new UserCollection();

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $user = $this->manager->create($collection);

                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = 'Добавление нового пользователя';

        return $this->render('create', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function actionUpdate($id)
    {
        $user = $this->manager->repository->get($id);
        $collection = new UserCollection($user);

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $this->manager->update($user, $collection);

                $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        try {
            $this->manager->remove($id);
        } catch (\Exception $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}