<?php

namespace backend\controllers;

use Yii;
use yii\web\Response;
use backend\helpers\StringHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
/**
 * Class SlugController
 * @package backend\controllers
 */
class SlugController extends Controller
{
    public function actionMake($str)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return StringHelper::translit($str);
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'make' => ['POST'],
                ],
            ],
        ];
    }
}