<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\auth\Identity;
use backend\managers\AuthManager;
use backend\models\collections\LoginCollection;
use yii\filters\AccessControl;

/**
 * Class AuthController
 * @package backend\controllers
 */
class AuthController extends Controller
{
    public $layout = 'login';

    private $_manager;

    public function __construct($id, $module, AuthManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_manager = $manager;
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (Yii::$app->user->can('admin'))
            return $this->goHome();

        $collection = new LoginCollection();

        if ($collection->load(Yii::$app->request->post()) && $collection->validate()) {
            try {
                $user = $this->_manager->auth($collection);
                Yii::$app->user->login($user, $collection->rememberMe ? 3600 * 24 * 30 : 0);

                return $this->goBack();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = Yii::t('app', 'Authorization');

        return $this->render('login', [
            'model' => $collection,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}