<?php

namespace backend\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class MainMenuAsset
 * @package backend\widgets\assets
 */
class MainMenuAsset extends AssetBundle
{
    public $sourcePath = '@backend/widgets/assets/dist';

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\fonts\ProximaNovaAsset',
    ];

    public function init()
    {
        $this->css = [
            'css/mainmenu.css?' . time(),
        ];

        $this->js = [
            'js/mainmenu.js?' . time(),
        ];
    }
}