<?php

namespace backend\helpers;

/**
 * Class ReviewHelper
 * @package backend\helpers
 */
class ReviewHelper extends ModelHelper
{
    public static function statusList(): array
    {
        return [
            self::STATUS_WAIT   => 'Черновик',
            self::STATUS_ACTIVE => 'Опубликован',
        ];
    }
}