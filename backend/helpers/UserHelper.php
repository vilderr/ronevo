<?php

namespace backend\helpers;

use backend\models\user\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class UserHelper
 * @package backend\helpers
 */
class UserHelper
{
    public static function statusList(): array
    {
        return [
            User::STATUS_WAIT   => 'Не активен',
            User::STATUS_ACTIVE => 'Активен',
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case User::STATUS_WAIT:
                $class = 'label label-default';
                break;
            case User::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}