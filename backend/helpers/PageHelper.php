<?php

namespace backend\helpers;

use backend\models\Page;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class PageHelper
 * @package backend\helpers
 */
class PageHelper
{
    public static function statusList(): array
    {
        return [
            Page::STATUS_WAIT   => 'Черновик',
            Page::STATUS_ACTIVE => 'Опубликована',
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case Page::STATUS_WAIT:
                $class = 'label label-default';
                break;
            case Page::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}