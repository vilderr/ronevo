<?php

namespace backend\helpers;

/**
 * Class ServiceHelper
 * @package backend\helpers
 */
class ServiceHelper extends ModelHelper
{
    public static function statusList(): array
    {
        return [
            self::STATUS_WAIT   => 'Черновик',
            self::STATUS_ACTIVE => 'Опубликована',
        ];
    }
}