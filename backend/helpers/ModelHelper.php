<?php

namespace backend\helpers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ModelHelper
{
    const DEFAULT_SORT = 500;
    const DEFAULT_STATUS = 1;

    const STATUS_WAIT = 0;
    const STATUS_ACTIVE = 1;

    public static function statusList(): array
    {
        return [
            self::STATUS_WAIT   => 'Нет',
            self::STATUS_ACTIVE => 'Да',
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(static::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case self::STATUS_WAIT:
                $class = 'label label-default';
                break;
            case self::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(static::statusList(), $status), [
            'class' => $class,
        ]);
    }
}