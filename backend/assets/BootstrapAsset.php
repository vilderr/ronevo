<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class GridAsset
 * @package backend\assets
 */
class BootstrapAsset extends AssetBundle
{
    public $sourcePath = '@vendor/twbs/bootstrap/dist';

    public function init()
    {
        $this->publishOptions = [
            'forceCopy' => YII_ENV_DEV ? true : false,
        ];

        $this->css = [
            'css/bootstrap.min.css?' . time(),
        ];
    }
}