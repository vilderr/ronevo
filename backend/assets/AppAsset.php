<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Login & forgot backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\fonts\ProximaNovaAsset',
    ];

    public function init()
    {
        $this->css = [
            'css/app.css?' . time(),
        ];

        $this->js = [
            'js/app.js?' . time(),
        ];
    }
}