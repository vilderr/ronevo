<?php

namespace backend\managers;

use backend\models\collections\manage\review\ReviewCollection;
use backend\models\review\Review;
use backend\repositories\ReviewRepository;
use yii\web\UploadedFile;

/**
 * Class ReviewManager
 * @package backend\managers
 */
class ReviewManager
{
    public $repository;

    public function __construct(ReviewRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(ReviewCollection $collection)
    {
        $review = Review::create($collection);
        if ($collection->picture->file instanceof UploadedFile) {
            $review->addPicture($collection->picture->file);
        }

        $this->repository->save($review);

        return $review;
    }

    public function edit(Review $review, ReviewCollection $collection)
    {
        $review->edit($collection);
        if ($collection->picture->file instanceof UploadedFile) {
            $review->addPicture($collection->picture->file);
        }
        $this->repository->save($review);
    }

    public function remove(Review $place)
    {
        $this->repository->delete($place);
    }

    public function removePicture($id, $picture_id)
    {
        $review = $this->repository->get($id);
        $review->removePicture($picture_id);
        $this->repository->save($review);
    }
}