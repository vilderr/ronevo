<?php

namespace backend\managers;

use Yii;
use backend\models\collections\LoginCollection;
use backend\repositories\UserRepository;
use yii\rbac\Item;

class AuthManager
{
    private $_repository;

    public function __construct(UserRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function auth(LoginCollection $collection)
    {
        $user = $this->_repository->findByUsernameOrEmail($collection->username);
        if (!$user || !$user->isActive() || !$user->validatePassword($collection->password)) {
            throw new \DomainException(\Yii::t('app', 'Undefined user or password'));
        }

        if (!Yii::$app->authManager->checkAccess($user->id, 'admin')) {
            throw new \DomainException('Permission denied');
        }

        return $user;
    }
}