<?php

namespace backend\managers;

use backend\models\collections\manage\service\ServiceCollection;
use backend\models\service\Service;
use backend\repositories\ServiceRepository;
use yii\web\UploadedFile;

class ServiceManager
{
    public $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(ServiceCollection $collection)
    {
        $service = Service::create($collection);
        if ($collection->picture->file instanceof UploadedFile) {
            $service->addPicture($collection->picture->file);
        }

        $this->repository->save($service);

        return $service;
    }

    public function edit(Service $service, ServiceCollection $collection)
    {
        $service->edit($collection);
        if ($collection->picture->file instanceof UploadedFile) {
            $service->addPicture($collection->picture->file);
        }

        $this->repository->save($service);
    }

    public function remove(Service $service)
    {
        $this->repository->delete($service);
    }

    public function removePicture($id, $picture_id)
    {
        $place = $this->repository->get($id);
        $place->removePicture($picture_id);
        $this->repository->save($place);
    }
}