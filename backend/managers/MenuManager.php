<?php

namespace backend\managers;

use backend\models\collections\manage\MenuCollection;
use backend\models\Menu;
use backend\repositories\MenuRepository;
use yii\base\Model;

/**
 * Class MenuManager
 * @package backend\managers
 */
class MenuManager extends Model
{
    public $repository;

    public function __construct(MenuRepository $repository, array $config = [])
    {
        $this->repository = $repository;
        parent::__construct($config);
    }

    /**
     * @param MenuCollection $collection
     *
     * @return Menu
     */
    public function create(MenuCollection $collection)
    {
        $item = Menu::create($collection);

        if ($collection->isRoot()) {
            $item->makeRoot();
        } else {
            $item->appendTo($collection->parent);
        }

        $this->repository->save($item);

        return $item;
    }

    /**
     * @param Menu           $item
     * @param MenuCollection $collection
     */
    public function update(Menu $item, MenuCollection $collection)
    {
        $item->edit($collection);
        $this->repository->save($item);
    }

    public function remove(Menu $item)
    {
        $this->repository->delete($item);
    }

    /**
     * @param Menu|null $item
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNavChain(Menu $item = null)
    {
        $items = [];

        if ($item) {
            $items = $item->getParents()->all();
        }

        return $items;
    }
}