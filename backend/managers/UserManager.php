<?php

namespace backend\managers;

use backend\models\collections\manage\UserCollection;
use backend\models\user\User;
use backend\repositories\UserRepository;

/**
 * Class UserManager
 * @package backend\managers
 */
class UserManager
{
    public $repository;
    private $_roles;

    /**
     * UserManager constructor.
     *
     * @param UserRepository $repository
     * @param RoleManager    $roles
     */
    public function __construct(
        UserRepository $repository,
        RoleManager $roles
    )
    {
        $this->repository = $repository;
        $this->_roles = $roles;
    }

    /**
     * @param UserCollection $collection
     *
     * @return User
     */
    public function create(UserCollection $collection)
    {
        $user = User::create($collection);

        $this->repository->save($user);
        $this->_roles->assign($user->id, $collection->role);

        return $user;
    }

    public function update(User $user, UserCollection $collection)
    {
        $user->edit($collection);
        $this->repository->save($user);
        $this->_roles->assign($user->id, $collection->role);
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $user = $this->repository->get($id);
        $this->repository->remove($user);
    }
}