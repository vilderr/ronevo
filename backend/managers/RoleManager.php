<?php

namespace backend\managers;

use yii\rbac\ManagerInterface;

/**
 * Class RoleManager
 * @package backend\managers
 */
class RoleManager
{
    private $_manager;

    public function __construct(ManagerInterface $manager)
    {
        $this->_manager = $manager;
    }

    public function assign($userId, $name)
    {
        $am = $this->_manager;
        $am->revokeAll($userId);
        if (!$role = $am->getRole($name)) {
            throw new \DomainException('Role "' . $name . '" does not exist.');
        }
        $am->revokeAll($userId);
        $am->assign($role, $userId);
    }
}