<?php

namespace backend\managers;

use backend\models\collections\manage\place\PlaceCollection;
use backend\models\place\Place;
use backend\repositories\PlaceRepository;

/**
 * Class PlaceManager
 * @package backend\managers
 */
class PlaceManager
{
    public $repostitory;

    public function __construct(PlaceRepository $repository)
    {
        $this->repostitory = $repository;
    }

    public function create(PlaceCollection $collection): Place
    {
        $place = Place::create($collection);
        foreach ($collection->pictures->files as $file) {
            $place->addPicture($file);
        }

        $this->repostitory->save($place);

        return $place;
    }

    public function edit(Place $place, PlaceCollection $collection)
    {
        $place->edit($collection);
        foreach ($collection->pictures->files as $file) {
            $place->addPicture($file);
        }

        $this->repostitory->save($place);
    }

    public function remove(Place $place)
    {
        $this->repostitory->delete($place);
    }

    public function removePicture($id, $picture_id)
    {
        $place = $this->repostitory->get($id);
        $place->removePicture($picture_id);
        $this->repostitory->save($place);
    }
}