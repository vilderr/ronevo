<?php

namespace backend\managers;

use backend\models\collections\manage\PageCollection;
use backend\models\Page;
use backend\repositories\PageRepository;

/**
 * Class PageManager
 * @package backend\managers
 */
class PageManager
{
    public $repository;

    /**
     * PageManager constructor.
     *
     * @param PageRepository $repository
     */
    public function __construct(PageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(PageCollection $collection): Page
    {
        $page = Page::create($collection);

        if ($collection->isRoot()) {
            $page->makeRoot();
        } else {
            $page->appendTo($collection->parent);
        }

        $this->repository->save($page);

        return $page;
    }

    public function update(Page $page, PageCollection $collection): void
    {
        $page->edit($collection);
        $this->repository->save($page);
    }

    public function remove(Page $page): void
    {
        $this->repository->delete($page);
    }

    public function getNavChain(Page $page = null)
    {
        $items = [];

        if ($page) {
            $items = $page->getParents()->all();
        }

        return $items;
    }
}