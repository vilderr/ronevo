<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\review\ReviewCollection
 */
?>
<div class="backend-review-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>
</div>