<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\review\ReviewCollection
 */
?>
<div class="backend-review-form flat">
    <?php $form = ActiveForm::begin(); ?>
    <?= \yii\bootstrap\Tabs::widget([
        'id'          => 'review-form-tabs',
        'linkOptions' => [
            'class' => 'flat',
        ],
        'items'       => [
            [
                'label'   => 'Основные настройки',
                'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
            ],
        ],
    ]); ?>
    <div class="panel-footer">
        <div class="button-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary flat', 'name' => 'action', 'value' => 'save']) ?>
            <?= Html::submitButton(Yii::t('app', 'Apply'), ['class' => 'btn btn-default flat', 'name' => 'action', 'value' => 'apply']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default pull-right flat']); ?>
        </div>
    </div>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>
