<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/**
 * @var $model \backend\models\collections\manage\review\ReviewCollection
 * @var $form  \yii\widgets\ActiveForm
 */

$fileInputOptions = [
    'initialPreviewAsData' => true,
    'overwriteInitial'     => false,
    'showRemove'           => false,
    'showUpload'           => false,
    'showClose'            => false,
    'previewFileType'      => 'image',
];

if ($model->review && $model->review->picture) {
    $fileInputOptions['initialPreview'][] = $model->review->picture->getThumbFileUrl('file', 'admin');
    $fileInputOptions['initialPreviewConfig'][] = [
        'caption'  => $model->review->picture->file,
        'showDrag' => false,
        'url'      => Url::toRoute(['delete-picture', 'id' => $model->review->id, 'picture_id' => $model->review->picture->id]),
    ];
}
?>
<?= $form->field($model, 'name')->textInput([
    'maxlength'   => true,
    'id'          => 'place-name',
    'placeholder' => $model->getAttributePlaceholder('name'),
]); ?>
<?= $form->field($model, 'description')->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('description'),
]); ?>
<?= $form->field($model->picture, 'file')->widget(FileInput::class, [
    'options'       => ['multiple' => false, 'accept' => 'image/*'],
    'pluginOptions' => $fileInputOptions,
]); ?>
<?= $form->field($model, 'announcement')->textarea(); ?>
<?= $form->field($model, 'sort')->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('sort'),
]); ?>
<?= $form->field($model, 'status')->checkbox(); ?>
