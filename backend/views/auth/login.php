<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\LoginCollection
 */

$fieldOptions1 = [
    'options'       => ['class' => 'form-group has-feedback'],
    'inputOptions'  => ['class' => 'form-control form-control-sm rounded-0'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>",
];

$fieldOptions2 = [
    'options'       => ['class' => 'form-group has-feedback'],
    'inputOptions'  => ['class' => 'form-control form-control-sm rounded-0'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>",
];
?>
<div class="login-box text-dark">
    <div class="text-center h2">
        <span><b class="text-uppercase"><?= Yii::$app->name; ?></b> admin</span>
    </div>
    <div class="login-box-body flat">
        <p class="text-center"><?= Yii::t('app', 'Sign in to start your session'); ?></p>
        <?= \backend\widgets\Alert::widget(['options' => ['class' => 'flat']]); ?>
        <? $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="align-items-center">
            <?= $form->field($model, 'rememberMe', ['options' => ['class' => 'form-check mb-0']])->label(null, ['class' => 'form-check-label pt-3'])->checkbox(['class' => 'form-check-input']) ?>
            <!-- /.col -->
            <?= Html::submitButton(Yii::t('app', 'Sign in'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
