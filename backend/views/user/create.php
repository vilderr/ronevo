<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\UserCollection
 */
?>
<div class="backend-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
