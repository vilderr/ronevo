<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\UserCollection
 */
?>
<div class="backend-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
