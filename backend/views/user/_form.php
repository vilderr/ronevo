<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\collections\manage\UserCollection */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default flat">
    <div class="backend-menu-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="panel-body">
            <?= $form->field($model, 'username')->textInput([
                'maxlength'   => true,
                'placeholder' => $model->getAttributePlaceholder('username'),
            ]); ?>

            <?= $form->field($model, 'email')->textInput([
                'maxlength'   => true,
                'placeholder' => $model->getAttributePlaceholder('email'),
            ]); ?>

            <?= $form->field($model, 'phone')->textInput([
                'maxlength'   => true,
                'placeholder' => $model->getAttributePlaceholder('phone'),
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput([
                'placeholder' => $model->getAttributePlaceholder('password'),
            ]); ?>

            <?= $form->field($model, 'status')->checkbox(); ?>
            <?= $form->field($model, 'role')->dropDownList($model->rolesList()) ?>
        </div>
        <div class="panel-footer">
            <div class="button-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary flat']) ?>
                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default flat']); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>