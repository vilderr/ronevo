<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use backend\models\user\User;
use backend\widgets\grid\RoleColumn;
use backend\helpers\UserHelper;
use kartik\icons\Icon;

/**
 * @var $this         \yii\web\View
 * @var $searchModel  \backend\models\collections\search\UserSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<?= DynaGrid::widget([
    'options'           => [
        'id'    => 'users-grid',
        'class' => 'flat',
    ],
    'columns'           => [
        'id',
        [
            'attribute' => 'username',
            'value'     => function (User $model) {
                return Html::a(Html::encode($model->username), ['update', 'id' => $model->id]);
            },
            'format'    => 'raw',
        ],
        'email:email',
        [
            'attribute' => 'role',
            'class'     => RoleColumn::class,
            'filter'    => $searchModel->rolesList(),
        ],
        [
            'attribute' => 'status',
            'filter'    => UserHelper::statusList(),
            'value'     => function (User $model) {
                return UserHelper::statusLabel($model->status);
            },
            'format'    => 'raw',
        ],
        [
            'class'          => 'yii\grid\ActionColumn',
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'template'       => '<div class="btn-group">{update}{delete}</div>',
            'buttons'        => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                },
            ],
            'options'        => [
                'width' => '90px',
            ],
        ],
    ],
    'theme'             => 'panel-default',
    'gridOptions'       => [
        'dataProvider' => $dataProvider,
        'hover'        => true,
        'panel'        => [
            'before' => Html::a(Icon::show('plus') . 'Добавить пользователя', ['create'], ['class' => 'btn btn-primary']),
            'after'  => false,
        ],
    ],
    'allowSortSetting'  => false,
    'allowThemeSetting' => false,
]); ?>
