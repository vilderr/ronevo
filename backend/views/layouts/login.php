<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

/**
 * @var $this    \yii\web\View
 * @var $content string
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login">
<?php $this->beginBody() ?>

<div class="container wrapper">
    <div id="login-wrapper" class="row">
        <div class="col-lg-4 col-md-5 col-sm-6 col-11 center-box"><?= $content ?></div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
