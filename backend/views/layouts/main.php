<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use kartik\icons\Icon;
use yii\widgets\Breadcrumbs;
use backend\widgets\MainMenu;
use backend\widgets\Alert;

Icon::map($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">
    <header class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="/" class="navbar-brand"><strong><?= Yii::$app->name; ?></strong> admin</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item"><a
                            href="<?= Yii::$app->params['frontendHostInfo']; ?>"><?= Icon::show('desktop', ['class' => 'icon']); ?>
                        Перейти на сайт</a></li>
                <li class="nav-item"><a href="#"><?= Icon::show('user', ['class' => 'icon']); ?>Сергей Димитриев</a>
                </li>
                <li class="nav-item"><?= Html::a(Icon::show('power-off'), ['/auth/logout'], ['data-method' => 'post', 'title' => Yii::t('app', 'Logout')]) ?></li>
            </ul>
        </div>
    </header>
    <aside class="sidebar">
        <?= MainMenu::widget([
            'options'         => [
                'class' => 'sidebar-menu',
            ],
            'submenuTemplate' => '<ul class="treeview-menu">{items}</ul>',
        ]); ?>
    </aside>
    <section class="content">
        <div class="clearfix" style="height: 100%">
            <div class="main">
                <div class="content-header">
                    <h1><?= $this->title; ?></h1>
                    <?= Breadcrumbs::widget([
                        'options'  => [
                            'class' => 'breadcrumb flat',
                        ],
                        'homeLink' => [
                            'label' => Yii::t('app', 'Dashboard'),
                            'url'   => '/',
                        ],
                        'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
                <div class="content-body container-fluid">
                    <?= Alert::widget(['options' => ['class' => 'flat']]) ?>
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </section>
    <footer></footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
