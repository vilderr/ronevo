<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\service\ServiceCollection
 */
?>
<div class="backend-service-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>