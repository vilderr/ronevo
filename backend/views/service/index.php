<?php
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\icons\Icon;
use backend\helpers\ServiceHelper;
use backend\models\service\Service;

/**
 * @var $this         \yii\web\View
 * @var $searchModel  \backend\models\collections\search\ServiceSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<?= DynaGrid::widget([
    'options'           => [
        'id'    => 'reviews-grid',
        'class' => 'flat',
    ],
    'columns'           => [
        'id',
        [
            'attribute' => 'name',
            'value'     => function ($model) {
                return Html::a($model->name, ['update', 'id' => $model->id]);
            },
            'format'    => 'raw',
        ],
        'sort',
        [
            'attribute' => 'status',
            'filter'    => ServiceHelper::statusList(),
            'value'     => function (Service $model) {
                return ServiceHelper::statusLabel($model->status);
            },
            'format'    => 'raw',
        ],
        [
            'class'          => 'yii\grid\ActionColumn',
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'template'       => '<div class="btn-group">{update}{delete}</div>',
            'buttons'        => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                },
            ],
            'options'        => [
                'width' => '90px',
            ],
        ],
    ],
    'theme'             => 'panel-default',
    'gridOptions'       => [
        'dataProvider' => $dataProvider,
        'hover'        => true,
        'panel'        => [
            'heading' => 'Услуги',
            'before'  => Html::a(Icon::show('plus') . 'Добавить услугу', ['create'], ['class' => 'btn btn-primary']),
            'after'   => false,
        ],
    ],
    'allowSortSetting'  => false,
    'allowThemeSetting' => false,
]); ?>
