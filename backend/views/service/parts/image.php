<?php
use yii\helpers\Url;
use kartik\widgets\FileInput;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\service\ServiceCollection
 * @var $form  \backend\widgets\ActiveForm
 */
$fileInputOptions = [
    'initialPreviewAsData' => true,
    'overwriteInitial'     => false,
    'showRemove'           => false,
    'showUpload'           => false,
    'showClose'            => false,
    'previewFileType'      => 'image',
];
if ($model->service && $model->service->picture) {
    $fileInputOptions['initialPreview'][] = $model->service->picture->getThumbFileUrl('file', 'admin');
    $fileInputOptions['initialPreviewConfig'][] = [
        'caption'  => $model->service->picture->file,
        'showDrag' => false,
        'url'      => Url::toRoute(['delete-picture', 'id' => $model->service->id, 'picture_id' => $model->service->picture->id]),
    ];
}
?>
<?= $form->field($model->picture, 'file')->widget(FileInput::class, [
    'options'       => ['multiple' => false, 'accept' => 'image/*'],
    'pluginOptions' => $fileInputOptions,
]); ?>
