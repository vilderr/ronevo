<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\service\ServiceCollection
 * @var $form  \backend\widgets\ActiveForm
 */
?>
<?= $form->field($model, 'name')->textInput([
    'maxlength'   => true,
    'id'          => 'page-name',
    'placeholder' => $model->getAttributePlaceholder('name'),
])->hint('Название отображается в пунктах меню на сайте', ['class' => 'text-muted']); ?>

<?= $form->field($model, 'title')->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('title'),
])->hint('Заголовок страницы', ['class' => 'text-muted']); ?>

<?= $form->field($model, 'slug', ['makeSlug' => "#page-name"])->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('slug'),
])->hint('Код страницы для ЧПУ', ['class' => 'text-muted']); ?>

<?= $form->field($model, 'sort')->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('sort'),
])->hint('Порядок сортировки страниц в меню, карте сайта итд.', ['class' => 'text-muted']); ?>

<?= $form->field($model, 'status')->checkbox(); ?>
