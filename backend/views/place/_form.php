<?php
use backend\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\place\PlaceCollection
 */
?>
<div class="backend-place-form flat">

    <?php $form = ActiveForm::begin(); ?>
    <?= \yii\bootstrap\Tabs::widget([
        'id'          => 'place-form-tabs',
        'linkOptions' => [
            'class' => 'flat',
        ],
        'items'       => [
            [
                'label'   => 'Основные настройки',
                'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
            ],
            [
                'label'   => 'Анонс',
                'content' => $this->render('parts/announcement', ['form' => $form, 'model' => $model]),
            ],
            [
                'label'   => 'Контент',
                'content' => $this->render('parts/content', ['form' => $form, 'model' => $model]),
            ],
            [
                'label'   => 'Настройки SEO',
                'content' => $this->render('parts/seo', ['form' => $form, 'model' => $model]),
            ],
        ],
    ]); ?>
    <div class="panel-footer">
        <div class="button-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary flat', 'name' => 'action', 'value' => 'save']) ?>
            <?= Html::submitButton(Yii::t('app', 'Apply'), ['class' => 'btn btn-default flat', 'name' => 'action', 'value' => 'apply']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default pull-right flat']); ?>
        </div>
    </div>
    <? ActiveForm::end(); ?>
</div>
