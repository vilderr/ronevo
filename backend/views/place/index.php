<?php
use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;
use kartik\icons\Icon;
use backend\models\place\Place;
use backend\helpers\PageHelper;

/**
 * @var $this         \yii\web\View
 * @var $searchModel  \backend\models\collections\search\PlaceSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<?= DynaGrid::widget([
    'options'           => [
        'id'    => 'places-grid',
        'class' => 'flat',
    ],
    'columns'           => [
        'id',
        [
            'attribute' => 'name',
            'value' => function (Place $model) {
                return Html::a($model->name, ['update', 'id' => $model->id]);
            },
            'format'    => 'raw',
        ],
        'sort',
        [
            'attribute' => 'status',
            'filter'    => PageHelper::statusList(),
            'value'     => function (Place $model) {
                return PageHelper::statusLabel($model->status);
            },
            'format'    => 'raw',
        ],
        [
            'class'          => 'yii\grid\ActionColumn',
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'template'       => '<div class="btn-group">{update}{delete}</div>',
            'buttons'        => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                },
            ],
            'options'        => [
                'width' => '90px',
            ],
        ],
    ],
    'theme'             => 'panel-default',
    'gridOptions'       => [
        'dataProvider' => $dataProvider,
        'hover'        => true,
        'panel'        => [
            'before' => Html::a(Icon::show('plus') . 'Добавить площадку', ['create'], ['class' => 'btn btn-primary']),
            'after'  => false,
        ],
    ],
    'allowSortSetting'  => false,
    'allowThemeSetting' => false,
]); ?>
