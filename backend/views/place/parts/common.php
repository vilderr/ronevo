<?php

use kartik\widgets\FileInput;
use yii\helpers\Url;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\place\PlaceCollection
 * @var $form  \backend\widgets\ActiveForm
 */

$pluginOptions = [
    'initialPreviewAsData' => true,
    'overwriteInitial'     => false,
    'showRemove'           => false,
    'showUpload'           => false,
    'showClose'            => false,
    'previewFileType'      => 'image',
];

if ($model->place instanceof \backend\models\place\Place) {
    foreach ($model->place->pictures as $picture) {
        $pluginOptions['initialPreview'][] = $picture->getThumbFileUrl('file', 'admin');
        $pluginOptions['initialPreviewConfig'][] = [
            'caption'  => $picture->file,
            'showDrag' => false,
            'url'      => Url::toRoute(['delete-picture', 'id' => $model->place->id, 'picture_id' => $picture->id]),
        ];
    }
}
?>
<?= $form->field($model, 'name')->textInput([
    'maxlength'   => true,
    'id'          => 'place-name',
    'placeholder' => $model->getAttributePlaceholder('name'),
])->hint('Название отображается в пунктах меню на сайте', ['class' => 'text-muted']); ?>
<?= $form->field($model, 'title')->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('title'),
])->hint('Заголовок страницы', ['class' => 'text-muted']); ?>
<?= $form->field($model, 'slug', ['makeSlug' => "#place-name"])->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('slug'),
])->hint('Код страницы для ЧПУ', ['class' => 'text-muted']); ?>
<?= $form->field($model->pictures, 'files[]')->widget(FileInput::class, [
    'options'       => ['multiple' => true, 'accept' => 'image/*'],
    'pluginOptions' => $pluginOptions,
]); ?>
<?= $form->field($model, 'sort')->textInput([
    'maxlength'   => true,
    'placeholder' => $model->getAttributePlaceholder('sort'),
])->hint('Порядок сортировки страниц в меню, карте сайта итд.', ['class' => 'text-muted']); ?>
<?= $form->field($model, 'status')->checkbox(); ?>
