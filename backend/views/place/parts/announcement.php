<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\place\PlaceCollection
 * @var $form  \backend\widgets\ActiveForm
 */
?>
<?= $form->field($model, 'announcement')->widget(CKEditor::class, [
    'editorOptions' => ElFinder::ckeditorOptions('elfinder', []),
]) ?>
