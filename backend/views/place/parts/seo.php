<?php
/**
 * @var $form  \yii\widgets\ActiveForm
 * @var $model \backend\models\collections\manage\place\PlaceCollection
 * @var $form  \backend\widgets\ActiveForm
 */
?>
<?= $form->field($model->meta, 'title')->textInput() ?>
<?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
<?= $form->field($model->meta, 'keywords')->textInput() ?>