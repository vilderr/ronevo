<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\place\PlaceCollection
 */
?>
<div class="backend-place-edit">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>