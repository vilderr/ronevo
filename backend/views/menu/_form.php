<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\collections\manage\MenuCollection */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default flat">
    <div class="backend-menu-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="panel-body">
            <?= $form->field($model, 'name')->textInput([
                'maxlength'   => true,
                'placeholder' => $model->getAttributePlaceholder('name'),
            ]); ?>

            <?= $form->field($model, 'route')->textInput([
                'maxlength'   => true,
                'placeholder' => $model->getAttributePlaceholder('route'),
            ]); ?>

            <?= $form->field($model, 'icon')->textInput([
                'maxlength'   => true,
                'placeholder' => $model->getAttributePlaceholder('icon'),
            ]); ?>

            <?= $form->field($model, 'sort')->textInput([
                'placeholder' => $model->getAttributePlaceholder('sort'),
            ]) ?>
        </div>
        <div class="panel-footer">
            <div class="button-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary flat']) ?>
                <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->parent ? $model->parent->id : 0], ['class' => 'btn btn-default flat']); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

