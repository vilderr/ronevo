<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Menu */

?>
<div class="backend-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
