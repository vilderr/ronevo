<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\collections\manage\MenuCollection */
?>
<div class="backend-menu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
