<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\dynagrid\DynaGrid;
use yii\helpers\Url;

/**
 * @var $this         yii\web\View
 * @var $model        \backend\models\Menu
 * @var $dataProvider yii\data\ActiveDataProvider
 */
?>
<div class="backend-menu-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id'    => 'menu-items-grid',
            'class' => 'flat',
        ],
        'columns'           => [
            'id',
            [
                'attribute' => 'name',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute(['index', 'id' => $model->id]));
                },
                'format'    => 'raw',
            ],
            'route',
            [
                'attribute' => 'icon',
                'value'     => function ($model) {
                    return \kartik\icons\Icon::show($model->icon);
                },
                'format'    => 'raw',
            ],
            'sort',
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'heading' => Yii::t('app', 'Menu items: {ITEMS}', ['ITEMS' => $model ? $model->name : 'верхний уровень']),
                'before'  => Html::a(\kartik\icons\Icon::show('plus') . Yii::t('app', 'Create menu item'), ['create', 'id' => $model ? $model->id : 0], ['class' => 'btn btn-primary']),
                'after'   => false,
                'footer'  => false,
            ],

        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
