<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\PageCollection
 */
?>
<div class="backend-page-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>