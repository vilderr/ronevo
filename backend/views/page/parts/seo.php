<?php
/**
 * @var $form  \yii\widgets\ActiveForm
 * @var $model \backend\models\collections\manage\PageCollection
 */
?>
<?= $form->field($model->meta, 'title')->textInput() ?>
<?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
<?= $form->field($model->meta, 'keywords')->textInput() ?>
