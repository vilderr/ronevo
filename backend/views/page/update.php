<?php
/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\PageCollection
 */
?>
<div class="backend-page-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
