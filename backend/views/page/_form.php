<?php
use yii\helpers\Html;
use backend\widgets\ActiveForm;

/**
 * @var $this  \yii\web\View
 * @var $model \backend\models\collections\manage\PageCollection
 */
?>

<div class="backend-page-form flat">
    <?php $form = ActiveForm::begin(); ?>

    <?= \yii\bootstrap\Tabs::widget([
        'id'          => 'page-form-tabs',
        'linkOptions' => [
            'class' => 'flat',
        ],
        'items'       => [
            [
                'label'   => 'Основные настройки',
                'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
            ],
            [
                'label'   => 'Настройки SEO',
                'content' => $this->render('parts/seo', ['form' => $form, 'model' => $model]),
            ],
        ],
    ]); ?>

    <div class="panel-footer">
        <div class="button-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary flat']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->parent ? $model->parent->id : 0], ['class' => 'btn btn-default flat']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
