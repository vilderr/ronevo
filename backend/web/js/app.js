var Backend = function () {
};

Backend.prototype = {
    constructor: Backend
};

Backend.options = {};

Backend.makeSlug = function (selectorsFrom, selectorTo, callback) {
    "use strict";

    var valueFrom = $(selectorsFrom).val();
    var csrfParam = $('meta[name="csrf-param"]').attr("content");
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    if (valueFrom.length) {
        $.ajax({
            'url': '/slug/make?str=' + valueFrom,
            'type': 'POST',
            'cache': false,
            'dataType': 'json',
        }).done(function (data) {
            var $field = $(selectorTo);

            if (typeof $field.attr('maxlength') !== typeof undefined) {
                data = data.substr(0, $field.attr('maxlength'));
            }

            $field.val(data);
        }).fail(function (jqXHR, textStatus) {
            //console.log(jqXHR.responseText);
        });
    }
};