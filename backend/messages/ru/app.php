<?php
return [
    'Dashboard'                     => 'Панель',
    'Sign in to start your session' => 'Пожалуйста авторизуйтесь',
    'Username'                      => 'Логин',
    'Username or email'             => 'Логин или e-mail',
    'Password'                      => 'Пароль',
    'Remember Me'                   => 'Запомнить',
    'Sign in'                       => 'Вход',
    'Logout'                        => 'Выход',
    'Undefined user or password'    => 'Неправильный логин или пароль',

    'Save'   => 'Сохранить',
    'Cancel' => 'Отмена',
    'Apply'  => 'Применить',

    'Create menu item'    => 'Добавить пункт меню',
    'Menu items: {ITEMS}' => 'Пункты меню: {ITEMS}',
    '{ITEM}: page list'   => '{ITEM}: cписок страниц',
];