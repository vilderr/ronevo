<?php

namespace console\controllers;

use backend\models\collections\manage\UserCollection;
use backend\managers\UserManager;
use yii\console\Controller;

/**
 * Class UserController
 * @package console\controllers
 */
class UserController extends Controller
{
    private $_manager;

    /**
     * UserController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param UserManager      $manager
     * @param array            $config
     */
    public function __construct($id, $module, UserManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_manager = $manager;
    }

    public function actionCreate($role)
    {
        $username = $this->prompt('Username:', ['required' => true]);
        $email = $this->prompt('Email:', ['required' => true]);
        $phone = $this->prompt('Phone:', ['required' => true]);
        $password = $this->prompt('Password:', ['required' => true]);
        $password2 = $this->prompt('Retype password:', ['required' => true]);

        if ($password === $password2) {
            $collection = new UserCollection();
            $collection->username = $username;
            $collection->email = $email;
            $collection->phone = $phone;
            $collection->password = $password;
            $collection->role = $role;

            $this->_manager->create($collection);

            $this->stdout('Done!' . PHP_EOL);
        }
    }
}