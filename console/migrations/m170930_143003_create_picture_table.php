<?php

use yii\db\Migration;

/**
 * Handles the creation of table `picture`.
 */
class m170930_143003_create_picture_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%pictures}}', [
            'id'         => $this->primaryKey(),
            'model_name' => $this->string()->notNull(),
            'model_id'   => $this->integer()->notNull(),
            'file'       => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-picture-model_name}}', '{{%pictures}}', 'model_name');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%pictures}}');
    }
}
