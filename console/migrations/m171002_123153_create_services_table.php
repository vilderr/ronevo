<?php

use yii\db\Migration;

/**
 * Handles the creation of table `services`.
 */
class m171002_123153_create_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%services}}', [
            'id'           => $this->primaryKey(),
            'name'         => $this->string(255)->notNull(),
            'title'        => $this->string(255)->notNull(),
            'slug'         => $this->string(255)->notNull(),
            'announcement' => 'MEDIUMTEXT',
            'content'      => 'MEDIUMTEXT',
            'picture_id'   => $this->integer(),
            'meta_json'    => 'JSON NOT NULL',
            'sort'         => $this->integer()->defaultValue(100),
            'created_at'   => $this->integer()->notNull(),
            'updated_at'   => $this->integer()->notNull(),
            'status'       => $this->integer(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('{{%idx-services-status}}', '{{%services}}', 'status');
        $this->addForeignKey('{{%fk-services-picture}}', '{{%services}}', 'picture_id', '{{%pictures}}', 'id', 'SET NULL', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%services}}');
    }
}
