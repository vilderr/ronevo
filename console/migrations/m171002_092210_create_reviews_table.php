<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reviews`.
 */
class m171002_092210_create_reviews_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%reviews}}', [
            'id'           => $this->primaryKey(),
            'name'         => $this->string(255)->notNull(),
            'description'  => $this->string(255),
            'announcement' => 'MEDIUMTEXT',
            'picture_id'   => $this->integer(),
            'sort'         => $this->integer()->defaultValue(100),
            'created_at'   => $this->integer()->notNull(),
            'updated_at'   => $this->integer()->notNull(),
            'status'       => $this->integer(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('{{%idx-reviews-status}}', '{{%reviews}}', 'status');
        $this->addForeignKey('{{%fk-reviews-picture}}', '{{%reviews}}', 'picture_id', '{{%pictures}}', 'id', 'SET NULL', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reviews');
    }
}
