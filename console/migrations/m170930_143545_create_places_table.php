<?php

use yii\db\Migration;

/**
 * Handles the creation of table `places`.
 */
class m170930_143545_create_places_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%places}}', [
            'id'           => $this->primaryKey(),
            'name'         => $this->string(255)->notNull(),
            'title'        => $this->string(255)->notNull(),
            'slug'         => $this->string(255)->notNull(),
            'announcement' => 'MEDIUMTEXT',
            'content'      => 'MEDIUMTEXT',
            'picture_id'   => $this->integer(),
            'meta_json'    => 'JSON NOT NULL',
            'sort'         => $this->integer()->defaultValue(100),
            'created_at'   => $this->integer()->notNull(),
            'updated_at'   => $this->integer()->notNull(),
            'status'       => $this->integer(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('{{%idx-places-slug}}', '{{%places}}', 'slug', true);
        $this->createIndex('{{%idx-places-status}}', '{{%places}}', 'status');

        $this->addForeignKey('{{%fk-places-picture}}', '{{%places}}', 'picture_id', '{{%pictures}}', 'id', 'SET NULL', 'RESTRICT');
        $this->addForeignKey('{{%fk-pictures-places}}', '{{%pictures}}', 'model_id', '{{%places}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('{{%fk-places-picture}}', '{{%places}}');
        $this->dropTable('{{%places}}');
    }
}
