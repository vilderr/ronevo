<?php

use yii\db\Migration;

class m170928_070510_create_dynagrid_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dynagrid}}', [
            'id'        => $this->string(100)->defaultValue(''),
            'filter_id' => $this->string(100),
            'sort_id'   => $this->string(100),
            'data'      => $this->text(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-id', '{{%dynagrid}}', 'id');

        $this->createTable('{{%dynagrid_dtl}}', [
            'id'          => $this->string(128)->defaultValue(''),
            'category'    => $this->string(10),
            'name'        => $this->string(150),
            'data'        => $this->text(),
            'dynagrid_id' => $this->string(100),
            'UNIQUE `uniq_dtl` (`name`, `category`, `dynagrid_id`)',
        ], $tableOptions);

        $this->addPrimaryKey('pk-id', '{{%dynagrid_dtl}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%dynagrid}}');
        $this->dropTable('{{%dynagrid_dtl}}');
    }
}
