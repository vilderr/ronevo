<?php

use yii\db\Migration;

/**
 * Handles the creation of table `backend_menu`.
 */
class m170927_204320_create_backend_menu_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%backend_menu}}', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string(255)->notNull(),
            'route' => $this->string(255),
            'icon'  => $this->string(255),
            'sort'  => $this->integer()->defaultValue(100),
            'lft'   => $this->integer()->notNull(),
            'rgt'   => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'tree'  => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('backend_menu');
    }
}
