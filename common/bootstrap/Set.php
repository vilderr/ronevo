<?php

namespace common\bootstrap;

use Yii;
use yii\base\BootstrapInterface;
use yii\rbac\ManagerInterface;
use yii\caching\Cache;

/**
 * Class Set
 * @package common\bootstrap
 */
class Set implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = Yii::$container;

        $container->setSingleton(ManagerInterface::class, function () use ($app) {
            return $app->authManager;
        });

        $container->setSingleton(Cache::class, function () use ($app) {
            return $app->cache;
        });
    }
}