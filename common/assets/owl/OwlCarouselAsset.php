<?php

namespace common\assets\owl;

use yii\web\AssetBundle;

/**
 * Class OwlCarouselAsset
 * @package common\assets\owl
 */
class OwlCarouselAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/owl/dist';

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $css = [
        'css/owl.carousel.min.css',
    ];

    public $js = [
        'js/owl.carousel.min.js',
    ];
}