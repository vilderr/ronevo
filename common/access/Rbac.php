<?php

namespace common\access;

/**
 * Class Rbac
 * @package common\access
 */
class Rbac
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
}