<?php
return [
    'robotEmail'                    => 'robot@ronevo.local',
    'adminEmail'                    => 'admin@ronevo.local',
    'supportEmail'                  => 'info@ronevo.local',
    'supportPhone'                  => '+7(495) 999-99-88',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration'       => 3600 * 24 * 30,
    'cookieDomain'                  => '.ronevo.local',
    'frontendHostInfo'              => 'http://ronevo.local',
    'backendHostInfo'               => 'http://admin.ronevo.local',
    'staticHostInfo'                => 'http://static.ronevo.local',
    'staticPath'                    => dirname(__DIR__, 2) . '/common/upload',
];
